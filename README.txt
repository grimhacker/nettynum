.       .1111...          | Title: Nettynum
    .10000000000011.   .. | Author: GrimHacker
 .00              000...  | Email: grimhacker@grimhacker.com
1                  01..   | Description:
                    ..    | A tool to enumerate information from the
                   ..     | Windows Domain.
GrimHacker        ..      |
                 ..       |
grimhacker.com  ..        |
@grimhacker    ..         |
----------------------------------------------------------------------------
Nettynum - A Windows Domain Enumeration Tool
    Copyright (C) 2014  Oliver Morton

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

This tool requires the following:
 - Python2.7
 - pywin32
 - pysnmp
 - dnspython

 
Usage:
+ Automated Enumeration:

 - Domain Enumeration
    (i.e. find: domain name, domain controllers, domain accounts policy, domain
    groups, domain group members*, user information*):

    python nettynum.py -A

 - Local Enumeration
    (i.e. find: domain name and 'interesting' hosts. For each interesting host
    enumerate: local accounts policy, local groups, local group members*, user
    information*):

    python nettynum.py -a

* The regular expression in conf/group_regex determines which groups have their 
members enumerated. User account information for these group members is enumerated.


+ Manual Domain Enumeration:

 - Domain Name Enumeration
    (i.e. find domain names from the network.)
    
        python nettynum.py --enumerate_domain_names
 
 - Domain Controller Enumeration
    (i.e. find domain controllers for the specified domain name.)
    
        python nettynum.py --domain DOMAIN --enumerate_domain_controllers 

 - Damain Accounts Policies Enumeration
    (i.e. For the specified domain controller, find: lockout threshold, duration
    and observation window; password length age and history requirements and force
    logoff time.)

        python nettynum.py --domain DOMAIN --domain_controller CONTROLLER
                            --enumerate_domain_policies
 
 - Domain Groups Enumeration
    (i.e. For the specified domain controller, find: group names and comments.)

        python nettynum.py --domain DOMAIN --domain_controller CONTROLLER
                            --enumerate_domain_groups

 - Domain Group Members Enumeration
    (i.e. For the specified group name on the specified domain controller, find:
    the usernames of its members.)

        python nettynum.py --domain DOMAIN --domain_controller CONTROLLER
                            --group_name GROUP --enumerate_domain_group_members

 - Domain Users Enumeration
    (i.e. For the specified domain controller, find: all the 'normal' user account
    names.)

        python nettynum.py --domain DOMAIN --domain_controller CONTROLLER
                            --enumerate_domain_users

 - Domain User Information Enumeration
    (i.e. For the specified user on the specified domain controller, find account
    information including: comment, bad password count, password expired, account
    expired, etc.)

        python nettynum.py --domain DOMAIN --domain_controller CONTROLLER
                            --enumerate_domain_user --user USERNAME
    
 - Domain User Group Membership Enumeration
    (i.e. For the specified user on the specified domain controller, find the groups
    of which the user is a member.)

        python nettynum.py --domain DOMAIN --domain_controller CONTROLLER
                            --user USERNAME --enumerate_domain_group_membership


+ Manual Local Enumeration:

 - Interesting Host Enumeration
    (i.e. find hosts advertised through DNS and WINS.)

        python nettynum.py --domain DOMAIN --enumerate_interesting_hosts

 - Local Accounts Policies Enumeration
    (i.e. For the specified host, find: lockout threshold, duration and observation
    window; password length age and history requirements and force logoff time.)

        python nettynum.py --host HOST --enumerate_local_policies

 - Local Groups Enumeration
    (i.e. For the specified host, find: group names and comments.)

        python nettynum.py --host HOST --enumerate_local_groups
    
 - Local Group Members Enumeration
    (i.e. For the specified group name on the specified host, find: the usernames of
    its members.)

        python nettynum.py --host HOST --group_name GROUP
                            --enumerate_local_group_members

 - Local Users Enumeration
    (i.e. For the specified host, find: all the 'normal' user account names.)

        python nettynum.py --host HOST --enumerate_local_users
        
 - Local User Information Enumeration
    (i.e. For the specified user on the specified host, find account information
    including: comment, bad password count, password expired, account expired, etc.)

        python nettynum.py --host HOST --user USERNAME --enumerate_local_user
    
 - Local User Group Membership Enumeration
    (i.e. For the specified user on the specified host, find the groups of which the
    user is a member.)
    
        python nettynum.py --host HOST --user USERNAME
                            --enumerate_local_group_membership

                            
+ Authentication
    By default null credentials (i.e. empty username and password) are used.
    To specify a different set of credentials user the following options:
    
 - Username to authenticate with:
        --auth_user USER
        
 - Prompt for password to authenticate with:
        --auth_passwd
        
 - Domain name to authenticate with:
        --auth_domain DOMAIN


+ Targeting
    The following options can be used to provide prerequisite information for manual
    enumeration (as shown in the sections above) or curtail automated enumeration.
    e.g. Specifiying the domain name will cause the automated enumeration to skip
    domain name enumeration and only enumeration domain controllers for the specified
    domain name.
    
 - Domain name to enumerate:
        --domain DOMAIN
        
 - Domain controller to enumerate:
        --domain_controller CONTROLLER
        
 - Group name to enumerate:
        --group GROUP
        
 - User name to enumerate:
        --user USERNAME

 - Host to enumerate:
        --host HOST


+ Output

 - Output file:
        The default XML output file is 'nettynum.xml' this can either be read directly
        using your prefered text editor or it can be opened in the browser where it will
        be transformed into a more readable form using the XSLT file.
        The default output filename can be changed using the following option:
            --output_file OUTPUT_FILENAME

 - Logging:
        Verbosity.
        The default logging level is INFO, this states the general action that the program
        is taking and any errors that occur. The following option can be used to increase
        the logging verbosity to DEBUG, where more detailed information about the actions
        the tool is taking and the responses it receives are stated.
            --verbose

        Log file.
        By default logging messages are output to the console, using the following option
        DEBUG level logging is output to the specified file.
            --log_file LOG_FILENAME
