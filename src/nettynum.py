'''
.       .1111...          | Title: nettynum.py
    .10000000000011.   .. | Author: Oliver Morton
 .00              000...  | Email: grimhacker@grimhacker.com
1                  01..   | Description:
                    ..    | Windows domain enumerator.
                   ..     |
GrimHacker        ..      |
                 ..       |
grimhacker.com  ..        |
@grimhacker    ..         | Created on: 28 Jan 2014
----------------------------------------------------------------------------
Nettynum - A Windows Domain Enumeration Tool
    Copyright (C) 2014  Oliver Morton

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

__version__ = "$Revision: 2 $"
# $Source$

import logging
import argparse
import getpass
import os

from lib.automation import DomainAutomation as DomainAutomation
from lib.automation import LocalAutomation as LocalAutomation
from lib.manual import DomainManual as DomainManual
from lib.manual import LocalManual as LocalManual


class Nettynum(object):
    """"""
    def __init__(self, **kwargs):
        """Initialise Nettynum.
        Ensure only one form of enumeration is selected.
        Ensure prerequisite information for enumeration is provided."""
        self.log = logging.getLogger(__name__)

        # Get arguments:
        try:
            self.automated_domain = kwargs['automated_domain']
        except:
            self.automated_domain = False
        try:
            self.automated_local = kwargs['automated_local']
        except:
            self.automated_local = False
        try:
            self.enumerate_domain_names = kwargs['enumerate_domain_names']
        except:
            self.enumerate_domain_names = False
        try:
            self.enumerate_domain_controllers = kwargs['enumerate_domain_controllers']
        except:
            self.enumerate_domain_controllers = False
        try:
            self.enumerate_domain_policies = kwargs['enumerate_domain_policies']
        except:
            self.enumerate_domain_policies = False
        try:
            self.enumerate_domain_groups = kwargs['enumerate_domain_groups']
        except:
            self.enumerate_domain_groups = False
        try:
            self.enumerate_domain_group_members = kwargs['enumerate_domain_group_members']
        except:
            self.enumerate_domain_group_members = False
        try:
            self.enumerate_domain_users = kwargs['enumerate_domain_users']
        except:
            self.enumerate_domain_users = False
        try:
            self.enumerate_domain_user = kwargs['enumerate_domain_user']
        except:
            self.enumerate_domain_user = False
        try:
            self.enumerate_domain_group_membership = kwargs['enumerate_domain_group_membership']
        except:
            self.enumerate_domain_group_membership = False
        try:
            self.enumerate_interesting_hosts = kwargs['enumerate_interesting_hosts']
        except:
            self.enumerate_interesting_hosts = False
        try:
            self.enumerate_local_policies = kwargs['enumerate_local_policies']
        except:
            self.enumerate_local_policies = False
        try:
            self.enumerate_local_groups = kwargs['enumerate_local_groups']
        except:
            self.enumerate_local_groups = False
        try:
            self.enumerate_local_group_members = kwargs['enumerate_local_group_members']
        except:
            self.enumerate_local_group_members = False
        try:
            self.enumerate_local_users = kwargs['enumerate_local_users']
        except:
            self.enumerate_local_users = False
        try:
            self.enumerate_local_user = kwargs['enumerate_local_user']
        except:
            self.enumerate_local_user = False
        try:
            self.enumerate_local_user = kwargs['enumerate_local_user']
        except:
            self.enumerate_local_user = False
        try:
            self.enumerate_local_group_membership = kwargs['enumerate_local_group_membership']
        except:
            self.enumerate_local_group_memberhsip = False
        try:
            self.enumerate_local_shares = kwargs['enumerate_local_shares']
        except:
            self.enumerate_local_shares = False
        try:
            self.domain_name = kwargs['domain_name']
        except:
            self.domain_name = None
        try:
            self.domain_controller = kwargs['domain_controller']
        except:
            self.domain_controller = None
        try:
            self.host = kwargs['host']
        except:
            self.host = None
        try:
            self.group_name = kwargs['group_name']
        except:
            self.group_name = None
        try:
            self.user_name = kwargs['user_name']
        except:
            self.user_name = None
        try:
            self.domain = kwargs['domain']
        except:
            self.domain = None
        try:
            self.user = kwargs['user']
        except:
            self.user = None
        try:
            self.passwd = kwargs['passwd']
        except:
            self.passwd = None
        try:
            self.community = kwargs['community']
        except:
            self.community = None
        try:
            self.output_file = kwargs['output_file']
        except:
            self.output_file = None
        try:
            self._forced_local_groups_file = kwargs['forced_local_groups_file']
        except:
            self._forced_local_groups_file = os.path.join("conf", "local_groups")
        try:
            self._forced_domain_groups_file = kwargs['_forced_domain_groups_file']
        except:
            self._forced_domain_groups_file = os.path.join("conf", "domain_groups")

        # Ensure only one enumeration has been specified.
        directions = [self.automated_domain,
                      self.automated_local,
                      self.enumerate_domain_names,
                      self.enumerate_domain_controllers,
                      self.enumerate_domain_policies,
                      self.enumerate_domain_groups,
                      self.enumerate_domain_group_members,
                      self.enumerate_domain_users,
                      self.enumerate_domain_user,
                      self.enumerate_domain_group_membership,
                      self.enumerate_interesting_hosts,
                      self.enumerate_local_policies,
                      self.enumerate_local_groups,
                      self.enumerate_local_group_members,
                      self.enumerate_local_users,
                      self.enumerate_local_user,
                      self.enumerate_local_group_membership,
                      self.enumerate_local_shares]
        if directions.count(False) != (len(directions) - 1):
            self.log.critical("Must specify (only) one enumeration.")
            exit()
        else:
            # directions.count(False) == (len(directions) - 1) therefore only one direction has been selected.
            pass

        # Ensure appropriate prerequisite information has been specified for the selected enumeration.
        if self.automated_domain:
            # No prerequisite information unless targeting parameters are used.
            if self.user_name is not None:
                # User to enumerate specified.
                if self.domain_controller is None:
                    self.log.critical("Must specify domain_controller to specify user.")
                    exit()
                elif self.domain_name is None:
                    self.log.critical("Must specify domain to specify user.")
                    exit()
                else:
                    pass # all prerequisites for user enumeration present.
            elif self.group_name is not None:
                # Group to enumerate specified.
                if self.domain_controller is None:
                    self.log.critical("Must specify domain_controller to specify group.")
                    exit()
                elif self.domain_name is None:
                    self.log.critical("Must specify domain to specify group.")
                    exit()
                else:
                    pass # all prerequisites for group enumeration present.
            elif self.domain_controller is not None:
                # Domain controller to enumerate specified.
                if self.domain_name is None:
                    self.log.critical("Must specify domain to specify domain_controller.")
                    exit()
                else:
                    pass # all prerequisites for domain controller enumeration present.
        elif self.automated_local:
            # No prerequisite information unless targeting parameters are used.
            if self.user_name is not None:
                # User to enumerate specified.
                if self.host is None:
                    self.log.critical("Must specify host to specify user.")
                    exit()
                else:
                    pass # all prerequisites for user enumeration present.
            elif self.group_name is not None:
                # Group to enumerate specified.
                if self.host is None:
                    self.log.critical("Must specify host to specify group.")
                    exit()
                else:
                    pass # all prerequisites for group enumeration present.
        elif self.enumerate_domain_names:
            pass  # No prerequisite information.
        elif self.enumerate_domain_controllers:
            if self.domain_name is None:
                self.log.critical("Must specify domain to enumerate domain controllers.")
                exit()
        elif self.enumerate_domain_policies:
            if self.domain_name is None or self.domain_controller is None:
                self.log.critical("Must specify domain and domain_controller to enumerate domain policies.")
                exit()
        elif self.enumerate_domain_groups:
            if self.domain_name is None or self.domain_controller is None:
                self.log.critical("Must specify domain and domain_controller to enumerate domain groups.")
                exit()
        elif self.enumerate_domain_group_members:
            if self.domain_name is None or self.domain_controller is None or self.group_name is None:
                self.log.critical("Must specify domain, domain_controller and group to enumerate domain group members.")
                exit()
        elif self.enumerate_domain_users:
            if self.domain_name is None or self.domain_controller is None:
                self.log.critical("Must specify domain and domain_controller to enumerate domain users.")
                exit()
        elif self.enumerate_domain_user:
            if self.domain_name is None or self.domain_controller is None or self.user_name is None:
                self.log.critical("Must specify domain, domain_controller and user to enumerate domain user account information.")
                exit()
        elif self.enumerate_domain_group_membership:
            if self.domain_name is None or self.domain_controller is None or self.user_name is None:
                self.log.critical("Must specify domain, domain_controller and user to enumerate domain user group membership.")
                exit()
        elif self.enumerate_interesting_hosts:
            if self.domain_name is None:
                self.log.critical("Must specify domain to enumerate interesting hosts.")
                exit()
        elif self.enumerate_local_policies:
            if self.host is None:
                self.log.critical("Must specify host to enumerate local accounts policies.")
                exit()
        elif self.enumerate_local_shares:
            if self.host is None:
                self.log.critical("Must specify host to enumerate local shares.")
                exit()
        elif self.enumerate_local_groups:
            if self.host is None:
                self.log.critical("Must specify host to enumerate local groups.")
                exit()
        elif self.enumerate_local_group_members:
            if self.host is None or self.group_name is None:
                self.log.critical("Must specify host and group to enumerate local group members.")
                exit()
        elif self.enumerate_local_users:
            if self.host is None:
                self.log.critical("Must specify host to enumerate local users.")
                exit()
        elif self.enumerate_local_user:
            if self.host is None or self.user_name is None:
                self.log.critical("Must specify host and user_name to enumerate local user account information.")
                exit()
        elif self.enumerate_local_group_membership:
            if self.host is None or self.user_name is None:
                self.log.critical("Must specify host and user_name to enumerate local user group membership.")
                exit()
        else:
            pass

    def _automated_domain_enumeration(self):
        """Create and run DomainAutomation object."""
        args = {}
        if self.domain_name is not None:
            args['domain_name'] = self.domain_name
        if self.domain_controller is not None:
            args['domain_controller'] = self.domain_controller
        if self.group_name is not None:
            args['group'] = self.group_name
        if self.domain is not None:
            args['auth_domain'] = self.domain
        if self.user is not None:
            args['auth_user'] = self.user
        if self.passwd is not None:
            args['auth_passwd'] = self.passwd
        if self.community is not None:
            args['auth_community'] = self.community
        args['forced_groups'] = self._forced_domain_groups_file
        enumerator = DomainAutomation(**args)
        enumerator.enumerate()
        return enumerator

    def _automated_local_enumeration(self):
        """Create and run LocalAutomation object."""
        args = {}
        if self.domain_name is not None:
            args['domain_name'] = self.domain_name
        if self.host is not None:
            args['host'] = self.host
        if self.group_name is not None:
            args['group_name'] = self.group_name
        if self.domain is not None:
            args['auth_domain'] = self.domain
        if self.user is not None:
            args['auth_user'] = self.user
        if self.passwd is not None:
            args['auth_passwd'] = self.passwd
        if self.community is not None:
            args['auth_community'] = self.community
        args['forced_groups'] = self._forced_local_groups_file
        enumerator = LocalAutomation(**args)
        enumerator.enumerate()
        return enumerator

    def _domain_names_enumeration(self):
        """Create DomainManual object and run domain name enumeration."""
        enumerator = DomainManual()
        enumerator.enumerate_domain_names()
        return enumerator

    def _domain_controllers_enumeration(self):
        """Create DomainManual object and run domain controller enumeration."""
        args = {}
        if self.domain_name is not None:
            args['domain_name'] = self.domain_name
        if self.domain is not None:
            args['auth_domain'] = self.domain
        if self.user is not None:
            args['auth_user'] = self.user
        if self.passwd is not None:
            args['auth_passwd'] = self.passwd
        if self.community is not None:
            args['auth_community'] = self.community
        enumerator = DomainManual(**args)
        enumerator.enumerate_domain_controllers()
        return enumerator

    def _domain_policies_enumeration(self):
        """Create DomainManual object and run domain policies enumeration."""
        args = {}
        if self.domain_name is not None:
            args['domain_name'] = self.domain_name
        if self.domain_controller is not None:
            args['domain_controller'] = self.domain_controller
        if self.domain is not None:
            args['auth_domain'] = self.domain
        if self.user is not None:
            args['auth_user'] = self.user
        if self.passwd is not None:
            args['auth_passwd'] = self.passwd
        if self.community is not None:
            args['auth_community'] = self.community
        enumerator = DomainManual(**args)
        enumerator.enumerate_domain_policies()
        return enumerator

    def _domain_groups_enumeration(self):
        """Create DomainManual object and run domain groups enumeration."""
        args = {}
        if self.domain_name is not None:
            args['domain_name'] = self.domain_name
        if self.domain_controller is not None:
            args['domain_controller'] = self.domain_controller

        if self.domain is not None:
            args['auth_domain'] = self.domain
        if self.user is not None:
            args['auth_user'] = self.user
        if self.passwd is not None:
            args['auth_passwd'] = self.passwd
        if self.community is not None:
            args['auth_community'] = self.community
        enumerator = DomainManual(**args)
        enumerator.enumerate_domain_groups()
        return enumerator

    def _domain_group_members_enumeration(self):
        """Create DomainManual object and run domain group members enumeration."""
        args = {}
        if self.domain_name is not None:
            args['domain_name'] = self.domain_name
        if self.domain_controller is not None:
            args['domain_controller'] = self.domain_controller
        if self.group_name is not None:
            args['group_name'] = self.group_name
        if self.domain is not None:
            args['auth_domain'] = self.domain
        if self.user is not None:
            args['auth_user'] = self.user
        if self.passwd is not None:
            args['auth_passwd'] = self.passwd
        if self.community is not None:
            args['auth_community'] = self.community
        enumerator = DomainManual(**args)
        enumerator.enumerate_domain_group_members()
        return enumerator

    def _domain_users_enumeration(self):
        """Create DomainManual object and run domain users enumeration."""
        args = {}
        if self.domain_name is not None:
            args['domain_name'] = self.domain_name
        if self.domain_controller is not None:
            args['domain_controller'] = self.domain_controller
        if self.domain is not None:
            args['auth_domain'] = self.domain
        if self.user is not None:
            args['auth_user'] = self.user
        if self.passwd is not None:
            args['auth_passwd'] = self.passwd
        if self.community is not None:
            args['auth_community'] = self.community
        enumerator = DomainManual(**args)
        enumerator.enumerate_users()
        return enumerator

    def _domain_user_enumeration(self):
        """Create DomainManual object and run domain user account information enumeration."""
        args = {}
        if self.domain_name is not None:
            args['domain_name'] = self.domain_name
        if self.domain_controller is not None:
            args['domain_controller'] = self.domain_controller
        if self.user_name is not None:
            args['user'] = self.user_name
        if self.domain is not None:
            args['auth_domain'] = self.domain
        if self.user is not None:
            args['auth_user'] = self.user
        if self.passwd is not None:
            args['auth_passwd'] = self.passwd
        if self.community is not None:
            args['auth_community'] = self.community
        enumerator = DomainManual(**args)
        enumerator.enumerate_user_info()
        return enumerator

    def _domain_group_membership_enumeration(self):
        """Create DomainManual object and run domain group membership enumeration."""
        args = {}
        if self.domain_name is not None:
            args['domain_name'] = self.domain_name
        if self.domain_controller is not None:
            args['domain_controller'] = self.domain_controller
        if self.user_name is not None:
            args['user'] = self.user_name
        if self.domain is not None:
            args['auth_domain'] = self.domain
        if self.user is not None:
            args['auth_user'] = self.user
        if self.passwd is not None:
            args['auth_passwd'] = self.passwd
        if self.community is not None:
            args['auth_community'] = self.community
        enumerator = DomainManual(**args)
        enumerator.enumerate_group_membership()
        return enumerator

    def _interesting_hosts_enumeration(self):
        """Create LocalManual object and run interesting hosts enumeration."""
        args = {}
        if self.domain_name is not None:
            args['domain_name'] = self.domain_name
        if self.domain is not None:
            args['auth_domain'] = self.domain
        if self.user is not None:
            args['auth_user'] = self.user
        if self.passwd is not None:
            args['auth_passwd'] = self.passwd
        if self.community is not None:
            args['auth_community'] = self.community
        enumerator = LocalManual(**args)
        enumerator.enumerate_interesting_hosts()
        return enumerator

    def _local_policies_enumeration(self):
        """Create LocalManual object and run local policies enumeration."""
        args = {}
        if self.domain_name is not None:
            args['domain_name'] = self.domain_name
        if self.host is not None:
            args['host'] = self.host
        if self.domain is not None:
            args['auth_domain'] = self.domain
        if self.user is not None:
            args['auth_user'] = self.user
        if self.passwd is not None:
            args['auth_passwd'] = self.passwd
        if self.community is not None:
            args['auth_community'] = self.community
        enumerator = LocalManual(**args)
        enumerator.enumerate_local_policies()
        return enumerator

    def _local_shares_enumeration(self):
        """Create LocalManual object and run local shares enumeration."""
        args = {}
        if self.domain_name is not None:
            args['domain_name'] = self.domain_name
        if self.host is not None:
            args['host'] = self.host
        if self.domain is not None:
            args['auth_domain'] = self.domain
        if self.user is not None:
            args['auth_user'] = self.user
        if self.passwd is not None:
            args['auth_passwd'] = self.passwd
        if self.community is not None:
            args['auth_community'] = self.community
        enumerator = LocalManual(**args)
        enumerator.enumerate_local_shares()
        return enumerator

    def _local_groups_enumeration(self):
        """Create LocalManual object and run local groups enumeration."""
        args = {}
        if self.domain_name is not None:
            args['domain_name'] = self.domain_name
        if self.host is not None:
            args['host'] = self.host
        if self.domain is not None:
            args['auth_domain'] = self.domain
        if self.user is not None:
            args['auth_user'] = self.user
        if self.passwd is not None:
            args['auth_passwd'] = self.passwd
        if self.community is not None:
            args['auth_community'] = self.community
        enumerator = LocalManual(**args)
        enumerator.enumerate_local_groups()
        return enumerator

    def _local_group_members_enumeration(self):
        """Create LocalManual object and run local group members enumeration."""
        args = {}
        if self.domain_name is not None:
            args['domain_name'] = self.domain_name
        if self.host is not None:
            args['host'] = self.host
        if self.group_name is not None:
            args['group_name'] = self.group_name
        if self.domain is not None:
            args['auth_domain'] = self.domain
        if self.user is not None:
            args['auth_user'] = self.user
        if self.passwd is not None:
            args['auth_passwd'] = self.passwd
        if self.community is not None:
            args['auth_community'] = self.community
        enumerator = LocalManual(**args)
        enumerator.enumerate_local_group_members()
        return enumerator

    def _local_users_enumeration(self):
        """Create LocalManual object and run local users enumeration."""
        args = {}
        if self.domain_name is not None:
            args['domain_name'] = self.domain_name
        if self.host is not None:
            args['host'] = self.host
        if self.domain is not None:
            args['auth_domain'] = self.domain
        if self.user is not None:
            args['auth_user'] = self.user
        if self.passwd is not None:
            args['auth_passwd'] = self.passwd
        if self.community is not None:
            args['auth_community'] = self.community
        enumerator = LocalManual(**args)
        enumerator.enumerate_users()
        return enumerator

    def _local_user_enumeration(self):
        """Create LocalManual object and run local user account information enumeration."""
        args = {}
        if self.domain_name is not None:
            args['domain_name'] = self.domain_name
        if self.host is not None:
            args['host'] = self.host
        if self.user_name is not None:
            args['user'] = self.user_name
        if self.domain is not None:
            args['auth_domain'] = self.domain
        if self.user is not None:
            args['auth_user'] = self.user
        if self.passwd is not None:
            args['auth_passwd'] = self.passwd
        if self.community is not None:
            args['auth_community'] = self.community
        enumerator = LocalManual(**args)
        enumerator.enumerate_user_info()
        return enumerator

    def _local_group_membership_enumeration(self):
        """Create LocalManual object and run local group membership enumeration."""
        args = {}
        if self.domain_name is not None:
            args['domain_name'] = self.domain_name
        if self.host is not None:
            args['host'] = self.host
        if self.user_name is not None:
            args['user'] = self.user_name
        if self.domain is not None:
            args['auth_domain'] = self.domain
        if self.user is not None:
            args['auth_user'] = self.user
        if self.passwd is not None:
            args['auth_passwd'] = self.passwd
        if self.community is not None:
            args['auth_community'] = self.community
        enumerator = LocalManual(**args)
        enumerator.enumerate_group_membership()
        return enumerator

    def run(self):
        """Run selected enumeration and generate report."""
        if self.automated_domain:
            enumerator = self._automated_domain_enumeration()
        elif self.automated_local:
            enumerator = self._automated_local_enumeration()
        elif self.enumerate_domain_names:
            enumerator = self._domain_names_enumeration()
        elif self.enumerate_domain_controllers:
            enumerator = self._domain_controllers_enumeration()
        elif self.enumerate_domain_policies:
            enumerator = self._domain_policies_enumeration()
        elif self.enumerate_domain_groups:
            enumerator = self._domain_groups_enumeration()
        elif self.enumerate_domain_group_members:
            enumerator = self._domain_group_members_enumeration()
        elif self.enumerate_domain_users:
            enumerator = self._domain_users_enumeration()
        elif self.enumerate_domain_user:
            enumerator = self._domain_user_enumeration()
        elif self.enumerate_domain_group_membership:
            enumerator = self._domain_group_membership_enumeration()
        elif self.enumerate_interesting_hosts:
            enumerator = self._interesting_hosts_enumeration()
        elif self.enumerate_local_policies:
            enumerator = self._local_policies_enumeration()
        elif self.enumerate_local_shares:
            enumerator = self._local_shares_enumeration()
        elif self.enumerate_local_groups:
            enumerator = self._local_groups_enumeration()
        elif self.enumerate_local_group_members:
            enumerator = self._local_group_members_enumeration()
        elif self.enumerate_local_users:
            enumerator = self._local_users_enumeration()
        elif self.enumerate_local_user:
            enumerator = self._local_user_enumeration()
        elif self.enumerate_local_group_membership:
            enumerator = self._local_group_membership_enumeration()
        else:
            self.log.critical("Nothing to enumerate.")  # This should never happen because of checks in __init__()
            exit()

        enumerator.generate_report(filename=self.output_file)  # Generate report

        # If group has been manually specified to have its members enumerated, check if this group should be enumerated during automated enumeration in future.
        if self.group_name is not None:
            if self.enumerate_domain_group_members or self.automated_domain:
                forced_group_file = self._forced_domain_groups_file
            elif self.enumerate_local_group_members or self.automated_local:
                forced_group_file = self._forced_local_groups_file

            exists = False
            try:
                with open(forced_group_file, "r") as f:
                    for line in f:
                        if self.group_name.lower() == line.strip().lower():
                            exists = True
                            break
            except Exception as e:
                self.log.warning("Could not read file '{0}'. It will be created. {1}".format(forced_group_file, e))
            if exists == False:
                # Check if the group should be added to the forced group list.
                response = ""
                while response.lower() != "y" and response.lower() != "n":
                    response = raw_input("Would you like to add '{0}' to the list of groups that will always have their members enumerated during automated enumeration? Y/N: ".format(self.group_name))
                if response.lower() == "y":
                    try:
                        with open(forced_group_file, "a+") as f:
                            self.log.debug("Appending '{0}' to '{1}'.".format(self.group_name.lower(), forced_group_file))
                            try:
                                f.writelines("{0}\n".format(self.group_name))
                            except Exception as e:
                                self.log.warning("Failed to append group name to file. {0}".format(e))
                    except Exception as e:
                        self.log.warning("Could not append to file: {0}".format(forced_group_file, e))


def print_version():
    """Print command line version banner."""
    print """

.       .1111...          | Title: nettynum.py {0}
    .10000000000011.   .. | Author: Oliver Morton
 .00              000...  | Email: grimhacker@grimhacker.com
1                  01..   | Description:
                    ..    | Windows domain enumerator.
                   ..     | Requires: pywin32, dnspython, pysnmp
GrimHacker        ..      |
                 ..       |
grimhacker.com  ..        |
@grimhacker    ..         |
----------------------------------------------------------------------------
""".format(__version__)


def setup_logging(verbose, log_file):
    """Configure logging."""
    if log_file:
        logging.basicConfig(level=logging.DEBUG,
                            format="%(asctime)s: %(levelname)s: %(module)s: %(message)s",
                            filename=log_file,
                            filemode='w')
        console_handler = logging.StreamHandler()
        formatter = logging.Formatter("%(levelname)s: %(module)s: %(message)s")
        console_handler.setFormatter(formatter)
        if verbose:
            console_handler.setLevel(logging.DEBUG)
        else:
            console_handler.setLevel(logging.INFO)
        logging.getLogger().addHandler(console_handler)
    else:
        if verbose:
            level = logging.DEBUG
        else:
            level = logging.INFO
        logging.basicConfig(level=level,
                            format="%(levelname)s: %(module)s: %(message)s")


def arguments():
    """Configure command line arguments."""
    parser = argparse.ArgumentParser(description="Windows domain enumerator.")
    param = parser.add_mutually_exclusive_group(required=True)
    param.add_argument("--version", help="Display version information", action="store_true")

    # automated
    param.add_argument("-A", "--automated_domain", help="Automatically enumerate domain level.", action="store_true")
    param.add_argument("-a", "--automated_local", help="Automatically enumerate hosts at a local level.", action="store_true")

    # manual - domain
    param.add_argument("--enumerate_domain_names", help="Enumerate a list of domain names.", action="store_true")
    param.add_argument("--enumerate_domain_controllers", help="Enumerate a list of domain controllers.", action="store_true")
    param.add_argument("--enumerate_domain_policies", help="Enumerate domain accounts policies.", action="store_true")
    param.add_argument("--enumerate_domain_groups", help="Enumerate a list of domain groups", action="store_true")
    param.add_argument("--enumerate_domain_group_members", help="Enumerate a list of domain group members.", action="store_true")
    param.add_argument("--enumerate_domain_users", help="Enumerate a list of domain users.", action="store_true")
    param.add_argument("--enumerate_domain_user", help="Enumerate account information for domain user.", action="store_true")
    param.add_argument("--enumerate_domain_group_membership", help="Enumerate a list of domain groups to which the user belongs.", action="store_true")

    # manual - local
    param.add_argument("--enumerate_interesting_hosts", help="Enumerate a list of interesting hosts.", action="store_true")
    param.add_argument("--enumerate_local_policies", help="Enumerate host's account policies.", action="store_true")
    param.add_argument("--enumerate_local_groups", help="Enumerate a list of local groups.", action="store_true")
    param.add_argument("--enumerate_local_group_members", help="Enumerate a list of local group members.", action="store_true")
    param.add_argument("--enumerate_local_users", help="Enumerate a list of local users.", action="store_true")
    param.add_argument("--enumerate_local_user", help="Enumerate account information for a local user.", action="store_true")
    param.add_argument("--enumerate_local_group_membership", help="Enumerate a list of local groups to which the user belongs.", action="store_true")
    param.add_argument("--enumerate_local_shares", help="Enumerate a list of local shares.", action="store_true")

    # targeting
    targeting = parser.add_argument_group(title="Targeting.")
    targeting.add_argument("--domain", help="Domain name to enumerate.")
    targeting.add_argument("--domain_controller", help="Domain controller to enumerate.")
    targeting.add_argument("--host", help="Host to enumerate.")
    targeting.add_argument("--group", help="Group name to enumerate.")
    targeting.add_argument("--user", help="User name to enumerate.")

    # credentials
    credentials = parser.add_argument_group(title="Credentials.")
    credentials.add_argument("--auth_domain", help="Domain name for authentication.", default="")
    credentials.add_argument("--auth_user", help="User name for authentication.", default="")
    credentials.add_argument("--auth_passwd", help="Password for authentication. (Will be prompted.)", action="store_true")

    # output
    output = parser.add_argument_group(title="Output.")
    output.add_argument("--log_file", help="Log file name.")
    output.add_argument("--output_file", help="Output file name.", default="nettynum.xml")
    output.add_argument("--verbose", help="Verbose logging.", action="store_true")

    # parse
    args = parser.parse_args()
    if args.auth_passwd:
        # Prompt for the password without echoing. Using default prompt "Password: "
        auth_passwd = getpass.getpass()
    else:
        auth_passwd = "" # Default password
    args_dict = {
                 'version': args.version,
                 'automated_domain': args.automated_domain,
                 'automated_local': args.automated_local,
                 'enumerate_domain_names': args.enumerate_domain_names,
                 'enumerate_domain_controllers': args.enumerate_domain_controllers,
                 'enumerate_domain_policies': args.enumerate_domain_policies,
                 'enumerate_domain_groups': args.enumerate_domain_groups,
                 'enumerate_domain_group_members': args.enumerate_domain_group_members,
                 'enumerate_domain_users': args.enumerate_domain_users,
                 'enumerate_domain_user': args.enumerate_domain_user,
                 'enumerate_domain_group_membership': args.enumerate_domain_group_membership,
                 'enumerate_interesting_hosts': args.enumerate_interesting_hosts,
                 'enumerate_local_policies': args.enumerate_local_policies,
                 'enumerate_local_groups': args.enumerate_local_groups,
                 'enumerate_local_group_members': args.enumerate_local_group_members,
                 'enumerate_local_users': args.enumerate_local_users,
                 'enumerate_local_user': args.enumerate_local_user,
                 'enumerate_local_group_membership': args.enumerate_local_group_membership,
                 'enumerate_local_shares': args.enumerate_local_shares,
                 'domain_name': args.domain,
                 'domain_controller': args.domain_controller,
                 'host': args.host,
                 'group_name': args.group,
                 'user_name': args.user,
                 'domain': args.auth_domain,
                 'user': args.auth_user,
                 'passwd': auth_passwd,
                 'log_file': args.log_file,
                 'output_file': args.output_file,
                 'verbose': args.verbose
                 }
    return args_dict

if __name__ == '__main__':
    print """
    Nettynum  Copyright (C) 2014  Oliver Morton
    This program comes with ABSOLUTELY NO WARRANTY.
    This is free software, and you are welcome to redistribute it
    under certain conditions. See GPLv3 License.
"""
    args = arguments()
    setup_logging(args['verbose'], args['log_file'])
    if args['version']:
        print_version()
        exit()
    nettynum = Nettynum(**args)
    nettynum.run()
