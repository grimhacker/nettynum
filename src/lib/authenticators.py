'''
.       .1111...          | Title: authenticators.py
    .10000000000011.   .. | Author: Oliver Morton
 .00              000...  | Email: grimhacker@grimhacker.com
1                  01..   | Description:
                    ..    | All authentication objects.
                   ..     |
GrimHacker        ..      |
                 ..       |
grimhacker.com  ..        |
@grimhacker    ..         | Created on: 22 Oct 2012
----------------------------------------------------------------------------
Nettynum - A Windows Domain Enumeration Tool
    Copyright (C) 2014  Oliver Morton

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
import logging
import win32net
import pywintypes
from pysnmp.entity.rfc3413.oneliner import cmdgen


class BaseAuthenticator(object):
    """Base authentication class."""
    def __init__(self, **kwargs):
        """Initialise base authentication class"""
        self.log = logging.getLogger(__name__)
        self._allow_deauth = True  # Flag used to indicate whether deauthentication is allowed, set to false when a session already existed which must be maintained.
        try:
            self._host = kwargs['host']
        except Exception as e:
            raise Exception("Target host required. {0}".format(e))
        else:
            if isinstance(self._host, basestring):
                pass
            else:
                raise Exception("Target host must be a string.")

    def get_host(self):
        """Return host used for authentication."""
        return self._host

    def authenticate(self, auth_type):
        """Authenticate to host with given credentials."""
        pass

    def deauthenticate(self):
        """Deauthenticate from host."""
        pass

    def _set_allow_deauth(self, value):
        """Define whether deauthentication is allowed."""
        self._allow_deauth = bool(value)

    def _get_allow_deauth(self):
        """Return whether deauthentication is allowed."""
        return self._allow_deauth


class SMBAuthenticator(BaseAuthenticator):
    """Server Message Block Authenticator class."""
    def __init__(self, **kwargs):
        """Initialise SMBAuthenticator. Extends BaseAuthentiactor.__init__()."""
        super(SMBAuthenticator, self).__init__(**kwargs)
        try:
            self._user = kwargs['user']
        except:
            raise Exception("Target user required.")
        else:
            if isinstance(self._user, basestring):
                pass
            else:
                raise Exception("Target user must be a string.")
        try:
            self._passwd = kwargs['passwd']
        except:
            raise Exception("Target passwd required.")
        else:
            if isinstance(self._passwd, basestring):
                pass
            else:
                raise Exception("Target passwd must be a string.")
        try:
            self._domain = kwargs['domain']
        except:
            raise Exception("Target domain required.")
        else:
            if isinstance(self._domain, basestring):
                pass
            else:
                raise Exception("Target domain must be a string.")
        try:
            self._share = kwargs['share']
        except:
            self._share = "IPC$"
            self.log.debug("share not specified, using default: '{0}'".format(self._share))
        else:
            if isinstance(self._share, basestring):
                pass
            else:
                raise Exception("share must be a string.")

        if self._host.startswith("\\\\"):
            self._host = self._host[2:]  # Strip \\ from start
        else:
            self._host = "{0}".format(self._host)

        self._target = "\\\\{0}\\{1}".format(self._host, self._share)  # Construct target, results in: \\<host>\IPC$ by default.
        self.log.debug("target constructed as: '{0}'".format(self._target))

    def get_user(self):
        """Return user used for authentication."""
        return self._user

    def get_passwd(self):
        """Return password used for authentication."""
        return self._passwd

    def get_domain(self):
        """Return domain name used for authentication."""
        return self._domain

    def get_target(self):
        """Return target used for authentication."""
        return self._target

    def get_share(self):
        """Return share used for authentication."""
        return self._share

    def _already_authenticated(self):
        """Check if the system is already authenticated to the host."""
        self.log.debug("Checking for an established connection to {0}".format(self._target))
        status = {0: 'Ok',
                  1: 'Paused',
                  2: 'Disconnected',
                  3: 'Network Error',
                  4: 'Connected',
                  5: 'Reconnected'}
        resume = 0  # Flag used by the Windows API to indicate if there is more data.
        established = False  # Flag used to indicate if there is already a session established.
        while True:
            try:
                # Use pywin32 to get a list of the connections.
                (connections, total, resume) = win32net.NetUseEnum(None, 1, resume)
            except Exception as e:
                self.log.warning("Could not retrieve list of existing connections. {0}".format(e))
                established = False  # If we can't get the list, assume there is no connection.
                break  # If there was an error, break out of the infinite loop.
            else:
                for connection in connections:
                    # For each connection in the list check if it is connected to our target.
                    if connection['remote'] == self._target:
                        self.log.debug("connection to '{0}' exists with status '{1}'".format(self._target, status[connection['status']]))
                        if connection['status'] == 0:
                            established = True  # If the status is 'OK', set the established flag to true to indicate that there is an existing connection that we can use.
                        else:
                            self.deauthenticate()  # If the status is not 'OK', deauthenticate because it is no use to us.
                            established = False  # Set the established flag to false to indicate that there is not an existing connect that we can use.
                        break  # Only one entry for target will be in list so break out of loop.
                if not resume:
                    break  # There is no more information to retrieve using win32net.NetUseEnum so break from the loop.
        return established

    def authenticate(self, auth_type="smb"):
        """Authenticate to host using given credentials. Overrides BaseAuthenticator.authenticate().
        Returns True if successfully authenticated, otherwise it returns False."""
        if self._already_authenticated():  # Check if there is an existing connection to the target.
            self.log.info("Using established session to '{0}' and disallowing deauthentication.".format(self._target))
            self._set_allow_deauth(False)  # If there is an existing connection set the allow_deauth flag to False so that the connection is not terminated later
                                            # (we didn't set it up so we won't tear it down.)
            return True  # Return True to indicate we have a connection to the target.
        else:
            # Data structure required by NetUseAdd:
            data = {'remote': self._target, 'local': "", 'password': self.get_passwd(), 'username': self.get_user(), 'domainname': self.get_domain(), 'asg_type': 3}  # Note: asg_type:0 is a normal share 3 is an ipc$ share.
            try:
                # Use pywin32 to create a connection to the target.
                win32net.NetUseAdd(None, 2, data)
            except pywintypes.error as details:
                self.log.critical("Failed to authenticate to '{0}'. ({1}, '{2}', '{3}')".format(self._target, details[0], details[1], details[2]))
                self.deauthenticate()  # Clean up the failed authentication.
                return False # Return flase to indicate that we did not successfully connect.
            else:
                self.log.debug("Authenticated to '{0}'".format(self._target))
                return True # Return true to indicate we successfully connected to the target.

    def deauthenticate(self):
        """Deauthenticate from host. Overrides BaseAuthenticator.deauthenticate()."""
        if self._get_allow_deauth():  # If the allow_deauth flag is set to True (indicating we are allowed to tear down the connection).
            try:
                # Use pywin32 to delete the connection to the target.
                win32net.NetUseDel(None, self._target)
            except pywintypes.error as details:
                self.log.warning("Failed to deauthenticate from '{0}'. ({1}, '{2}', '{3}')".format(self._target, details[0], details[1], details[2]))
            else:
                self.log.debug("Deauthenticated from '{0}'".format(self._target))
        else:
            self.log.warning("not allowed to deauthenticate.")


class SNMPAuthenticator(BaseAuthenticator):
    """Simple Network Management Protocol Authenticator class."""
    def __init__(self, **kwargs):
        """Initialise SNMPAuthenticator. Extends BaseAuthentiactor.__init__()."""
        super(SNMPAuthenticator, self).__init__(**kwargs)
        try:
            self._community_string = kwargs['community_string']
        except:
            raise Exception("community string required.")
        else:
            if isinstance(self._community_string, basestring):
                pass
            else:
                raise Exception("community string must be a string.")

    def get_community_string(self):
        """Return community string."""
        return self._community_string

    def authenticate(self, auth_type="snmp"):
        """Authenticate to host using given community string. Overrides BaseAuthenticator.authenticate()"""
        # Note: this function only tests for authentication because the SNMP library authenticates and sends the command as a single function.
        try:
            command_generator = cmdgen.CommandGenerator()  # Create pysnmp command object.
        except Exception as e:
            self.log.warning("Failed to create SNMP command generator: {0}".format(e))
        else:
            try:
                # Use pysnmp to get the system description oid. If we can do this then the community string is correct.
                err_indication, err_status, err_index, var_binds = command_generator.getCmd(
                    cmdgen.CommunityData(self.get_community_string()),
                    cmdgen.UdpTransportTarget((self.get_host(), 161)),
                    '1.3.6.1.2.1.1.1.0'  # System description OID.
                    )
            except Exception as e:
                self.log.warning("Failed to test SNMP authentication: {0}".format(e))
            else:
                if err_indication or err_status:
                    self.log.warning("Failed to authenticate using SNMP indicated.")
                else:
                    return True  # Return True to indicate the community string is correct.
        return False  # Return false to indicate the community string is incorrect (or snmp is not available on the host.)


class AuthenticationController(BaseAuthenticator):
    """Object to handle session with host."""
    def __init__(self, host, **kwargs):
        """Initialise AuthenticationController object."""
        super(AuthenticationController, self).__init__(host=host, **kwargs)
        self._authenticators = {}  # Dictionary to map instances of authenticators to their protocol name.
        self._authenticated = []  # List to hold protocols that have been successfully authenticated with.
        try:
            if isinstance(kwargs['user'], basestring):
                self._user = kwargs['user']
            else:
                raise Exception("user must be a string.")
        except:
            self._user = None

        try:
            if isinstance(kwargs['domain'], basestring):
                self._domain = kwargs['domain']
            else:
                raise Exception("domain must be a string.")
        except:
            self._domain = None

        try:
            if isinstance(kwargs['passwd'], basestring):
                self._passwd = kwargs['passwd']
            else:
                raise Exception("passwd must be a string.")
            self._passwd = kwargs['passwd']
        except:
            self._passwd = None

        try:
            if isinstance(kwargs['community_string'], basestring):
                self._community_string = kwargs['community_string']
            else:
                raise Exception("community_string must be a string.")
            self._community_string = kwargs['community_string']
        except:
            self._community_string = None

        if (self._user is None or self._passwd is None) and not (self._user is None and self._passwd is None):
            # Either self._user or self._passwd (but not both) is None.
            raise Exception("Cannot specify user without password or vice versa.")

    def get_community_string(self):
        """Return community string."""
        return self._community_string

    def create_sessions(self):
        """Establish sessions with host."""
        if self._user is not None and self._passwd is not None:  # If user and password are specified.
            try:
                # Create an SMBAuthenticator instance and add it to the dictionary with the key 'smb'
                self._authenticators['smb'] = SMBAuthenticator(host=self._host, user=self._user, passwd=self._passwd, domain=self._domain)
            except Exception as e:
                self.log.warning("error creating SMBAuthenticator: {0}".format(e))
            else:
                try:
                    # Attempt to authenticate with the SMBAuthenticator.
                    if self._authenticators['smb'].authenticate("smb"):
                        self._authenticated.append('smb')  # SMB authentication was successful. Add it to the list of successful protocols.
                except Exception as e:
                    #TODO: Delete SMBAuthenticator instance here with: del self._authenticators['smb']
                    self.log.warning("SMBAuthentication failed: {0}".format(e))

        if self._community_string is not None:  # If the community string is specified.
            try:
                # Create an SNMPAuthenticator instance and add it to the dictionary with the key 'snmp'
                self._authenticators['snmp'] = SNMPAuthenticator(host=self._host, community_string=self._community_string)
            except Exception as e:
                self.log.warning("error creating SNMPAuthenticator: {0}".format(e))
            else:
                try:
                    # Attempt to authenticate with the SNMPAuthenticator
                    if self._authenticators['snmp'].authenticate("snmp"):
                            self._authenticated.append('snmp')  # SNMP authentication was successful. Add it to the list of successful protocols.
                except Exception as e:
                    #TODO: Delete SNMPAuthenticator instance here with: del self._authenticators['snmp']
                    self.log.warning("SNMPAuthentication failed: {0}".format(e))

        if len(self._authenticated):
            self.log.debug("authenticated = {0}".format(self._authenticated))
            return True  # If we created even one type of authenticated session, return True.
        else:
            return False  # If no authenticated sessions, return False.

    def destroy_sessions(self):
        """Destroy sessions with host"""
        #TODO: Should either be: "for session in self._authenticated:", or the authenticators should be deleted from the dictionary if not successful.
        for session in self._authenticators.keys():
            # For each authenticator, deauthenticate the session.
            self._authenticators[session].deauthenticate()

    def authenticate(self, auth_type):
        """Establish session with host. Overrides BaseAuthenticator.authenticate()."""
        if auth_type.lower() in self._authenticated:
            return True  # If we successfully authenticated using the specified protocol earlier, return True to indicate a successful authentication.
        else:
            return False  # If we do not have a connection for the specified protocol return False to indicate failure.

    def deauthenticate(self):
        """Establish session with host. Overrides BaseAuthenticator.deauthenticate()."""
        pass  # Existence of this function prevents errors in enumerators while allowing deauthentication to be prevented.


if __name__ == '__main__':
    logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.DEBUG)
