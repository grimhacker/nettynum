'''
.       .1111...          | Title: manual.py
    .10000000000011.   .. | Author: Oliver Morton
 .00              000...  | Email: grimhacker@grimhacker.com
1                  01..   | Description:
                    ..    | All manual objects.
                   ..     |
GrimHacker        ..      |
                 ..       |
grimhacker.com  ..        |
@grimhacker    ..         | Created on: 10 Jan 2014
----------------------------------------------------------------------------
Nettynum - A Windows Domain Enumeration Tool
    Copyright (C) 2014  Oliver Morton

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
import logging
import re

from data import Domain as Domain
from data import Host as Host
from authenticators import AuthenticationController as AuthenticationController
from enumerators import DomainNameEnumerationController as DomainNameEnumerationController
from enumerators import DomainControllerEnumerationController as DomainControllerEnumerationController
from enumerators import PolicyEnumerationController as PolicyEnumerationController
from enumerators import GroupEnumerationController as GroupEnumerationController
from enumerators import GroupMemberEnumerationController as GroupMemberEnumerationController
from enumerators import GroupMembershipEnumerationController as GroupMembershipEnumerationController
from enumerators import UserEnumerationController as UserEnumerationController
from enumerators import UserInfoEnumerationController as UserInfoEnumerationController
from enumerators import InterestingHostEnumerationController as InterestingHostEnumerationController
from enumerators import LocalGroupEnumerationController as LocalGroupEnumerationController
from enumerators import LocalGroupMemberEnumerationController as LocalGroupMemberEnumerationController
from enumerators import LocalGroupMembershipEnumerationController as LocalGroupMembershipEnumerationController
from enumerators import ShareEnumerationController as ShareEnumerationController
from reporters import DomainXMLReporter as DomainXMLReporter
from reporters import LocalXMLReporter as LocalXMLReporter


class BaseManual(object):
    """Base manual class."""
    def __init__(self, **kwargs):
        """Initialise base manual class."""
        self.log = logging.getLogger(__name__)
        self._data = []

        try:
            self._auth_domain = kwargs['auth_domain']
        except:
            self._auth_domain = ""

        try:
            self._auth_user = kwargs['auth_user']
        except:
            self._auth_user = ""

        try:
            self._auth_passwd = kwargs['auth_passwd']
        except:
            self._auth_passwd = ""

        try:
            self._auth_community = kwargs['auth_community']
        except:
            self._auth_community = ""

        try:
            self._group_name = kwargs['group']
        except:
            self._group_name = None

        try:
            self._user = kwargs['user']
        except:
            self._user = None

    def _get_domain_name(self):
        """Return domain name"""
        return self._domain_name

    def _get_group_name(self):
        """Return group name."""
        return self._group_name

    def _get_user(self):
        """Return user."""
        return self._user

    def _get_auth(self):
        """Return authentication controller."""
        return self._auth

    def get_data(self):
        """Return enumerated information."""
        return self._data

    def _create_reporter(self, filename, data):
        """Return an instance of a reporter object."""
        self.log.critical("_create_reporter() should have been overridden. Something has gone wrong.")
        pass

    def generate_report(self, **kwargs):
        """Generate report of enumerated data."""
        try:
            filename = kwargs['filename']
        except:
            filename = "nettynum.xml"

        try:
            # Create reporter.
            reporter = self._create_reporter(filename, self.get_data())
        except Exception as e:
            self.log.warning("Failed to create reporter. {0}".format(e))
        else:
            try:
                # Generate report.
                reporter.generate()
            except Exception as e:
                self.log.warning("Failed to generate report. {0}".format(e))


class DomainManual(BaseManual):
    """Domain manual class."""
    def __init__(self, **kwargs):
        """Initialise domain manual class. Extends BaseManual.__init__()"""
        super(DomainManual, self).__init__(**kwargs)
        self._auth = None

        try:
            self._domain_name = kwargs['domain_name']
        except:
            self._domain_name = None
        else:
            self._set_domain_name(self._domain_name)

        try:
            self._domain_controller = kwargs['domain_controller']
        except:
            self._domain_controller = None
        else:
            if "." in self._domain_controller:
                # DNS style or IP address
                if re.match("[a-zA-Z]", self._domain_controller):
                    # DNS Style
                    self._data[0].set_domain_controller(dc_fqdn=self._domain_controller)
                else:
                    # IP address
                    self._data[0].set_domain_controller(dc_ip=self._domain_controller)
            else:
                # NetBIOS style
                self._data[0].set_domain_controller(dc_nb=self._domain_controller)
            # If domain controller has been specified, authentication will be required. Included here to reduce code duplication.
            self._auth = AuthenticationController(user=self._auth_user, passwd=self._auth_passwd, host=self._domain_controller, domain=self._auth_domain, community_string=self._auth_community)
            self._auth.create_sessions()
        try:
            user = kwargs['user']
        except:
            user = None
        finally:
            self.set_user(user)

        try:
            group_name = kwargs['group_name']
        except:
            group_name = None
        finally:
            self.set_group_name(group_name)

    def get_group_name(self):
        return self._group_name

    def set_group_name(self, group_name):
        self._group_name = group_name

    def get_user(self):
        return self._user

    def set_user(self, user):
        self._user = user

    def _set_domain_name(self, domain_name):
        """Create domain object for domain_name."""
        domains = []
        if domain_name.endswith("."):
            domain_name = domain_name[:-1]  # strip the trailing "." from fully qualified domain names.
        # Get all the domain names that are in self._data in a list.
        for domain in self.get_data():
            domains.append(domain.get_domain_name())
        # If the new domain name is not already in the list of domains, create a new Domain instance to add it.
        if domain_name not in domains:
            self._data.append(Domain(domain_name))

    def _get_domain_controller(self):
        """Return domain controller."""
        return self._domain_controller

    def _create_reporter(self, filename, data):
        """Return an instance of a reporter object."""
        return DomainXMLReporter(filename=filename, data=data)

    def enumerate_domain_names(self):
        """Enumerate domain names."""
        self.log.info("Enumerating domain names...")
        # Use DomainNameEnumerationController to enumerate domain names.
        domain_name_enumerator = DomainNameEnumerationController()
        domain_name_enumerator.enumerate()
        for domain_name in domain_name_enumerator.get_domain_names():
            self._set_domain_name(domain_name)  # For each domain name enumerated, add it to self.

        #self._auth.destroy_sessions()  # Deauthenticate from host.

    def enumerate_domain_controllers(self):
        """Enumerate domain controllers."""
        self.log.info("Enumerating domain controllers...")
        for domain in self.get_data():
            if not isinstance(domain.get_domain_name(), basestring):
                raise StandardError("must specify domain name to enumerate domain controllers.")

        for domain in self.get_data():
            # For each domain name that we know about, enumerate domain controllers using DomainControllerEnumerationController.
            domain_controller_enumerator = DomainControllerEnumerationController(domain_name=domain.get_domain_name())
            domain_controller_enumerator.enumerate()
            for domain_controller in domain_controller_enumerator.get_domain_controllers():
                domain.set_domain_controller(**domain_controller)  # For each domain controller enumerated, add it to self.

        #self._auth.destroy_sessions()  # Deauthenticate from host.

    def enumerate_domain_policies(self):
        """Enumerate domain accounts policies."""
        self.log.info("Enumerating domain accounts policies...")
        for domain in self.get_data():
            for domain_controller in domain.get_domain_controllers():
                if not (domain_controller['dc_ip'] != "" or domain_controller['dc_fqdn'] != "" or domain_controller['dc_nb'] != ""):
                    raise StandardError("must specify domain controller to enumerate domain account policies.")

        for domain in self.get_data():
            auth = self._get_auth()  # Get the authenticator, it is required so raise an error if it doesn't exist.
            if auth is None:
                raise StandardError("authentication controller doesn't exist.")
            else:
                # For each domain name domain we know about, get the accounts policies using the PolicyEnumerationController. 
                domain_controller = auth.get_host()  # Get the domain controller from the authenticator.
                policy_enumerator = PolicyEnumerationController(auth=auth)
                policy_enumerator.enumerate()
                domain.set_policy(**policy_enumerator.get_policy())  # Add the policies to self.

        self._auth.destroy_sessions()  # Deauthenticate from the host.

    def enumerate_domain_groups(self):
        """Enumerate domain groups."""
        self.log.info("Enumerating domain groups...")
        for domain in self.get_data():
            for domain_controller in domain.get_domain_controllers():
                if not (domain_controller['dc_ip'] != "" or domain_controller['dc_fqdn'] != "" or domain_controller['dc_nb'] != ""):
                    raise StandardError("must specify domain controller to enumerate domain account policies.")

        for domain in self.get_data():
            auth = self._get_auth()  # Get the authenticator, it is required so raise an error if it doesn't exist.
            if auth is None:
                raise StandardError("authentication controller doesn't exist.")
            else:
                # For each domain name domain we know about, get the domain groups using the GroupEnumerationController.
                group_enumerator = GroupEnumerationController(auth=auth)
                group_enumerator.enumerate()
                for group in group_enumerator.get_groups():
                    domain.set_group(**group)  # Add enumerated groups to self.

        self._auth.destroy_sessions()  # Deauthenticate from the host.

    def enumerate_domain_group_members(self):
        """Enumerate domain group members."""
        self.log.info("Enumerating domain group members...")
        for domain in self.get_data():
            for domain_controller in domain.get_domain_controllers():
                if not (domain_controller['dc_ip'] != "" or domain_controller['dc_fqdn'] != "" or domain_controller['dc_nb'] != ""):
                    raise StandardError("must specify domain controller to enumerate domain account policies.")

        for domain in self.get_data():
            auth = self._get_auth()  # Get the authenticator, it is required so raise an error if it doesn't exist.
            if auth is None:
                raise StandardError("authentication controller doesn't exist.")
            else:
                # For each domain name domain we know about, get the domain group members for the specified group using the GroupMemberEnumerationController.
                domain_controller = auth.get_host()
                domain.set_group(name=self.get_group_name())
                for group in domain.get_groups():
                    group_member_enumerator = GroupMemberEnumerationController(auth=auth, group=group.get_group_name())
                    group_member_enumerator.enumerate()
                    for member in group_member_enumerator.get_members():
                        group.set_member(member)  # Add enumerated groups to self.

        self._auth.destroy_sessions()  # Deauthenticate from the host.

    def enumerate_users(self):
        """Enumerate user accounts."""
        self.log.info("Enumerating domain users...")
        for domain in self.get_data():
            for domain_controller in domain.get_domain_controllers():
                if not (domain_controller['dc_ip'] != "" or domain_controller['dc_fqdn'] != "" or domain_controller['dc_nb'] != ""):
                    raise StandardError("must specify domain controller to enumerate domain account policies.")

        for domain in self.get_data():
            auth = self._get_auth()  # Get the authenticator, it is required so raise an error if it doesn't exist.
            if auth is None:
                raise StandardError("authentication controller doesn't exist.")
            else:
                # For each domain name domain we know about, get the domain users using the UserEnumerationController.
                domain_controller = auth.get_host()
                user_enumerator = UserEnumerationController(auth=auth)
                user_enumerator.enumerate()
                for user in user_enumerator.get_info():
                    domain.set_user(**user)  # Add enumerated user to self.

        self._auth.destroy_sessions()  # Deauthenticate from the host

    def enumerate_user_info(self):
        """Enumerate user account information."""
        self.log.info("Enumerating domain user account information...")
        for domain in self.get_data():
            for domain_controller in domain.get_domain_controllers():
                if not (domain_controller['dc_ip'] != "" or domain_controller['dc_fqdn'] != "" or domain_controller['dc_nb'] != ""):
                    raise StandardError("must specify domain controller to enumerate domain user information.")

        for domain in self.get_data():
            auth = self._get_auth()  # Get the authenticator, it is required so raise an error if it doesn't exist.
            if auth is None:
                raise StandardError("authentication controller doesn't exist.")
            else:
                # For each domain name domain we know about, get account information for the specified user using the UserInfoEnumerationController.
                domain_controller = auth.get_host()
                user_info_enumerator = UserInfoEnumerationController(user=self.get_user(), auth=auth)
                user_info_enumerator.enumerate()
                domain.set_user(**user_info_enumerator.get_info())  # Add enumerated user to self.

        self._auth.destroy_sessions()  # Destroy sessions.

    def enumerate_group_membership(self):
        """Enumerate a list of groups to which the user belongs."""
        self.log.info("Enumerating domain user's group membership...")
        for domain in self.get_data():
            for domain_controller in domain.get_domain_controllers():
                if not (domain_controller['dc_ip'] != "" or domain_controller['dc_fqdn'] != "" or domain_controller['dc_nb'] != ""):
                    raise StandardError("must specify domain controller to enumerate domain group membership.")

        for domain in self.get_data():
            auth = self._get_auth()  # Get the authenticator, it is required so raise an error if it doesn't exist.
            if auth is None:
                raise StandardError("authentication controller doesn't exist.")
            else:
                # For each domain name domain we know about, get the specified user's group membership using the GroupMembershipEnumerationController.
                domain_controller = auth.get_host()
                membership_enumerator = GroupMembershipEnumerationController(user=self.get_user(), auth=auth)
                membership_enumerator.enumerate()
                domain.set_user(name=self.get_user(), groups=membership_enumerator.get_groups())  # Add enumerated groups to self.

        self._auth.destroy_sessions()  # Deauthenticate from host.


class LocalManual(BaseManual):
    """Domain manual class."""
    def __init__(self, **kwargs):
        """Initialise domain manual class. Extends BaseManual.__init__()"""
        super(LocalManual, self).__init__(**kwargs)

        try:
            self._domain_name = kwargs['domain_name']
        except:
            self._domain_name = None

        try:
            self._host = kwargs['host']
        except:
            self._host = None
        else:
            self._set_host(self._host)
            self._auth = AuthenticationController(user=self._auth_user, passwd=self._auth_passwd, host=self._host, domain=self._auth_domain, community_string=self._auth_community)
            self._auth.create_sessions()
        try:
            user = kwargs['user']
        except:
            user = None
        finally:
            self.set_user(user)

        try:
            group_name = kwargs['group_name']
        except:
            group_name = None
        finally:
            self.set_group_name(group_name)

    def get_group_name(self):
        return self._group_name

    def set_group_name(self, group_name):
        self._group_name = group_name

    def get_user(self):
        return self._user

    def set_user(self, user):
        self._user = user

    def _set_host(self, name):
        """Add host to list of hosts"""
        host_in_list = False  # Flag to indicate whether host is already in the list or not.
        for host in self._data:
            if name == host.get_name():  # Check every known host name to see if it matches the new host.
                host_in_list = True
        if host_in_list:
            self.log.debug("ignoring duplicate host name.")
        else:
            # If the new host is not already in the list, create a new Host instance for it.
            self._data.append(Host(name=name))

    def _create_reporter(self, filename, data):
        """Return an instance of a reporter object."""
        return LocalXMLReporter(filename=filename, data=data)

    def enumerate_interesting_hosts(self):
        self.log.info("Enumerating 'interesting' hosts...")
        if self._get_domain_name() is None:
            raise StandardError("must specify domain name to enumerate interesting hosts.")
        else:
            # Enumerate hosts in the specified domain using InterestingHostEnumerationController.
            interesting_hosts_enumerator = InterestingHostEnumerationController(domain=self._get_domain_name())
            interesting_hosts_enumerator.enumerate()
            for host_service in interesting_hosts_enumerator.get_hosts():
                self._set_host(host_service['name'])  # Add host names

            for host in self.get_data():
                for host_service in interesting_hosts_enumerator.get_hosts():
                    if host_service['name'] == host.get_name():
                        host.set_service(host_service['service'], host_service['port'])  # Add services.

    def enumerate_local_policies(self):
        """Enumerate local accounts policies."""
        self.log.info("Enumerating local accounts policies...")
        for host_data in self.get_data():
            if not host_data.get_name():  # check that all host has been specified.
                raise StandardError("must specify host to enumerate local account policies.")

        for host_data in self.get_data():
            auth = self._get_auth()  # Get the authenticator, it is required so raise an error if it doesn't exist.
            if auth is None:
                raise StandardError("authentication controller doesn't exist.")
            else:
                # Enumerate the accounts policy for the specified host.
                policy_enumerator = PolicyEnumerationController(auth=auth)
                policy_enumerator.enumerate()
                host_data.set_policy(**policy_enumerator.get_policy())  # Add the enumerated policies to the data structure.

        self._auth.destroy_sessions()  # Deauthenticate from host.

    def enumerate_local_shares(self):
        """Enumerate local shares."""
        self.log.info("Enumerating local shares...")
        for host_data in self.get_data():
            if not host_data.get_name():  # check that all host has been specified.
                raise StandardError("must specify host to enumerate local shares.")

        for host_data in self.get_data():
            auth = self._get_auth()  # Get the authenticator, it is required so raise an error if it doesn't exist.
            if auth is None:
                raise StandardError("authentication controller doesn't exist.")
            else:
                # Enumerate shares for the specified host.
                share_enumerator = ShareEnumerationController(auth=auth)
                share_enumerator.enumerate()
                for share in share_enumerator.get_shares():
                    host_data.set_share(**share)  # Add the enumerated shares to the data structure.

        self._auth.destroy_sessions()  # Deauthenticate from host.

    def enumerate_local_groups(self):
        """Enumerate local groups."""
        self.log.info("Enumerating local groups...")
        for host_data in self.get_data():
            if not host_data.get_name():  # check that host has been specified.
                raise StandardError("must specify host to enumerate local groups.")

        for host_data in self.get_data():
            auth = self._get_auth()  # Get the authenticator, it is required so raise an error if it doesn't exist.
            if auth is None:
                raise StandardError("authentication controller doesn't exist.")
            else:
                # Enumerate local groups for the specified host.
                group_enumerator = LocalGroupEnumerationController(auth=auth)
                group_enumerator.enumerate()
                for group in group_enumerator.get_groups():
                    host_data.set_group(**group)  # Add the enumerated local groups to the data structure.

        self._auth.destroy_sessions()  # Deauthenticate from host.

    def enumerate_local_group_members(self):
        """Enumerate local group members."""
        self.log.info("Enumerating local group members...")
        for host_data in self.get_data():
            if not host_data.get_name() and not isinstance(self.get_group_name(), basestring):  # check that host and group have been specified.
                raise StandardError("must specify host and group name to enumerate local group members.")

        for host_data in self.get_data():
            auth = self._get_auth()  # Get the authenticator, it is required so raise an error if it doesn't exist.
            if auth is None:
                raise StandardError("authentication controller doesn't exist.")
            else:
                host_data.set_group(name=self.get_group_name())
                for group in host_data.get_groups():
                    # For each group of each host that we know about. Enumerate the members of the group.
                    group_member_enumerator = LocalGroupMemberEnumerationController(auth=auth, group=group.get_group_name())
                    group_member_enumerator.enumerate()
                    for member in group_member_enumerator.get_members():
                        group.set_member(member)  # Add the enumerated members to the data structure.

        self._auth.destroy_sessions()  # Deauthenticate from host.

    def enumerate_users(self):
        """Enumerate user accounts."""
        self.log.info("Enumerating local users...")
        for host_data in self.get_data():
            if not host_data.get_name():  # check that host has been specified.
                raise StandardError("must specify host to enumerate local user account information.")

        for host_data in self.get_data():
            auth = self._get_auth()  # Get the authenticator, it is required so raise an error if it doesn't exist.
            if auth is None:
                raise StandardError("authentication controller doesn't exist.")
            else:
                # Enumerate users for the specified host.
                user_enumerator = UserEnumerationController(auth=auth)
                user_enumerator.enumerate()
                for user in user_enumerator.get_info():
                    host_data.set_user(**user)  # Add enumerated users to the data structure.

        self._auth.destroy_sessions()  # Deauthenticate from host.

    def enumerate_user_info(self):
        """Enumerate user account information."""
        self.log.info("Enumerating local user account information...")
        for host_data in self.get_data():
            if not host_data.get_name() and not isinstance(self.get_user(), basestring):  # check that host and username have been specified.
                raise StandardError("must specify host to enumerate local user account information.")

        for host_data in self.get_data():
            auth = self._get_auth()  # Get the authenticator, it is required so raise an error if it doesn't exist.
            if auth is None:
                raise StandardError("authentication controller doesn't exist.")
            else:
                # Enumerate the specified user's account information from the specified host.
                user_info_enumerator = UserInfoEnumerationController(user=self.get_user(), auth=auth)
                user_info_enumerator.enumerate()
                host_data.set_user(**user_info_enumerator.get_info())  # Add enumerated users to the data structure.

        self._auth.destroy_sessions()  # Deauthenticate from host.

    def enumerate_group_membership(self):
        """Enumerate the groups a user is a member of."""
        self.log.info("Enumerating local user's group membership...")
        for host_data in self.get_data():
            if not host_data.get_name() and not isinstance(self.get_user(), basestring):  # check that host and user have been specified.
                raise StandardError("must specify host to enumerate local user account information.")

        for host_data in self.get_data():
            auth = self._get_auth()  # Get the authenticator, it is required so raise an error if it doesn't exist.
            if auth is None:
                raise StandardError("authentication controller doesn't exist.")
            else:
                # Enumerate the local groups of which the specified user is a member.
                membership_enumerator = LocalGroupMembershipEnumerationController(user=self.get_user(), auth=auth)
                membership_enumerator.enumerate()
                host_data.set_user(name=self.get_user(), groups=membership_enumerator.get_groups())  # Add the enumerated groups to the data structure.

        self._auth.destroy_sessions()  # Deauthenticate from host.

if __name__ == '__main__':
    logging.basicConfig(format='%(levelname)s - %(module)s: %(message)s', level=logging.DEBUG)
