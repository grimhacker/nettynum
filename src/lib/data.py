'''
.       .1111...          | Title: data.py
    .10000000000011.   .. | Author: Oliver Morton
 .00              000...  | Email: grimhacker@grimhacker.com
1                  01..   | Description:
                    ..    | Data objects.
                   ..     |
GrimHacker        ..      |
                 ..       |
grimhacker.com  ..        |
@grimhacker    ..         | Created on: 8 Jan 2014
----------------------------------------------------------------------------
Nettynum - A Windows Domain Enumeration Tool
    Copyright (C) 2014  Oliver Morton

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
import logging


class Domain(object):
    """Object that holds domain data."""
    def __init__(self, domain_name):
        self.log = logging.getLogger(__name__)
        self.set_domain_name(domain_name)
        self._domain_controllers = []
        self._groups = []
        self._users = []
        self._policies = {'force_logoff': "",
                         'min_passwd_len': "",
                         'max_passwd_age': "",
                         'password_hist_len': "",
                         'min_passwd_age': "",
                         'lockout_observation_window': "",
                         'lockout_duration': "",
                         'lockout_threshold': "",
                         'complexity': ""}

    def set_domain_name(self, domain_name):
        """Set domain name of domain."""
        if isinstance(domain_name, basestring):
            self._domain_name = domain_name
        else:
            raise StandardError("domain_name must be a string.")

    def get_domain_name(self):
        """Return domain name of domain."""
        return self._domain_name

    def set_domain_controller(self, dc_ip="", dc_fqdn="", dc_nb=""):
        """Append domain controller to list of domain controllers."""
        dc = {'dc_ip': dc_ip, 'dc_fqdn': dc_fqdn, 'dc_nb': dc_nb}
        if dc not in self._domain_controllers:
            self._domain_controllers.append(dc)
        else:
            self.log.debug("Ignoring duplicate domain controller: '{0}' / '{1}' / '{2}' ".format(dc_ip, dc_fqdn, dc_nb))

    def get_domain_controllers(self):
        """Return list of dictionaries of domain controllers."""
        return self._domain_controllers

    def set_group(self, name="", comment="", members=[]):
        """Append group object to list of groups."""
        if name.lower() not in (group.get_group_name().lower() for group in self.get_groups()):
            # If the name of the group is not in the list of known groups, create a new group object and add it to the list.
            try:
                group = Group(name, comment, members)
            except Exception as e:
                self.log.warning("Failed to create group object. {0}".format(e))
            else:
                try:
                    self._groups.append(group)
                except Exception as e:
                    self.log.warning("Failed to append group object to list of groups. {0}".format(e))

    def get_groups(self):
        """Return list of group objects."""
        return self._groups

    def set_policy(self,
                    force_logoff="",
                    min_passwd_len="",
                    max_passwd_age="",
                    password_hist_len="",
                    min_passwd_age="",
                    lockout_observation_window="",
                    lockout_duration="",
                    lockout_threshold="",
                    complexity=""):
        """Add policy to dictionary."""
        try:
            raw_policy = locals()  # create dictionary of local variables (this includes 'self')
        except Exception as e:
            self.log.warning("Error converting policy information to dictionary. {0}".format(e))
        else:
            try:
                del raw_policy['self']
            except KeyError:
                self.log.warning("'self' not in dictionary.")
            else:
                for key in raw_policy.keys():
                    if raw_policy[key] == "":
                        pass  # Ignore default blanks - actual policies will never be blank.
                    else:
                        if key not in self._policies.keys() or self._policies[key] == "":
                            self._policies[key] = raw_policy[key]
                            self.log.debug("policy {0}: {1}".format(key, raw_policy[key]))
                        elif self._policies[key] == "":
                            self._policies[key] = raw_policy[key]
                            self.log.debug("policy {0}: {1}".format(key, raw_policy[key]))
                        else:
                            self.log.debug("ignoring duplicate policy {0}: {1}".format(key, raw_policy[key]))

    def get_policy(self):
        """Return dictionary of policies."""
        return self._policies

    def set_user(self, **kwargs):
            """Add user information to list of users as an instance of User object."""
            try:
                tmp = User(**kwargs)
            except Exception as e:
                self.log.warning("Error creating User object: {0}".format(e))
            else:
                if tmp not in self.get_users():
                    try:
                        self._users.append(tmp)
                    except Exception as e:
                        self.log.warning("Error appending user to list. {0}".format(e))
                    else:
                        pass
                else:
                    self.log.debug("Ignoring duplicate account information for user: {0}".format(kwargs['name']))

    def get_users(self):
        """Return list of dictionaries of user account information."""
        return self._users


class Group(object):
    """Object that holds group data."""
    def __init__(self, name="", comment="", members=None):
        self.log = logging.getLogger(__name__)
        self._group_name = name
        self._group_comment = comment
        self._members = members or []

    def set_group_name(self, name):
        """Set group name of group object."""
        if isinstance(name, basestring):
            self._group_name = name
        else:
            raise StandardError("name must be a string.")

    def get_group_name(self):
        """Return group name."""
        return self._group_name

    def set_group_comment(self, comment):
        """Set group comment of group object."""
        if isinstance(comment, basestring):
            self._group_comment = comment
        else:
            raise StandardError("comment must be a string.")

    def get_group_comment(self):
        """Return group comment."""
        return self._group_comment

    def set_member(self, name):
        """Add name to list of members if it is no already known."""
        if name not in self.get_members():
            self._members.append(name)

    def get_members(self):
        """Return a list of members."""
        return self._members


class Host(object):
    """Object that holds host information."""
    def __init__(self, name):
        """Initialise Host object."""
        self.log = logging.getLogger(__name__)
        self.set_host_name(name)
        self._services = []
        self._shares = []
        self._groups = []
        self._users = []
        self._policies = {'force_logoff': "",
                         'min_passwd_len': "",
                         'max_passwd_age': "",
                         'password_hist_len': "",
                         'min_passwd_age': "",
                         'lockout_observation_window': "",
                         'lockout_duration': "",
                         'lockout_threshold': "",
                         'complexity': ""}

    def set_host_name(self, name):
        """Set domain name of domain."""
        if isinstance(name, basestring):
            self._name = name
        else:
            raise StandardError("name must be a string.")

    def get_name(self):
        """Return host name."""
        return self._name

    def set_group(self, name="", comment="", members=[]):
        """Append group object to list of groups."""
        if name.lower() not in (group.get_group_name().lower() for group in self.get_groups()):
            # If the name of the group is not in the list of known groups, create a new group object and add it to the list.
            try:
                group = Group(name, comment, members)
            except Exception as e:
                self.log.warning("Failed to create group object. {0}".format(e))
            else:
                try:
                    self._groups.append(group)
                except Exception as e:
                    self.log.warning("Failed to append group object to list of groups. {0}".format(e))

    def get_groups(self):
        """Return list of group objects."""
        return self._groups

    def set_policy(self,
                    force_logoff="",
                    min_passwd_len="",
                    max_passwd_age="",
                    password_hist_len="",
                    min_passwd_age="",
                    lockout_observation_window="",
                    lockout_duration="",
                    lockout_threshold="",
                    complexity=""):
        """Add policy to dictionary."""
        try:
            raw_policy = locals()  # create dictionary of local variables (this includes 'self')
        except Exception as e:
            self.log.warning("Error converting policy information to dictionary. {0}".format(e))
        else:
            try:
                del raw_policy['self']
            except KeyError:
                self.log.warning("'self' not in dictionary.")
            else:
                for key in raw_policy.keys():
                    if raw_policy[key] == "":
                        pass  # Ignore default blanks - actual policies will never be blank.
                    else:
                        if key not in self._policies.keys() or self._policies[key] == "":
                            self._policies[key] = raw_policy[key]
                            self.log.debug("policy {0}: {1}".format(key, raw_policy[key]))
                        elif self._policy[key] == "":
                            self._policy[key] = raw_policy[key]
                            self.log.debug("policy {0}: {1}".format(key, raw_policy[key]))
                        else:
                            self.log.debug("ignoring duplicate policy {0}: {1}".format(key, raw_policy[key]))

    def get_policy(self):
        """Return dictionary of policies."""
        return self._policies

    def set_share(self, name, remark, type_):
        """Add dictionary containing share information to list if not known."""
        share = {'name': name, 'remark': remark, 'type_': type_}
        if share not in self.get_shares():
            self._shares.append(share)

    def get_shares(self):
        """Return list of dictionaries containing shares information."""
        return self._shares

    def set_service(self, name, port):
        """Add service to list of services if it is not already known."""
        service = {'srv_name': name, 'srv_port': port}
        if service not in self._services:
            self._services.append(service)

    def get_services(self):
        """Return a list of services as dictionaries with keys name and port."""
        return self._services

    def set_user(self, **kwargs):
        """Add user information to list of users as an instance of User object."""
        try:
            tmp = User(**kwargs)
        except Exception as e:
            self.log.warning("Error creating User object: {0}".format(e))
        else:
            if tmp not in self.get_users():
                try:
                    self._users.append(tmp)
                except Exception as e:
                    self.log.warning("Error appending user to list. {0}".format(e))
                else:
                    pass
            else:
                self.log.debug("Ignoring duplicate account information for user: {0}".format(kwargs['name']))

    def get_users(self):
        """Return list of dictionaries of user account information."""
        return self._users


class User(object):
    def __init__(self,
                 comment='',
                 workstations='',
                 country_code='',
                 last_logon='',
                 password_expired='',
                 full_name='',
                 parms='',
                 code_page='',
                 priv='',
                 auth_flags='',
                 logon_server='',
                 home_dir='',
                 home_dir_drive='',
                 usr_comment='',
                 profile='',
                 acct_expires='',
                 primary_group_id='',
                 bad_pw_count='',
                 user_id='',
                 logon_hours='',
                 password='',
                 units_per_week='',
                 last_logoff='',
                 name='',
                 max_storage='',
                 num_logons='',
                 password_age='',
                 flags='',
                 script_path='',
                 groups = None):
        """Initialise User object."""
        self.log = logging.getLogger(__name__)
        self._comment = comment
        self._workstations = workstations
        self._country_code = country_code
        self._last_logon = last_logon
        self._password_expired = password_expired
        self._full_name = full_name
        self._parms = parms
        self._code_page = code_page
        self._priv = priv
        self._auth_flags = auth_flags
        self._logon_server = logon_server
        self._home_dir = home_dir
        self._home_dir_drive = home_dir_drive
        self._usr_comment = usr_comment
        self._profile = profile
        self._acct_expires = acct_expires
        self._primary_group_id = primary_group_id
        self._bad_pw_count = bad_pw_count
        self._user_id = user_id
        self._logon_hours = logon_hours
        self._password = password
        self._units_per_week = units_per_week
        self._last_logoff = last_logoff
        self._name = name
        self._max_storage = max_storage
        self._num_logons = num_logons
        self._password_age = password_age
        self._flags = flags
        self._script_path = script_path
        self._groups = groups or []

    def __eq__(self, other):
        """Compare based on dict. Overrides Object.__eq__"""
        return self.__dict__ == other.__dict__

    def get_user_dict(self):
        """Return dictionary representation of data."""
        user = {}
        user['comment'] = self.comment
        user['workstations'] = self._workstations
        user['country_code'] = self._country_code
        user['last_logon'] = self._last_logon
        user['password_expired'] = self._password_expired
        user['full_name'] = self._full_name
        user['parms'] = self._parms
        user['code_page'] = self._code_page
        user['priv'] = self._priv
        user['auth_flags'] = self._auth_flags
        user['logon_server'] = self._logon_server
        user['home_dir'] = self._home_dir
        user['home_dir_drive'] = self._home_dir_drive
        user['usr_comment'] = self._usr_comment
        user['profile'] = self._profile
        user['acct_expires'] = self._acct_expires
        user['primary_group_id'] = self._primary_group_id
        user['bad_pw_count'] = self._bad_pw_count
        user['user_id'] = self._user_id
        user['logon_hours'] = self._logon_hours
        user['password'] = self._password
        user['units_per_week'] = self._units_per_week
        user['last_logoff'] = self._last_logoff
        user['name'] = self._name
        user['max_storage'] = self._max_storage
        user['num_logons'] = self._num_logons
        user['password_age'] = self._password_age
        user['flags'] = self._flags
        user['script_path'] = self._script_path
        user['groups'] = self._groups
        return user

    def set_comment(self, comment):
        """Assign user comment."""
        self._comment = comment

    def set_workstations(self, workstations):
        """Assign user workstations."""
        self._workstations = workstations

    def set_country_code(self, country_code):
        """Assign user country_code."""
        self._country_code = country_code

    def set_last_logon(self, last_logon):
        """Assign user last_logon."""
        self._last_logon = last_logon

    def set_password_expired(self, password_expired):
        """Assign user password_expired."""
        self._password_expired = password_expired

    def set_full_name(self, full_name):
        """Assign user full_name."""
        self._full_name = full_name

    def set_params(self, params):
        """Assign user params."""
        self._params = params

    def set_code_page(self, code_page):
        """Assign user code_page."""
        self._code_page = code_page

    def set_priv(self, priv):
        """Assign user priv."""
        self._priv = priv

    def set_auth_flags(self, auth_flags):
        """Assign user auth_flags."""
        self._auth_flags = auth_flags

    def set_logon_server(self, logon_server):
        """Assign user logon_server."""
        self._logon_server = logon_server

    def set_home_dir(self, home_dir):
        """Assign user home_dir."""
        self._home_dir = home_dir

    def set_home_dir_drive(self, home_dir_drive):
        """Assign user home_dir_drive."""
        self._home_dir_drive = home_dir_drive

    def set_usr_comment(self, usr_comment):
        """Assign user usr_comment."""
        self._usr_comment = usr_comment

    def set_profile(self, profile):
        """Assign user profile."""
        self._profile = profile

    def set_acct_expires(self, acct_expires):
        """Assign user acct_expires."""
        self._acct_expires = acct_expires

    def set_primary_group_id(self, primary_group_id):
        """Assign user primary_group_id."""
        self._primary_group_id = primary_group_id

    def set_bad_pw_count(self, bad_pw_count):
        """Assign user bad_pw_count."""
        self._bad_pw_count = bad_pw_count

    def set_user_id(self, user_id):
        """Assign user user_id."""
        self._user_id = user_id

    def set_logon_hours(self, logon_hours):
        """Assign user logon_hours."""
        self._logon_hours = logon_hours

    def set_password(self, password):
        """Assign user password."""
        self._password = password

    def set_units_per_week(self, units_per_week):
        """Assign user units_per_week."""
        self._units_per_week = units_per_week

    def set_last_logoff(self, last_logoff):
        """Assign user last_logoff."""
        self._last_logoff = last_logoff

    def set_name(self, name):
        """Assign user name."""
        self._name = name

    def set_max_storage(self, max_storage):
        """Assign user max_storage."""
        self._max_storage = max_storage

    def set_num_logons(self, num_logons):
        """Assign user num_logons."""
        self._num_logons = num_logons

    def set_password_age(self, password_age):
        """Assign user password_age."""
        self._password_age = password_age

    def set_flags(self, flags):
        """Assign user flags."""
        self._flags = flags

    def set_script_path(self, script_path):
        """Assign user script_path."""
        self._script_path = script_path

    def set_group(self, group):
        """Assign user groups."""
        if group not in self.get_groups():
            self._group.append(group)

    def get_comment(self):
        """Return user comment."""
        return self._comment

    def get_workstations(self):
        """Return user workstations."""
        return self._workstations

    def get_country_code(self):
        """Return user country_code."""
        return self._country_code

    def get_last_logon(self):
        """Return user last_logon."""
        return self._last_logon

    def get_password_expired(self):
        """Return user password_expired."""
        return self._password_expired

    def get_full_name(self):
        """Return user full_name."""
        return self._full_name

    def get_params(self):
        """Return user params."""
        return self._params

    def get_code_page(self):
        """Return user code_page."""
        return self._code_page

    def get_priv(self):
        """Return user priv."""
        return self._priv

    def get_auth_flags(self):
        """Return user auth_flags."""
        return self._auth_flags

    def get_logon_server(self):
        """Return user logon_server."""
        return self._logon_server

    def get_home_dir(self):
        """Return user home_dir."""
        return self._home_dir

    def get_home_dir_drive(self):
        """Return user home_dir_drive."""
        return self._home_dir_drive

    def get_usr_comment(self):
        """Return user usr_comment."""
        return self._usr_comment

    def get_profile(self):
        """Return user profile."""
        return self._profile

    def get_acct_expires(self):
        """Return user acct_expires."""
        return self._acct_expires

    def get_primary_group_id(self):
        """Return user primary_group_id."""
        return self._primary_group_id

    def get_bad_pw_count(self):
        """Return user bad_pw_count."""
        return self._bad_pw_count

    def get_user_id(self):
        """Return user user_id."""
        return self._user_id

    def get_logon_hours(self):
        """Return user logon_hours."""
        return self._logon_hours

    def get_password(self):
        """Return user password."""
        return self._password

    def get_units_per_week(self):
        """Return user units_per_week."""
        return self._units_per_week

    def get_last_logoff(self):
        """Return user last_logoff."""
        return self._last_logoff

    def get_name(self):
        """Return user name."""
        return self._name

    def get_max_storage(self):
        """Return user max_storage."""
        return self._max_storage

    def get_num_logons(self):
        """Return user num_logons."""
        return self._num_logons

    def get_password_age(self):
        """Return user password_age."""
        return self._password_age

    def get_flags(self):
        """Return user flags."""
        return self._flags

    def get_script_path(self):
        """Return user script_path."""
        return self._script_path

    def get_groups(self):
        """Return user groups."""
        return self._groups
