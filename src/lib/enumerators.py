'''
.       .1111...          | Title: enumerators.py
    .10000000000011.   .. | Author: Oliver Morton
 .00              000...  | Email: grimhacker@grimhacker.com
1                  01..   | Description:
                    ..    | All enumeration objects.
                   ..     |
GrimHacker        ..      |
                 ..       |
grimhacker.com  ..        |
@grimhacker    ..         |Created on: 22 Oct 2012
----------------------------------------------------------------------------
Nettynum - A Windows Domain Enumeration Tool
    Copyright (C) 2014  Oliver Morton

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
import logging
import socket
import dns.resolver
import dns.reversename
import dns.name
import win32netcon
import win32com.client
import win32net
import win32security
import win32api
import pywintypes
import win32netcon
from pysnmp.entity.rfc3413.oneliner import cmdgen

from authenticators import BaseAuthenticator

#TODO: enumerate domain trusts: DsEnumerateDomainTrusts() not implemented in pywin32
#  enumerate domains with NetServerEnum using DOMAIN_ENUM - observed during testing that this may be possible.


class BaseEnumerator(object):
    """Base enumeration class."""
    def __init__(self, auth=None, **kwargs):
        """Initialise base enumeration class."""
        self.log = logging.getLogger(__name__)
        self._auth = auth

    def enumerate(self):
        pass


class DomainNameEnumerator(BaseEnumerator):
    """Domain name enumerator class."""
    def __init__(self, **kwargs):
        """Initialise domain name enumerator class. Extends BaseEnumerator.__init__()."""
        super(DomainNameEnumerator, self).__init__(**kwargs)
        self._domain_names = []

    def get_domain_names(self):
        """Return a list of discovered domain names"""
        return self._domain_names

    def _set_domain_name(self, name):
        """Add domain name to list of discovered names, if it is not already known."""
        if name not in self._domain_names:
            # If the domain name is not already in self, add it.
            self._domain_names.append(name)
            self.log.debug("Found domain name: '{0}'".format(name))
        else:
            self.log.debug("Ignoring duplicate domain name: '{0}'".format(name))


class FromFQDN(DomainNameEnumerator):
    """Domain name enumerator class which uses the local host's Fully Qualified Domain Name (FQDN)."""
    def __init__(self, **kwargs):
        """Initialise FromFQDN domain name enumerator class. Extends DomainNameEnumerator.__init__()."""
        super(FromFQDN, self).__init__(**kwargs)

    def enumerate(self):
        """Enumerate domain names using socket.fqdn().
        Overrides DomainNameEnumerator.enumerate()."""
        self.log.debug("enumerating domain names using socket.fqdn()")
        try:
            fqdn = socket.getfqdn()
        except Exception as e:
            self.log.warning("Failed to enumerate domain names using socket.fqdn(). {0}".format(e))
        else:
            if "." in fqdn:  # If the fqdn has a domain name in it there will be a dot.
                try:
                    name = fqdn.partition(".")[2]  # fqdn.partition should return: ("host", ".", "domainname")
                except Exception as e:
                    self.log.warning("Failed to extract domain name from: '{0}'. {1}".format(fqdn, e))
                else:
                    self._set_domain_name(name)  # set domain name


class FromObject(DomainNameEnumerator):
    """Domain name enumerator class which uses the pywin32 com object. Extends DomainNameEnumerator.__init__()."""
    def __init__(self, **kwargs):
        """Initialise FromObject domain name enumerator class."""
        super(FromObject, self).__init__(**kwargs)

    def enumerate(self):
        """Enumerate domain names using win32com.client.GetObject("WinNT:").
        Overrides DomainNameEnumerator.enumerate()."""
        self.log.debug("enumerating domain names using win32com.client.GetObject('WinNT:')")
        try:
            network = win32com.client.GetObject("WinNT:")
        except Exception as e:
            self.log.warning("Failed to enumerate domain names using win32com.client.GetObject('WinNT:'). {0}".format(e))
        else:
            # Extract domain names from structure and set them.
            for group in network:
                name = str(group.name)
                self._set_domain_name(name)


class FromDNSServFQDN(DomainNameEnumerator):
    """Domain name enumerator class which uses the DNS Server's Fully Qualified Domain Name (FQDN)."""
    def __init__(self, **kwargs):
        """Initialise FromDNSServFQDN domain name enumerator class. Extends DomainNameEnumerator.__init__()."""
        super(FromDNSServFQDN, self).__init__(**kwargs)

    def enumerate(self):
        """Enumerate domain names using DNS servers' fully qualified domain name (FQDN).
        Overrides DomainNameEnumerator.enumerate()."""
        self.log.debug("enumerating domain names using DNS servers' FQDN")
        try:
            resolver = dns.resolver.Resolver()  # Create resolver object using dnspython. This will get the DNS servers the machine knows about.
        except Exception as e:
            self.log.warning("Failed to get list of DNS servers. {0}".format(e))
        else:
            for nameserver in resolver.nameservers:  # For each dns server the system knows.
                try:
                    # Do a reverse lookup on the DNS server IP to get its reverse name - this will be <reverse of ip>.in-addr.arpa
                    # Do a PTR type query for this to get the actual fqdn for the dns server.
                    nameserver_answers = dns.resolver.query(dns.reversename.from_address(nameserver), "PTR")
                except Exception as e:
                    self.log.warning("Failed to get FQDN for '{0}'. {1}".format(nameserver, e))
                else:
                    # The answers are a list since there could be multiple responses. Process each answer.
                    for name in nameserver_answers:
                        try:
                            fqdn = name.to_text()  # Extract the fqdn from the dnspython Name object as a string.
                        except Exception as e:
                            self.log.warning("Failed to get convert FQDN to string. {0}".format(e))
                        else:
                            try:
                                # Remove hostname portion of fqdn leaving domain name.
                                domain = '.'.join(fqdn.split(".")[1:])
                            except Exception as e:
                                self.log.warning("Failed to get domain name from FQDN.")
                            else:
                                if domain == "":
                                    pass # Unlikely but possible depending on the DNS response.
                                else:
                                    self._set_domain_name(domain)  # Set the domain name.


class FromMaster(DomainNameEnumerator):
    """Domain name enumerator class which uses the Master Browser."""
    def __init__(self, **kwargs):
        """Initialise FromMaster domain name enumerator class. Extends DomainNameEnumerator.__init__()."""
        super(FromMaster, self).__init__(**kwargs)
        pass  # Not implemented because of performance concerns (will need to contact every ip in a subnet to find the master.)


class DomainNameEnumerationController(DomainNameEnumerator):
    """Run all domain name enumerators."""
    def __init__(self, **kwargs):
        """Initialise DomainNameController class. Extends DomainNameEnumerator.__init__()."""
        super(DomainNameEnumerationController, self).__init__(**kwargs)

    def enumerate(self):
        """Discover domain names using all known methods."""
        # Add an instance of every method for domain name enumeration to a list.
        methods = []
        methods.append(FromFQDN())
        methods.append(FromObject())
        methods.append(FromDNSServFQDN())
        methods.append(FromMaster())

        # Use each method to enumerate domain names.
        # Get the domain names the method has found and set them in self.
        for method in methods:
            method.enumerate()
            for name in method.get_domain_names():
                self._set_domain_name(name)


class DomainControllerEnumerator(BaseEnumerator):
    """Domain controller enumerator class."""
    def __init__(self, **kwargs):
        super(DomainControllerEnumerator, self).__init__(**kwargs)
        self._domain_controllers = []
        try:
            self._domain_name = kwargs['domain_name']
        except:
            raise Exception("Target domain_name required.")
        else:
            if isinstance(self._domain_name, basestring):
                pass
            else:
                raise Exception("Target domain_name must be a string.")

    def get_domain_name(self):
        """Return domain name."""
        return self._domain_name

    def set_domain_name(self, domain_name):
        """Assign domain_name to use for enumeration."""
        self._domain_name = domain_name

    def get_domain_controllers(self):
        """Return list of dictionaries of domain controllers with keys: dc_ip, dc_fqdn, dc_nb."""
        return self._domain_controllers

    def _set_domain_controller(self, dc_ip="", dc_fqdn="", dc_nb=""):
        """Add domain controller to list of discovered names, if it is not already known."""
        if dc_fqdn.endswith("."):
            # Truly fully qualified domain names end in ".", but we don't want this so strip it off.
            dc_fqdn = dc_fqdn[:-1]
        dc = {'dc_ip': dc_ip.lower(), 'dc_fqdn': dc_fqdn.lower(), 'dc_nb': dc_nb.upper()}  # Create a dictionary of the domain controller's names
        if dc not in self._domain_controllers:
            # If the domain controller is not already in self, add it.
            self._domain_controllers.append(dc)
            self.log.debug("Found domain controller: '{0}' / '{1}' / '{2}' for domain: '{3}'".format(dc_ip, dc_fqdn, dc_nb, self.get_domain_name()))
        else:
            self.log.debug("Ignoring duplicate domain controller: '{0}' / '{1}' / '{2}' for domain: '{3}'".format(dc_ip, dc_fqdn, dc_nb, self.get_domain_name()))


class DNSLookup(DomainControllerEnumerator):
    """Domain controller enumerator class which uses DNS records"""
    def __init__(self, **kwargs):
        """Initialise DNSLookup domain controller enumerator class. Extends DomainControllerEnumerator.__init__()."""
        super(DNSLookup, self).__init__(**kwargs)

    def _lookup_a(self):
        """Lookup DNS records.."""
        self.log.debug("enumerating domain controllers from domain name A records.")
        try:
            # Use dnspython to lookup A records for the domain name. Windows points these to the domain controller.
            a_answers = dns.resolver.query(self.get_domain_name(), "A")
        except Exception as e:
            self.log.warning("Failed to lookup records. {0}".format(e))
        else:
            # There could be several so loop over the list.
            for ip in a_answers:
                try:
                    # Do a reverse lookup on the ip address to get its reverse name - this will be <reverse of ip>.in-addr.arpa
                    # Do a PTR type query for this to get the actual fqdn of the domain controller.
                    ptr_answers = dns.resolver.query(dns.reversename.from_address(ip.to_text()), "PTR")  # need to query reverse name to get real name. type is pointer.
                except Exception as e:
                    self.log.warning("Failed to lookup name from IP address. {0}".format(e))
                    self._set_domain_controller(dc_ip=ip.to_text())  # If we failed to get the fqdn, add the ip address anyway.
                else:
                    # There could be several so loop over the list.
                    for name in ptr_answers:
                        try:
                            ip_str = ip.to_text()  # Extract the ip address from the dnspython Name object as a string
                        except Exception as e:
                            self.log.warning("Failed to convert ip to string.")
                            ip_str = ""
                        try:
                            name_str = name.to_text()  # Extract the FQDN from the dnspython Name object as a string
                        except Exception as e:
                            self.log.warning("Failed to convert name to string.")
                            name_str = ""
                        self._set_domain_controller(dc_ip=ip_str, dc_fqdn=name_str)  # Set the domain controller's names.

    def _lookup_srv(self):
        """Lookup SRV records for domain controller."""
        self.log.debug("enumerating domain controllers from SRV records.")
        services = ["_kerberos._tcp.dc._msdcs", "_ldap._tcp.dc._msdcs", "_ldap._tcp.gc._msdcs"]  # Services that by default are set to point to the domain controller.
        for service in services:
            srv = "{0}.{1}".format(service, self.get_domain_name())  # Construct the service fqdn.
            self.log.debug("Looking up: '{0}' records for '{1}'".format(service, self.get_domain_name()))
            try:
                # Use dnspython to do a SRV type query for the service.
                srv_answers = dns.resolver.query(srv, "SRV")
            except Exception as e:
                self.log.warning("Failed to lookup records. {0}".format(e))
            else:
                # There could be several so loop over the list.
                for srv_answer in srv_answers:
                    try:
                        fqdn = srv_answer.to_text().split(" ")[3]  # Responses have several fields separated by spaces. Get the fqdn.
                    except Exception as e:
                        self.log.warning("Failed to convert srv_answer. {0}".format(e))
                    else:
                        try:
                            # Use dnspython to lookup A records for the fqdn (of the host running the service) in order to get its ip address.
                            ip_answers = dns.resolver.query(fqdn, "A")
                        except Exception as e:
                            self.log.warning("Failed to lookup records. {0}".format(e))
                            self._set_domain_controller(dc_fqdn=fqdn)  # If fail to get the ip address, set the fqdn anyway.
                        else:
                            # There could be several so loop over the list.
                            for ip_answer in ip_answers:
                                try:
                                    ip = ip_answer.to_text()  # Extract the ip address from the dnspython Name object as a string
                                except Exception as e:
                                    self.log.warning("Failed to convert ip_answer. {0}".format(e))
                                    ip = ""
                                finally:
                                    self._set_domain_controller(dc_ip=ip, dc_fqdn=fqdn)  # Set the domain controllers name and ip.

    def enumerate(self):
        """Enumerate domain controllers for domain name using DNS records.
        Overrides DomainControllerEnumerator.enumerate()."""
        # Two methods of finding domain controllers using dns records. Do each in turn.
        self._lookup_a()
        self._lookup_srv()


class NetGetDC(DomainControllerEnumerator):
    """Domain controller enumerator class which uses pywin32 NetGetDC()."""
    def __init__(self, **kwargs):
        """Initialise NetGetDC domain controller enumerator class. Extends DomainControllerEnumerator.__init__()."""
        super(NetGetDC, self).__init__(**kwargs)

    def enumerate(self):
        """Enumerate domain controllers for domain name using NetGetDC().
        Overrides DomainControllerEnumerator.enumerate()."""
        self.log.debug("enumerating domain controllers using NetGetDC().")
        name = ""
        ip = ""
        fqdn = ""
        try:
            # Use pywin32 to lookup domain controllers for the NetBIOS style domain name.
            # Returned with "\\" at the start of the name, we don't want that so strip it off.
            name = win32net.NetGetDCName("", self.get_domain_name())[2:]
        except Exception as e:
            self.log.warning("Failed to enumerate domain controllers for '{0}' using NetGetDC(). {1}".format(self.get_domain_name(), e))
        else:
            try:
                # Use a socket to get the IP address of the host using the NetBIOS name.
                ip = socket.gethostbyname(name)
            except Exception as e:
                self.log.warning("Failed to get IP address from name. {0}".format(e))
            else:
                try:
                    # Use a socket to get the fqdn of the host from the IP address
                    #fqdn = socket.gethostbyaddr(ip)[0]  # Returns a triple (hostname, aliaslist, ipaddrlist). Hostname should be fqdn so get that.
                    fqdn = socket.getfqdn(ip)  # returns a string of the fqdn.
                    # TODO: should use socket.getfqdn(ip) as this will first check the hostname portion then go through the aliaslist.
                except Exception as e:
                    self.log.warning("Failed to get FQDN from IP address. {0}".format(e))
            self._set_domain_controller(dc_ip=ip, dc_fqdn=fqdn, dc_nb=name)  # Set the domain controller to self.


class DsGetDCName(DomainControllerEnumerator):
    """Domain controller enumerator class which uses pywin32 DsGetDCName()."""
    def __init__(self, **kwargs):
        """Initialise DsGetDCName domain controller enumerator class. Extends DomainControllerEnumerator.__init__()."""
        super(DsGetDCName, self).__init__(**kwargs)

    def enumerate(self):
        """Enumerate domain controllers for domain name using DsGetDCName().
        Overrides DomainControllerEnumerator.enumerate()."""
        self.log.debug("enumerating domain controllers using DsGetDCName().")
        name = ""
        ip = ""
        fqdn = ""
        try:
            # Use pywin32 to lookup domain controllers for DNS style domain name.
            domain_controller_info = win32security.DsGetDcName(domainName=self.get_domain_name())
        except Exception as e:
            self.log.warning("Failed to enumerate domain names using NetGetDC(). {0}".format(e))
        else:
            if "." in domain_controller_info['DomainControllerName']:
                # DNS Style.
                fqdn = domain_controller_info['DomainControllerName'][2:]  # Remove \\ from start of the name.
            else:
                # NetBIOS style.
                name = domain_controller_info['DomainControllerName'][2:]  # Remove \\ from start of the name.
            if domain_controller_info['DomainControllerAddressType'] == 1:
                # DomainControllerAddress is a string IP address (DS_INET_ADDRESS).
                ip = domain_controller_info['DomainControllerAddress'][2:]  # Remove \\ from start of the address.
            else:
                # DomainControllerAddress is a NetBIOS name (DS_NETBIOS_ADDRESS).
                # If name is still "" at this point it will be assigned from domain_controller_info['DomainControllerAddress']
                # but if it has already been assigned from domain_controller_info['DomainControllerName'] it will not be changed.
                name = name or domain_controller_info['DomainControllerAddress'][2:]  # Remove \\ from start of the name.
                try:
                    # Use socket to get the ip address from the name.
                    ip = socket.gethostbyname(name)
                except Exception as e:
                    self.log.warning("Failed to get IP from FQDN. {0}".format(e))

            self._set_domain_controller(dc_ip=ip, dc_fqdn=fqdn, dc_nb=name)  # Set the domain controller to self.


class DomainControllerEnumerationController(DomainControllerEnumerator):
    """Run all domain controller enumerators."""
    def __init__(self, **kwargs):
        """Initialise DomainControllerEnumerationController class. Extends DomainControllerEnumerator.__init__()."""
        super(DomainControllerEnumerationController, self).__init__(**kwargs)
        self._kwargs = kwargs

    def enumerate(self):
        """Discover domain controllers using all known methods."""
        # Add an instance of every method for enumerating domain controllers to the list passing it the keyword arguments.
        methods = []
        if "."  in self.get_domain_name():
            # Methods that ONLY work properly for DNS style.
            self.log.debug("domain name: '{0}' appears to be DNS style.".format(self.get_domain_name()))
            methods.append(DNSLookup(**self._kwargs))
        else:
            # Methods that ONLY work properly for NetBIOS style.
            self.log.debug("domain name: '{0}' appears to be NetBIOS Style".format(self.get_domain_name()))
            methods.append(NetGetDC(**self._kwargs))
        # Methods that work properly with NetBIOS or DNS style.
        methods.append(DsGetDCName(**self._kwargs))

        # Use each method to enumerate domain controllers and then assign them to self.
        # Duplicates from the different methods are removed because _set_domain_controller checks the list before adding.
        for method in methods:
            method.enumerate()
            for name in method.get_domain_controllers():
                self._set_domain_controller(**name)


class GroupEnumerator(BaseEnumerator):
    """Group enumerator class."""
    def __init__(self, **kwargs):
        """Initialise group enumerator class. Extends BaseEnumerator.__init__()."""
        super(GroupEnumerator, self).__init__(**kwargs)
        self._groups = []        
        try:
            if isinstance(kwargs['auth'], BaseAuthenticator):
                self.auth = kwargs['auth']
                try:
                    self._host = self.auth.get_host()
                except:
                    raise Exception("Target host required from auth object.")
                else:
                    if isinstance(self._host, basestring):
                        pass
                    else:
                        raise Exception("Target host must be a string.")
            else:
                raise Exception("auth must be an instance of BaseAuthenticator.")
        except Exception as e:
            raise Exception("An authenticator is required.")

    def _get_host(self):
        """Return domain controller."""
        return self._host

    def get_groups(self):
        """Return list of groups as dictionaries in form {name, comment}"""
        return self._groups

    def _set_group(self, name, comment):
        """Append a group with name and comment to self._groups"""
        try:
            group = {'name': str(name), 'comment': str(comment)}
        except Exception as e:
            self.log.warning("Error creating group dictionary. {0}".format(e))
        else:
            if group not in self.get_groups():
                try:
                    self._groups.append(group)
                except Exception as e:
                    self.log.warning("Error appending group to list. {0}".format(e))
                else:
                    self.log.debug("Found group: '{0}' with comment: '{1}'".format(group['name'], group['comment']))


class NetGroupEnum(GroupEnumerator):
    """Domain group enumerator which uses pywin32 NetGroupEnum()."""
    def __init__(self, **kwargs):
        """Initialise NetGroupEnum group enumerator class. Extends GroupEnumerator.__init__()."""
        super(NetGroupEnum, self).__init__(**kwargs)

    def enumerate(self):
        """Enumerate domain groups on specified host using NetGroupEnum().
        Overrides GroupEnumerator.enumerate()."""
        if self._auth.authenticate("smb"):  # Authenticate using SMB.
            self.log.debug("enumerating groups on '{0}' using NetGroupEnum().".format(self._get_host()))
            resume = 0  # Flag the Windows API uses to indicate if there is more data to return.
            while True:
                try:
                    # Use pywin32 to get the domain groups on the host.
                    group_data, total, resume = win32net.NetGroupEnum(self._get_host(), 1, resume)
                except Exception as e:
                    self.log.warning("Failed to enumerate groups with NetGroupEnum(). {0}".format(e))
                    break  # Break out of infinite loop if there was an error.
                else:
                    for group in group_data:  # For each group returned
                        try:
                            # Extract the group name and comment and set to self.
                            self._set_group(group['name'], group['comment'])
                        except Exception as e:
                            self.log.warning("Error extracting group details. {0}".format(e))
                    if not resume:
                        break  # If the Windows API indicates there is no more data, break out of the infinite loop.
            self._auth.deauthenticate()  # Deauthenticate from the host.
        else:
            self.log.debug("smb authentication failed. skipping NetGroupEnum() group enumeration on '{0}'".format(self._get_host()))


class NetLocalGroupEnum(GroupEnumerator):
    """Local group enumerator which uses pywin32 NetLocalGroupEnum()."""
    def __init__(self, **kwargs):
        """Initialise NetLocalGroupEnum group enumerator class. Extends GroupEnumerator.__init__()."""
        super(NetLocalGroupEnum, self).__init__(**kwargs)

    def enumerate(self):
        """Enumerate local groups on specified host using NetLocalGroupEnum().
        Overrides GroupEnumerator.enumerate()."""
        if self._auth.authenticate("smb"):  # Authenticate using SMB.
            self.log.debug("enumerating local groups on '{0}' using NetLocalGroupEnum().".format(self._get_host()))
            resume = 0  # Flag the Windows API uses to indicate if there is more data to return.
            while True:
                try:
                    # Use pywin32 to get the local groups on the host.
                    group_data, total, resume = win32net.NetLocalGroupEnum(self._get_host(), 1, resume)
                except Exception as e:
                    self.log.warning("Error enumerating local groups. {0}".format(e))
                    break
                else:
                    for group in group_data:  # For each group returned
                        try:
                            # Extract the group name and comment and set to self.
                            self._set_group(group['name'], group['comment'])
                        except Exception as e:
                            self.log.warning("Error extracting group details. {0}".format(e))
                    if not resume:
                        break  # If the Windows API indicates there is no more data, break out of the infinite loop.
            self._auth.deauthenticate()  # Deauthenticate from the host.
        else:
            self.log.debug("smb authentication failed. skipping NetLocalGroupEnum() local group enumeration on '{0}'".format(self._get_host()))


class GroupEnumerationController(GroupEnumerator):
    """Run all domain group enumerators."""
    def __init__(self, **kwargs):
        """Initialise GroupEnumerationController class. Extends GropuEnumerator.__init__()."""
        super(GroupEnumerationController, self).__init__(**kwargs)
        self._kwargs = kwargs

    def enumerate(self):
        """Discover domain groups using all known methods."""
        # Add an instance of every method for enumerating domain groups to the list passing it the keyword arguments.
        methods = []
        methods.append(NetGroupEnum(**self._kwargs))

        # Enumerate with each method and set the groups to self.
        # Duplicates are removed by _set_group.
        for method in methods:
            method.enumerate()
            for group in method.get_groups():
                self._set_group(**group)


class LocalGroupEnumerationController(GroupEnumerator):
    """Run all local group enumerators."""
    def __init__(self, **kwargs):
        """Initialise LocalGroupEnumerationController class. Extends GropuEnumerator.__init__()."""
        super(LocalGroupEnumerationController, self).__init__(**kwargs)
        self._kwargs = kwargs

    def enumerate(self):
        """Discover local groups using all known methods."""
        # Add an instance of every method for enumerating local groups to the list passing it the keyword arguments.
        methods = []
        methods.append(NetLocalGroupEnum(**self._kwargs))

        # Enumerate with each method and set the groups to self.
        # Duplicates are removed by _set_group.
        for method in methods:
            method.enumerate()
            for group in method.get_groups():
                self._set_group(**group)


class GroupMemberEnumerator(BaseEnumerator):
    """Group member enumerator."""
    def __init__(self, **kwargs):
        """Initialise group member enumerator class. Extends BaseEnumerator.__init__()."""
        super(GroupMemberEnumerator, self).__init__(**kwargs)
        self._members = []
        try:
            self._group = kwargs['group']
        except:
            raise Exception("Target group required.")
        else:
            if isinstance(self._group, basestring):
                pass
            else:
                raise Exception("Target group must be a string.")
        try:
            if isinstance(kwargs['auth'], BaseAuthenticator):
                self.auth = kwargs['auth']
                try:
                    self._host = self.auth.get_host()
                except:
                    raise Exception("Target host required from auth object.")
                else:
                    if isinstance(self._host, basestring):
                        pass
                    else:
                        raise Exception("Target host must be a string.")
            else:
                raise Exception("auth must be an instance of BaseAuthenticator.")
        except Exception as e:
            raise Exception("An authenticator is required.")

    def _get_host(self):
        """Return host."""
        return self._host

    def get_group(self):
        """Return group."""
        return self._group

    def set_group(self, group):
        """Assign group to use for enumeration."""
        self._group = group

    def get_members(self):
        """Return list of group members"""
        return self._members

    def _set_member(self, member):
        """Append member to self._member."""
        if str(member) not in self.get_members():
            # If the member isn't already in the list, add it.
            try:
                self._members.append(str(member))
            except Exception as e:
                self.log.warning("Error appending member to list. {0}".format(e))
            else:
                self.log.debug("Found member: '{0}'".format(str(member)))


class NetGroupGetUsers(GroupMemberEnumerator):
    """Domain group member enumerator which uses pywin32 NetGroupGetUsers()."""
    def __init__(self, **kwargs):
        """Initialise NetGroupGetUsers group enumerator class. Extends GroupMemberEnumerator.__init__()."""
        super(NetGroupGetUsers, self).__init__(**kwargs)

    def enumerate(self):
        """Enumerate members of specified group on specified host using NetGroupGetUsers().
        Overrides GroupEnumerator.enumerate()."""
        if self._auth.authenticate("smb"):  # Authenticate using SMB
            self.log.debug("enumerating members of group '{0}' on '{1}' using NetGroupGetUsers().".format(self.get_group(), self._get_host()))
            resume = 0  # Flag the Windows API uses to indicate if there is more data to return.
            while True:
                try:
                    # Use pywin32 to get the members of the domain group on the host.
                    member_data, total, resume = win32net.NetGroupGetUsers(self._get_host(), self.get_group(), 0, resume)
                except Exception as e:
                    self.log.warning("Error enumerating domain group members. {0}".format(e))
                    break
                else:
                    for member in member_data:
                        # For each member extract the name and set to self.
                        try:
                            self._set_member(member['name'])
                        except Exception as e:
                            self.log.warning("Error extracting member details. {0}".format(e))
                            break  # If there was an error, break out of the infinite loop
                    if not resume:
                        break  # Windows API indicates there is no more data, break out of the infinite loop.
            self._auth.deauthenticate()  # Deauthenticate from the host.
        else:
            self.log.debug("smb authentication failed. skipping NetGroupGetUsers() group member enumeration for group '{0}' on '{1}'".format(self.get_group(), self._get_host()))


class GroupMemberEnumerationController(GroupMemberEnumerator):
    """Run all domain group member enumerators."""
    def __init__(self, **kwargs):
        """Initialise GroupMemberEnumerationController class. Extends GropuEnumerator.__init__()."""
        super(GroupMemberEnumerationController, self).__init__(**kwargs)
        self._kwargs = kwargs

    def enumerate(self):
        """Discover domain group members using all known methods."""
        # Add an instance of every method for enumerating domain group members to the list passing it the keyword arguments.
        methods = []
        methods.append(NetGroupGetUsers(**self._kwargs))

        # Enumerate with each method and set the members to self.
        # Duplicates are removed by _set_member.
        for method in methods:
            method.enumerate()
            for member in method.get_members():
                self._set_member(member)


class NetLocalGroupGetMembers(GroupMemberEnumerator):
    """Local group member enumerator which uses pywin32 NetLocalGroupGetUsers()."""
    def __init__(self, **kwargs):
        """Initialise NetLocalGroupGetUsers group enumerator class. Extends GroupMemberEnumerator.__init__()."""
        super(NetLocalGroupGetMembers, self).__init__(**kwargs)

    def enumerate(self):
        """Enumerate members of specified local group on specified host using NetLocalGroupGetMembers().
        Overrides GroupEnumerator.enumerate()."""
        if self._auth.authenticate("smb"):  # Authenticate using SMB
            self.log.debug("enumerating members of local group '{0}' on '{1}' using NetLocalGroupGetMembers().".format(self.get_group(), self._get_host()))
            resume = 0  # Flag the Windows API uses to indicate if there is more data to return.
            while True:
                try:
                    # Use pywin32 to get the members of the local group on the host.
                    member_data, total, resume = win32net.NetLocalGroupGetMembers(self._get_host(), self.get_group(), 1, resume)
                except Exception as e:
                    self.log.warning("Error enumerating local group members. {0}".format(e))
                    break  # If there was an error, break out of the infinite loop.
                else:
                    for member in member_data:
                        # For each member, extract the name and set to self.
                        try:
                            self._set_member(member['name'])
                        except Exception as e:
                            self.log.warning("Error extracting member details. {0}".format(e))
                    if not resume:
                        break  # Windows API indicates no more data, break out of the infinite loop.
            self._auth.deauthenticate()  # Deauthenticate from the host.
        else:
            self.log.debug("smb authentication failed. skipping NetLocalGroupGetMembers() local group member enumeration for group '{0}' on '{1}'".format(self.get_group(), self._get_host()))


class LocalGroupMemberEnumerationController(GroupMemberEnumerator):
    """Run all local group member enumerators."""
    def __init__(self, **kwargs):
        """Initialise LocalGroupMemberEnumerationController class. Extends GropuEnumerator.__init__()."""
        super(LocalGroupMemberEnumerationController, self).__init__(**kwargs)
        self._kwargs = kwargs

    def enumerate(self):
        """Discover local group members using all known methods."""
        # Add an instance of every method for enumerating local group members to the list passing it the keyword arguments.
        methods = []
        methods.append(NetLocalGroupGetMembers(**self._kwargs))

        # Enumerate with each method and set the members to self.
        # Duplicates are removed by _set_member.
        for method in methods:
            method.enumerate()
            for member in method.get_members():
                self._set_member(member)


class BaseUserEnumerator(BaseEnumerator):
    """Base user enumerator class."""
    def __init__(self, **kwargs):
        """Initialise UserInfoEnumerator class. Extends BaseEnumerator.__init__()."""
        super(BaseUserEnumerator, self).__init__(**kwargs)
        try:
            if isinstance(kwargs['auth'], BaseAuthenticator):
                self.auth = kwargs['auth']
                try:
                    self._host = self.auth.get_host()
                except:
                    raise Exception("Target host required from auth object.")
                else:
                    if isinstance(self._host, basestring):
                        pass
                    else:
                        raise Exception("Target host must be a string.")
            else:
                raise Exception("auth must be an instance of BaseAuthenticator.")
        except Exception as e:
            raise Exception("An authenticator is required.")

    def _get_host(self):
        """Return host."""
        return self._host

    def get_info(self):
        """Return list of user objects"""
        return self._info

    def _assign_info(self, info):
        """Assign user information dictionary."""
        self._info = info

    def _set_info(self,
                 comment='',
                 workstations='',
                 country_code='',
                 last_logon='',
                 password_expired='',
                 full_name='',
                 parms='',
                 code_page='',
                 priv='',
                 auth_flags='',
                 logon_server='',
                 home_dir='',
                 home_dir_drive='',
                 usr_comment='',
                 profile='',
                 acct_expires='',
                 primary_group_id='',
                 bad_pw_count='',
                 user_id='',
                 logon_hours='',
                 password='',
                 units_per_week='',
                 last_logoff='',
                 name='',
                 max_storage='',
                 num_logons='',
                 password_age='',
                 flags='',
                 script_path=''):
        """Add user information to list of users."""
        try:
            info = locals()  # create dictionary of local variables (this includes 'self')
        except Exception as e:
            self.log.warning("Error converting user information to dictionary. {0}".format(e))
        else:
            try:
                del info['self']
            except KeyError:
                self.log.warning("'self' not in dictionary.")
            else:
                self._assign_info(info)  # Use the child's specific method of assigning info to the data structure.


class UserEnumerator(BaseUserEnumerator):
    """User enumerator class."""
    def __init__(self, **kwargs):
        """Initialise UserEnumerator class. Extends BaseUserEnumerator.__init__()."""
        super(UserEnumerator, self).__init__(**kwargs)
        self._info = []

    def _assign_info(self, info):
        """Append user information to list of user information."""
        if info not in self.get_info():
            # If the user information is not already in the list, add it.
            self._info.append(info)
            self.log.debug("Enumerated info for user: '{0}', comment is: '{1}'".format(info['name'], info['comment']))
        else:
            self.log.debug("Ignoring duplicate user information for user: '{0}'".format(info['name']))


class NetUserEnum(UserEnumerator):
    """User enumerator class which uses pywin32 NetUserEnum()."""
    def __init__(self, **kwargs):
        """Initialise NetUserEnum user enumerator class. Extends UserEnumerator.__init__()."""
        super(NetUserEnum, self).__init__(**kwargs)

    def enumerate(self):
        """Enumerate user account information for all users on the system using pywin32 NetUserEnum()."""
        if self._auth.authenticate("smb"):  # Authenticate using SMB.
            self.log.debug("enumerating all user account information on '{0}' using NetUserEnum().".format(self._get_host()))
            resume = 0  # Flag used by the Windows API to indicate if there is more data.
            while True:
                try:
                    # Use pywin32 to get all the normal user accounts on the host.
                    users, total, resume = win32net.NetUserEnum(self._get_host(), 3, win32netcon.FILTER_NORMAL_ACCOUNT, resume)
                    '''first parameter is the server to execute on.
                    the second parameter is the level of user account information to retrieve, in this case 3 (the maximum).
                    the third parameter is the type of account to retrieve, in this case normal user accounts (not machine accounts etc).
                    the fourth parameter is the resume handle used to continue an existing search.'''
                except Exception as e:
                    self.log.warning("Error retrieving all user account information. {0}".format(e))
                    break  # If there was an error, break out of the infinite loop.
                else:
                    for user in users:
                        # For each user, set the info to self.
                        self._set_info(**user)
                finally:
                    if not resume:  # Windows API has indicated that there is no more data.
                        self._auth.deauthenticate()  # Deauthenticate from the host
                        break  # Break out of the infinite loop.
        else:
            self.log.debug("smb authentication failed. skipping NetUserEnum() user enumeration on '{0}'".format(self._get_host()))


class SNMPUserEnum(UserEnumerator):
    """User enumerator class which uses pysnmp."""
    def __init__(self, **kwargs):
        """Initialise SNMPUser user enumerator class. Extends UserEnumerator.__init__()."""
        super(SNMPUserEnum, self).__init__(**kwargs)

    def enumerate(self):
        """Enumerate list of user account names on the system using SNMP."""
        user_oid = '1.3.6.1.4.1.77.1.2.25'  # SNMP OID for usernames on a Windows host.
        if self._auth.authenticate("snmp"):  # Authenticate using SNMP
            self.log.debug("enumerating list of user account names using SNMP.")
            try:
                command_generator = cmdgen.CommandGenerator()  # Create an SNMP command generator object.
            except Exception as e:
                self.log.warning("Failed to create SNMP command generator: {0}".format(e))
            else:
                try:
                    # Use pysnmp to lookup usernames on the host.
                    err_indication, err_status, err_index, table = command_generator.bulkCmd(
                        cmdgen.CommunityData(self._auth.get_community_string()),  # community string for authentication.
                        cmdgen.UdpTransportTarget((self._get_host(), 161)),  # host and port to connect to.
                        0, 1,  # number of OID's to retrieve
                        user_oid)  # Start OID for users
                except Exception as e:
                    self.log.warning("Error retrieving user names using SNMP: {0}".format(e))
                else:
                    if err_indication or err_status:
                        self.log.warning("Error retrieving user names using SNMP indicated.")
                    else:
                        # Extract the user names from the results and set to self.
                        for row in table:
                            for oid_name, user_name in row:
                                if oid_name.prettyPrint().startswith(user_oid):
                                    if not user_name.prettyPrint().endswith("$"):  # ignore machine accounts
                                        self._set_info(name=user_name.prettyPrint())

        else:
            self.log.debug("snmp authentication failed. skipping SNMP user enumeration on '{0}'".format(self._get_host()))


class UserEnumerationController(UserEnumerator):
    """Run all user enumerators."""
    def __init__(self, **kwargs):
        """Initialise UserEnumerationController class. Extends PolicyEnumerator.__init__()."""
        super(UserEnumerationController, self).__init__(**kwargs)
        self._kwargs = kwargs

    def enumerate(self):
        """Enumerate all users of the system using all known methods."""
        # Add an instance of every method for enumerating users to the list passing it the keyword arguments.
        methods = []
        methods.append(NetUserEnum(**self._kwargs))
        methods.append(SNMPUserEnum(**self._kwargs))

        # Enumerate with each method and set the users to self.
        # Duplicates are removed by _set_info.
        for method in methods:
            method.enumerate()
            for user in method.get_info():
                self.log.debug(user)
                self._set_info(**user)


class UserInfoEnumerator(BaseUserEnumerator):
    """User information enumerator class."""
    def __init__(self, **kwargs):
        """Initialise UserInfoEnumerator class. Extends BaseUserEnumerator.__init__()."""
        super(UserInfoEnumerator, self).__init__(**kwargs)
        self._info = {}
        try:
            self._user = kwargs['user']
        except:
            raise Exception("Target user required.")
        else:
            if isinstance(self._user, basestring):
                pass
            else:
                raise Exception("Target user must be a string.")
        try:
            if isinstance(kwargs['auth'], BaseAuthenticator):
                self.auth = kwargs['auth']
                try:
                    self._host = self.auth.get_host()
                except:
                    raise Exception("Target host required from auth object.")
                else:
                    if isinstance(self._host, basestring):
                        pass
                    else:
                        raise Exception("Target host must be a string.")
            else:
                raise Exception("auth must be an instance of BaseAuthenticator.")
        except Exception as e:
            raise Exception("An authenticator is required.")

    def _get_host(self):
        """Return host."""
        return self._host

    def get_user(self):
        """Return user."""
        return self._user

    def set_user(self, user):
        """Assign host to use for enumeration."""
        self._user = user


class NetUserGetInfo(UserInfoEnumerator):
    """User information enumerator class which uses pywin32 NetUserInfo()."""
    def __init__(self, **kwargs):
        """Initialise NetUserInfo user information enumerator class. Extends UserInfoEnumerator.__init__()"""
        super(NetUserGetInfo, self).__init__(**kwargs)

    def enumerate(self):
        """Enumerate user account information on specified host using NetUserGetInfo().
        Overrides GroupEnumerator.enumerate()."""
        if self._auth.authenticate("smb"):  # Authenticate using SMB.
            self.log.debug("enumerating user account information for user '{0}' on '{1}' using NetUserGetInfo().".format(self.get_user(), self._get_host()))
            try:
                # Use pywin32 to get user account information from the host.
                user_info = win32net.NetUserGetInfo(self._get_host(), self.get_user(), 3)
                '''first parameter is the server to execute on.
                    the second parameter is the user account name to retrieve information for.
                    the third parameter is the level of user account information to retrieve, in this case 3 (the maximum).'''
            except Exception as e:
                self.log.warning("Error retrieving user account information for '{0}'. {1}".format(self.get_user(), e))
            else:
                self._set_info(**user_info)  # Set the info to self.
            self._auth.deauthenticate()  # Deauthenticate for the host.
        else:
            self.log.debug("smb authentication failed. skipping NetUserGetInfo() user account information for user '{0}' on '{1}'".format(self.get_user(), self._get_host()))


class UserInfoEnumerationController(UserInfoEnumerator):
    """Run all user information enumerators."""
    def __init__(self, **kwargs):
        """Initialise UserEnumerationController class. Extends PolicyEnumerator.__init__()."""
        super(UserInfoEnumerationController, self).__init__(**kwargs)
        self._kwargs = kwargs

    def enumerate(self):
        """Enumerate user account information using all known methods."""
        # Add an instance of every method for enumerating user information to the list passing it the keyword arguments.
        methods = []
        methods.append(NetUserGetInfo(**self._kwargs))

        # Enumerate with each method and set the users to self.
        # Duplicates are removed by _set_info.
        for method in methods:
            method.enumerate()
            if method.get_info() != {}:
                self._set_info(**method.get_info())
                # Without this check a blank user will be added because _set_info has default values.


class GroupMembershipEnumerator(BaseEnumerator):
    """Group membership enumerator class."""
    def __init__(self, **kwargs):
        """Initialise GroupMembershipEnumerator object.
        Extends BaseEnumerator.__init__()"""
        super(GroupMembershipEnumerator, self).__init__(**kwargs)
        self._groups = []
        try:
            self.set_user(kwargs['user'])
        except:
            raise Exception("Target user required.")
        else:
            if isinstance(self._user, basestring):
                pass
            else:
                raise Exception("Target user must be a string.")
        try:
            if isinstance(kwargs['auth'], BaseAuthenticator):
                self.auth = kwargs['auth']
                try:
                    self._host = self.auth.get_host()
                except:
                    raise Exception("Target host required from auth object.")
                else:
                    if isinstance(self._host, basestring):
                        pass
                    else:
                        raise Exception("Target host must be a string.")
            else:
                raise Exception("auth must be an instance of BaseAuthenticator.")
        except Exception as e:
            raise Exception("An authenticator is required.")

    def _get_host(self):
        """Return host."""
        return self._host

    def get_user(self):
        """Return user."""
        return self._user

    def set_user(self, user):
        """Assign host to use for enumeration."""
        self._user = user

    def _set_group(self, group):
        """Add group to list."""
        if group not in self.get_groups():
            # If the group is not in the list, add it.
            self._groups.append(group)
        else:
            self.log.warning("ignoring duplicate group: {0}".format(group))

    def get_groups(self):
        """Return list of groups."""
        return self._groups


class NetUserGetGroups(GroupMembershipEnumerator):
    """Domain group membership enumerator class which uses pywin32 NetUserGetGroups()."""
    def __init__(self, **kwargs):
        """Initialise NetUserGetGroups object.
        Extends GroupMembershipEnumerator.__init__()"""
        super(NetUserGetGroups, self).__init__(**kwargs)

    def enumerate(self):
        """Enumerate domain groups the user is a member of.
        Overrides GroupMembershipEnumerator.enumerate()"""
        if self._auth.authenticate("smb"):  # Authenticate using SMB
            self.log.debug("enumerating domain group membership for user '{0}' on '{1}' using NetUserGetGroups().".format(self.get_user(), self._get_host()))
            try:
                # Use pywin32 to get the domain groups of which the user is a member.
                groups = win32net.NetUserGetGroups(self._get_host(), self.get_user())
                # Note although the Windows documentation indicates there are several levels for this function win32net.NetUserGetGroups always runs at Level 1
            except Exception as e:
                self.log.warning("Error getting membership of domain groups for {0}: {1}".format(self.get_user(), e))
            else:
                for group in groups:
                    # For each group extract the name and set to self.
                    # group is in the form (name, attribute)  where name is a string and attribute is a number
                    # ignoring attribute for the moment.
                    name, attrib = group
                    self._set_group(name)
            self._auth.deauthenticate()  # Deauthenticate from the host.
        else:
                self.log.debug("smb authentication failed. skipping NetUserGetGroups() user group membership enumeration '{0}' on '{1}'".format(self.get_user(), self._get_host()))


class NetUserGetLocalGroups(GroupMembershipEnumerator):
    """Local group membership enumerator class which uses pywin32 NetUserGetLocalGroups()."""
    def __init__(self, **kwargs):
        """Initialise NetUserGetLocalGroups object.
        Extends GroupMembershipEnumerator.__init__()"""
        super(NetUserGetLocalGroups, self).__init__(**kwargs)

    def enumerate(self):
        """Enumerate local groups the user is a member of."
        Overrides GroupMembershipEnumerator.enumerate()"""
        if self._auth.authenticate("smb"):  # Authenticate using SMB
            self.log.debug("enumerating local group membership for user '{0}' on '{1}' using NetUserGetLocalGroups().".format(self.get_user(), self._get_host()))
            try:
                # Use pywin32 to get the local groups of which the user is a member.
                groups = win32net.NetUserGetLocalGroups(self._get_host(), self.get_user())
            except Exception as e:
                self.log.warning("Error getting membership of local groups for {0}: {1}".format(self.get_user(), e))
            else:
                for name in groups:
                    # For each group, set name to self.
                    # Unlike the return of NetUserGetGroups, name is a string
                    self._set_group(name)
            self._auth.deauthenticate()  # Deauthenticate from host.
        else:
                self.log.debug("smb authentication failed. skipping NetUserGetLocalGroups() user local group membership enumeration '{0}' on '{1}'".format(self.get_user(), self._get_host()))


class GroupMembershipEnumerationController(GroupMembershipEnumerator):
    """Run all domain group membership enumerators."""
    def __init__(self, **kwargs):
        """Initialise GroupMembershipEnumerationController object.
        Extends GroupMembershipEnumerator.__init__()"""
        super(GroupMembershipEnumerationController, self).__init__(**kwargs)
        self._kwargs = kwargs

    def enumerate(self):
        """Discover the domain groups a user is a member of using all known methods.
        Overrides GroupMembershipEnumerator.enumerate()"""
        # Add an instance of every method for enumerating a user's domain groups to the list passing it the keyword arguments.
        methods = []
        methods.append(NetUserGetGroups(**self._kwargs))

        # Enumerate with each method and set the groups to self.
        # Duplicates are removed by _set_group.
        for method in methods:
            method.enumerate()
            for group in method.get_groups():
                self._set_group(group)


class LocalGroupMembershipEnumerationController(GroupMembershipEnumerator):
    """Run all local group membership enumerators."""
    def __init__(self, **kwargs):
        """Initialise LocalGroupMembershipEnumerationController object.
        Extends GroupMembershipEnumerator.__init__()"""
        super(LocalGroupMembershipEnumerationController, self).__init__(**kwargs)
        self._kwargs = kwargs

    def enumerate(self):
        """Discover the local groups a user is a member of using all known methods."
        Overrides GroupMembershipEnumerator.enumerate()"""
        # Add an instance of every method for enumerating a user's local groups to the list passing it the keyword arguments.
        methods = []
        methods.append(NetUserGetLocalGroups(**self._kwargs))

        # Enumerate with each method and set the groups to self.
        # Duplicates are removed by _set_group.
        for method in methods:
            method.enumerate()
            for group in method.get_groups():
                self._set_group(group)


class PolicyEnumerator(BaseEnumerator):
    """Policy enumerator class."""
    def __init__(self, **kwargs):
        """Initialise policy enumerator class. Extends BaseEnumerator.__init__()."""
        super(PolicyEnumerator, self).__init__(**kwargs)
        self._policy = {'force_logoff': "",
                         'min_passwd_len': "",
                         'max_passwd_age': "",
                         'password_hist_len': "",
                         'min_passwd_age': "",
                         'lockout_observation_window': "",
                         'lockout_duration': "",
                         'lockout_threshold': "",
                         'complexity': ""}
        try:
            if isinstance(kwargs['auth'], BaseAuthenticator):
                self.auth = kwargs['auth']
                try:
                    self._host = self.auth.get_host()
                except:
                    raise Exception("Target host required from auth object.")
                else:
                    if isinstance(self._host, basestring):
                        pass
                    else:
                        raise Exception("Target host must be a string.")
            else:
                raise Exception("auth must be an instance of BaseAuthenticator.")
        except Exception as e:
            raise Exception("An authenticator is required.")

    def _get_host(self):
        """Return host."""
        return self._host

    def get_policy(self):
        """Return policy."""
        return self._policy

    def _set_policy(self,
                    force_logoff="",
                    min_passwd_len="",
                    max_passwd_age="",
                    password_hist_len="",
                    min_passwd_age="",
                    lockout_observation_window="",
                    lockout_duration="",
                    lockout_threshold="",
                    complexity=""):
        """Add policy to dictionary."""
        try:
            raw_policy = locals()  # create dictionary of local variables (this includes 'self')
        except Exception as e:
            self.log.warning("Error converting policy information to dictionary. {0}".format(e))
        else:
            try:
                del raw_policy['self']
            except KeyError:
                self.log.warning("'self' not in dictionary.")
            else:
                for key in raw_policy.keys():
                    if raw_policy[key] == "":
                        pass  # Ignore default blanks - actual policies will never be blank.
                    else:
                        # Check if the key already exists and if not, set it.
                        if key not in self._policy.keys():
                            # If the key is not already in the policy, set it.
                            self._policy[key] = raw_policy[key]
                            self.log.debug("policy {0}: {1}".format(key, raw_policy[key]))
                        elif self._policy[key] == "":
                            # If the key already exists in the policy but it is blank, overwrite it.
                            self._policy[key] = raw_policy[key]
                            self.log.debug("policy {0}: {1}".format(key, raw_policy[key]))
                        else:
                            # The key already exists and it is not blank. Ignore the new information.
                            self.log.debug("ignoring duplicate policy {0}: {1}".format(key, raw_policy[key]))  # With intended use should never actually be in this situation.


class PasswordPolicy(PolicyEnumerator):
    """Password policy enumerator class."""
    def __init__(self, **kwargs):
        """Initialise PasswordPolicy policy enumerator class. Extends PolicyEnumerator.__init__()."""
        super(PasswordPolicy, self).__init__(**kwargs)

    def _enumerate_complexity(self, min_length):
        """Enumerate password complexity."""
        # This function works under the premise that a password consisting of the character 'a' that is the correct length
        # will meet the password policy, but would not meet the complexity policy if it is enabled.
        # TODO: It is possible to specify additional complexity rules ('Password Filter DLL').
        #        Get specific information about the implemented rules using multiple NetPasswordPolicy() calls.
        # TODO: This does not seem to work properly unless it is running on the local host. Investigate and work around.
        self.log.debug("enumerating complexity policy on '{0}' using NetValidatePasswordPolicy().".format(self._get_host()))
        validation_type = win32netcon.NetValidatePasswordChange
        arguments = {'ClearPassword': "", 'UserAccountName': "", 'PasswordMatch': True}
        try:
            arguments['ClearPassword'] = "a" * min_length  # Candidate password of minimum length.
        except Exception as e:
            self.log.warning("Failed to create minimum length candidate password.")
        else:
            try:
                # Use pywin32 to check if a password of the minimum length but with no complexity is accepted.
                fields, status = win32net.NetValidatePasswordPolicy(self._get_host(), None, validation_type, arguments)
            except Exception as e:
                self.log.warning("Failed to validate complexity policy. {0}".format(e))
            else:
                if status == 2245:
                    # Password is too short.
                    self.log.warning("Password '{0}' does not meet the password policy (length/history).".format(arguments['ClearPassword']))
                    # TODO: iterate through increasing password lengths to get the correct policy then redo complexity check.
                elif status == 2704:
                    # Complexity policy is enabled, set the policy to self..
                    self.log.debug("Password: '{0}' does not meet the complexity policy.".format(arguments['ClearPassword']))
                    self._set_policy(complexity=True)
                elif status == 0:
                    # Complexity policy is disabled, set the policy to self..
                    self.log.debug("Password: '{0}' meets password and complexity policy.".format(arguments['ClearPassword']))
                    self._set_policy(complexity=False)
                else:
                    self.log.warning("Unexpected return value from NetValidatePasswordPolicy: {0} - {1}".format(status, win32api.FormatMessage(status).strip()))

    def _enumerate_password(self):
        """Enumerate password policy on specified host using NetUserModalsGet()."""
        self.log.debug("enumerating password policy on '{0}' using NetUserModalsGet().".format(self._get_host()))
        try:
            # Use pywin32 to get the password policy.
            policy = win32net.NetUserModalsGet(self._get_host(), 0)
            # returns dictionary with keys: force_logoff, min_passwd_len, max_passwd_age, password_hist_len, min_passwd_age
        except Exception as e:
            self.log.warning("failed to get password policy on {0}: {1}".format(self._get_host(), e))
        else:
            # Convert the policies to sensible units. API Returns them in seconds, but does not allow that level of granulariy when setting.
            if policy['force_logoff'] == win32netcon.TIMEQ_FOREVER:
                policy['force_logoff'] = "Never"
            if policy['max_passwd_age'] == win32netcon.TIMEQ_FOREVER:
                policy['max_passwd_age'] = "Infinite"
            else:
                policy['max_passwd_age'] = (((policy['max_passwd_age'] / 60) / 60) / 24) # make it days
            policy['min_passwd_age'] = (((policy['min_passwd_age'] / 60) / 60) / 24) # make it days
            if policy['force_logoff'] == win32netcon.TIMEQ_FOREVER:
                policy['force_logoff'] = "Never"
            self._set_policy(**policy)  # Set the policy to self.

    def enumerate(self):
        """Enumerate password and complexity policy on specified host.
        Overrides PolicyEnumerator.enumerate()."""
        if self._auth.authenticate("smb"):  # Authenticate using SMB
            self._enumerate_password()  # Enumerate the password policy
            if self.get_policy()['min_passwd_len'] is not "" and "." not in self._get_host():
                # the minimum password length was enumerated and host is NetBIOS style.
                # host must be NetBIOS style or "parameter incorrect" error.
                self._enumerate_complexity(self.get_policy()['min_passwd_len'])  # Enumerate complexity policy.
            self._auth.deauthenticate()  # Deauthenticate from the host.
        else:
            self.log.debug("smb authentication failed. skipping enumerating password and complexity policy enumeration on '{0}'".format(self._get_host()))


class LockoutPolicy(PolicyEnumerator):
    """Lockout policy enumerator class."""
    def __init__(self, **kwargs):
        """Initialise LockoutPolicy policy enumerator class. Extends PolicyEnumerator.__init__()."""
        super(LockoutPolicy, self).__init__(**kwargs)

    def enumerate(self):
        """Enumerate lockout policy on specified host using NetUserModalsGet().
        Overrides PolicyEnumerator.enumerate()."""
        if self._auth.authenticate("smb"):  # Authenticate using SMB.
            self.log.debug("enumerating lockout policy on '{0}' using NetUserModalsGet().".format(self._get_host()))
            try:
                # Use pywin32 to get the lockout policy
                policy = win32net.NetUserModalsGet(self._get_host(), 3)
                # returns dictionary with keys: lockout_observation_window, lockout_duration, lockout_threshold
            except Exception as e:
                self.log.warning("failed to get lockout policy {0}: {1}".format(self._get_host(), e))
            else:
                # Convert the policies to sensible units. API Returns them in seconds, but does not allow that level of granulariy when setting.
                policy['lockout_observation_window'] = (policy['lockout_observation_window'] / 60) # Make it minutes
                policy['lockout_duration'] = (policy['lockout_duration'] / 60) # Make it minutes
                self._set_policy(**policy)  # Set the policy to self.
            self._auth.deauthenticate()  # Deauthenticate from the host.
        else:
            self.log.debug("smb authentication failed. skipping NetUserModalsGet() enumerating lockout policy on '{0}'".format(self._get_host()))


class PolicyEnumerationController(PolicyEnumerator):
    """Run all policy enumerators."""
    def __init__(self, **kwargs):
        """Initialise PolicyEnumerationController class. Extends PolicyEnumerator.__init__()."""
        super(PolicyEnumerationController, self).__init__(**kwargs)
        self._kwargs = kwargs

    def enumerate(self):
        """Discover policies using all known methods."""
        # Add an instance of every method for enumerating accounts policies to the list passing it the keyword arguments.
        methods = []
        methods.append(PasswordPolicy(**self._kwargs))
        methods.append(LockoutPolicy(**self._kwargs))

        # Enumerate with each method and set the policy to self.
        # Duplicates are removed by _set_policy.
        for method in methods:
            method.enumerate()
            self._set_policy(**method.get_policy())


class InterestingHostEnumerator(BaseEnumerator):
    """Interesting host enumerator class."""
    def __init__(self, **kwargs):
        """Initialise policy enumerator class. Extends BaseEnumerator.__init__()."""
        super(InterestingHostEnumerator, self).__init__(**kwargs)
        self._hosts = []

    def get_hosts(self):
        """Return list of dictionaries of hosts and services."""
        return self._hosts

    def _set_host(self, name, service, port=None):
        """Append host to list."""
        host_service = {'name': name, 'service': service, 'port': port}
        if host_service not in self.get_hosts():
            # If the host and service are not already in the list, add them.
            self._hosts.append(host_service)


class FromNetServerEnum(InterestingHostEnumerator):
    """Interesting host enumerator using pywin32 NetServerEnum()."""
    def __init__(self, **kwargs):
        """Initialise FromNetServerEnum enumerator. Extends InterestingHostEnumerator.__init__()."""
        super(FromNetServerEnum, self).__init__(**kwargs)
        # Valid types for this API call.
        self._valid_types = ['WORKSTATION',
                            'SERVER',
                            'SQLSERVER',
                            'DOMAIN_CTRL',
                            'DOMAIN_BAKCTRL',
                            'TIME_SOURCE',
                            'AFP',
                            'NOVELL',
                            'DOMAIN_MEMBER',
                            'PRINTQ_SERVER',
                            'DIALIN_SERVER',
                            'XENIX_SERVER',
                            'SERVER_UNIX',
                            'SQLSERVER',
                            'NT',
                            'WFW',
                            'SERVER_MFPN',
                            'SERVER_NT',
                            'POTENTIAL_BROWSER',
                            'BACKUP_BROWSER',
                            'MASTER_BROWSER',
                            'DOMAIN_MASTER',
                            'SERVER_OSF',
                            'SERVER_VMS',
                            'TYPE_WINDOWS',
                            'DFS',
                            'CLUSTER_NT',
                            # 'TERMINALSERVER', # Not in win32netcon
                            # 'CLUSTER_VS_NT', # Not in win32netcon
                            'ALTERNATE_XPORT',
                            'LOCAL_LIST_ONLY',
                            'DOMAIN_ENUM',
                            'ALL']

        try:
            self._domain_name = kwargs['domain']
        except:
            self._domain_name = None

    def get_domain(self):
        """Return domain name."""
        return self._domain_name

    def set_domain(self, name):
        """Assign domain to use for enumeration."""
        self._domain_name = name

    def get_valid_types(self):
        """Return a list of valid service types."""
        return self._valid_types

    def _convert_type(self, type_):
        """
        Convert a string representing server type e.g. "WORKSTATION" to the win32netcon type e.g. win32netcon.SV_TYPE_WORKSTATION
        type_ expects one of:
         WORKSTATION, SERVER, SQLSERVER, DOMAIN_CTRL, DOMAIN_BAKCTRL, TIME_SOURCE, AFP,
         NOVELL, DOMAIN_MEMBER, PRINTQ_SERVER, DIALIN_SERVER, XENIX_SERVER, SERVER_UNIX,
         SQLSERVER, NT, WFW, SERVER_MFPN, SERVER_NT, POTENTIAL_BROWSER, BACKUP_BROWSER,
         MASTER_BROWSER, DOMAIN_MASTER, SERVER_OSF, SERVER_VMS, TYPE_WINDOWS, DFS,
         CLUSTER_NT, TERMINALSERVER, CLUSTER_VS_NT, ALTERNATE_XPORT, LOCAL_LIST_ONLY,
         DOMAIN_ENUM, ALL
        Returns either a win32netcon type.
        """
        type_ = str(type_).upper()
        if type_ == "WORKSTATION":
            type_ = win32netcon.SV_TYPE_WORKSTATION
        elif type_ == "SERVER":
            type_ = win32netcon.SV_TYPE_SERVER
        elif type_ == "SQLSERVER":
            type_ = win32netcon.SV_TYPE_SQLSERVER
        elif type_ == "DOMAIN_CTRL":
            type_ = win32netcon.SV_TYPE_DOMAIN_CTRL
        elif type_ == "DOMAIN_BAKCTRL":
            type_ = win32netcon.SV_TYPE_DOMAIN_BAKCTRL
        elif type_ == "TIME_SOURCE":
            type_ = win32netcon.SV_TYPE_TIME_SOURCE
        elif type_ == "AFP":
            type_ = win32netcon.SV_TYPE_AFP
        elif type_ == "NOVELL":
            type_ = win32netcon.SV_TYPE_NOVELL
        elif type_ == "DOMAIN_MEMBER":
            type_ = win32netcon.SV_TYPE_DOMAIN_MEMBER
        elif type_ == "PRINTQ_SERVER":
            type_ = win32netcon.SV_TYPE_PRINTQ_SERVER
        elif type_ == "DIALIN_SERVER":
            type_ = win32netcon.SV_TYPE_DIALIN_SERVER
        elif type_ == "XENIX_SERVER":
            type_ = win32netcon.SV_TYPE_XENIX_SERVER
        elif type_ == "SERVER_UNIX":
            type_ = win32netcon.SV_TYPE_SERVER_UNIX
        elif type_ == "SQLSERVER":
            type_ = win32netcon.SV_TYPE_SQLSERVER
        elif type_ == "NT":
            type_ = win32netcon.SV_TYPE_NT
        elif type_ == "WFW":
            type_ = win32netcon.SV_TYPE_WFW
        elif type_ == "SERVER_MFPN":
            type_ = win32netcon.SV_TYPE_SERVER_MFPN
        elif type_ == "SERVER_NT":
            type_ = win32netcon.SV_TYPE_SERVER_NT
        elif type_ == "POTENTIAL_BROWSER":
            type_ = win32netcon.SV_TYPE_POTENTIAL_BROWSER
        elif type_ == "BACKUP_BROWSER":
            type_ = win32netcon.SV_TYPE_BACKUP_BROWSER
        elif type_ == "MASTER_BROWSER":
            type_ = win32netcon.SV_TYPE_MASTER_BROWSER
        elif type_ == "DOMAIN_MASTER":
            type_ = win32netcon.SV_TYPE_DOMAIN_MASTER
        elif type_ == "SERVER_OSF":
            type_ = win32netcon.SV_TYPE_SERVER_OSF
        elif type_ == "SERVER_VMS":
            type_ = win32netcon.SV_TYPE_SERVER_VMS
        elif type_ == "TYPE_WINDOWS":
            type_ = win32netcon.SV_TYPE_WINDOWS
        elif type_ == "DFS":
            type_ = win32netcon.SV_TYPE_DFS
        elif type_ == "CLUSTER_NT":
            type_ = win32netcon.SV_TYPE_CLUSTER_NT
        elif type_ == "TERMINALSERVER":
            # type_ = win32netcon.SV_TYPE_TERMINALSERVER
            self.log.critical("TERMINALSERVER not defined in win32netcon")
            raise StandardError("TERMINALSERVER not defined in win32netcon")
        elif type_ == "CLUSTER_VS_NT":
            # type_ = win32netcon.SV_TYPE_CLUSTER_VS_NT
            self.log.critical("CLUSTER_VS_NT not defined in win32netcon")
            raise StandardError("CLUSTER_VS_NT not defined in win32netcon")
        elif type_ == "ALTERNATE_XPORT":
            type_ = win32netcon.SV_TYPE_ALTERNATE_XPORT
        elif type_ == "LOCAL_LIST_ONLY":
            type_ = win32netcon.SV_TYPE_LOCAL_LIST_ONLY
        elif type_ == "DOMAIN_ENUM":
            type_ = win32netcon.SV_TYPE_DOMAIN_ENUM
        elif type_ == "ALL":
            type_ = win32netcon.SV_TYPE_ALL
        else:
            self.log.critical("{0} is not a valid win32netcon type".format(type_))
            raise StandardError("{0} is not a valid win32netcon type".format(type_))
        #self.log.debug("type_ = {0}".format(str(type_)))
        return type_

    def find_hosts(self, type_):
        """Enumerate hosts of specified type using NetServerEnum()."""
        self.log.debug("enumerating hosts of type '{0}' using NetServerEnum().".format(type_))
        try:
            # Convert the type string to the win32netcon constant.
            serv_type = self._convert_type(type_)
        except StandardError as error:
            self.log.warning("Error converting type: {0}".format(error))
        else:
            resume = 0  # Flag used by the Windows API to indicate there is more data.
            while True:
                try:
                    # Use pywin32 to enumerate hosts that are running the specified service.
                    server_data, total, resume = win32net.NetServerEnum(None, 100, serv_type, self.get_domain(), resume)
                except pywintypes.error as details:
                    if details[0] == 2127:
                        self.log.debug("Failed to find servers of type '{0}' (likely they don't exist). ({1}, '{2}', '{3}')".format(type_, details[0], details[1], details[2]))
                    else:
                        self.log.warning("Failed to find servers of type '{0}'. ({1}, '{2}', '{3}')".format(type_, details[0], details[1], details[2]))
                    break  # If an error occured, break out of the infinite loop.
                except Exception as e:
                    self.log.warning("failed to find servers of type '{0}'. {1}".format(type_, e))
                    break  # If an error occured, break out of the infinite loop.
                else:
                    for server in server_data:
                        # For each server in the returned data, extract the name and set it to self.
                        self._set_host(server['name'], type_)
                    if not resume:
                        break  # The Windows API indicated there is no more data, break out of the infinte loop.

    def enumerate(self):
        """Enumerate list of interesting hosts using pywin32 NetServerEnum().
        Overrides InterestingHostEnumerator.enumerate()."""
        self.log.debug("enumerating interesting hosts using NetServerEnum()")
        # TODO: This is very slow. See if it can be threaded. (Need to make sure win32net.NetServerEnum() is thread safe.)
        for type_ in self.get_valid_types():
            # For eery valid type, look up hosts.
            self.find_hosts(type_)


class FromDNSRecords(InterestingHostEnumerator):
    """Interesting host enumerator that uses DNS SRV records."""
    def __init__(self, **kwargs):
        """Initialise FromNetServerEnum enumerator. Extends InterestingHostEnumerator.__init__()."""
        super(FromDNSRecords, self).__init__(**kwargs)
        try:
            self._services = kwargs['services']
        except:
            # Services that have DNS SRV records.
            self._services = ["_ldap._tcp",  # LDAP
                             "_gc._tcp",  # Global Catalouge
                             "_kerberos._tcp",  # Kerberos
                             "_http._tcp",  # Web
                             "_sip._tcp",  # SIP (VOIP)
                             "_sip._udp",  # SIP (VOIP)
                             "_sql._tcp",  # SQL
                             "_ftp._tcp",  # FTP
                             "_imap._tcp",  # IMAP mail service
                             "_pop3._tcp",  # POP3 mail service
                             "_smtp._tcp",  # SMTP mail service
                             "_sip._tls",  # Lync if on port 433
                             "_sipfederationtls._tcp"  # Lync if on port 5061
                             ]
        try:
            self._domain_name = kwargs['domain']
        except:
            self._domain_name = ""

    def get_services(self):
        """Return list of services to enumerate."""
        return self._services

    def set_services(self, services):
        """Replace list of services to enumerate."""
        self._services = services

    def get_domain_name(self):
        """Return domain name to enumerate."""
        return self._domain_name

    def set_domain_name(self, domain):
        """Assign domain to enumerate."""
        self._domain_name = domain

    def lookup(self, query, type_):
        """Lookup records to get IP address, get name from IP address"""
        self.log.debug("Looking up: '{0}' records for '{1}'".format(type_, query))
        try:
            # Use dnspython to query the the specified name and type.
            answers = dns.resolver.query(query, type_)
        except dns.resolver.NXDOMAIN as e:
            self.log.debug("No {0} records for: {1}".format(type_, query))
        except Exception as e:
            self.log.warning("Failed to lookup records. {0}".format(e))
        else:
            # There could be several answers so loop over them.
            for answer in answers:
                try:
                    name_str = answer.to_text().split(" ")[3]  # Extract the name as a string from the dnspython Answer object.
                except Exception as e:
                    self.log.warning("Failed to lookup name from IP address. {0}".format(e))
                    name = dns.name.from_text("").to_text()  # If we failed to extract the name set it to an empty DNS name (Note this is "." once its been through dnspython.)
                finally:
                    try:
                        port = answer.to_text().split(" ")[2]  # Extract the port as a string from the dnspython Answer Object.
                    except Exception as e:
                        self.log.warning("Failed to extract port from answer. {0}".format(e))
                        port = ""
                    finally:
                        self._set_host(name=name_str, service=query, port=port)  # Set the host, service and port to self.

    def zone_transfer(self):
        """Perform zone transfer."""
        # TODO: This.
        self.log.debug("Performing zone transfers.")
        raise StandardError("not implemented.")

    def enumerate(self):
        """Enumerate list of interesting hosts using DNS SRV records.
        Overrides InterestingHostEnumerator.enumerate()."""
        self.log.debug("enumerating hosts from SRV records.")
        for service in self.get_services():
            # For each service, identify hosts.
            srv = "{0}.{1}".format(service, self.get_domain_name())
            self.lookup(srv, "SRV")


class InterestingHostEnumerationController(InterestingHostEnumerator):
    """Run all interesting host enumerators."""
    def __init__(self, **kwargs):
        """Initialise InterestingHostEnumerationController class. Extends InterestingHostEnumerator.__init__()."""
        super(InterestingHostEnumerationController, self).__init__(**kwargs)
        self._kwargs = kwargs

    def enumerate(self):
        """Discover interesting hosts using all known methods."""
        # Add an instance of every method for enumerating ahosts to the list passing it the keyword arguments.
        methods = []
        if "." in self._kwargs['domain']:
            # Looks like a dns domain name.
            # Use methods that only work for dns domain names.
            methods.append(FromDNSRecords(**self._kwargs))
        else:
            # Looks like a NetBIOS domain name.
            # Use methods that only work for NetBIOS domain names.
            methods.append(FromNetServerEnum(**self._kwargs))

        # Enumerate with each method and set the host to self.
        # Duplicates are removed by _set_host.
        for method in methods:
            method.enumerate()
            for host in method.get_hosts():
                self._set_host(**host)


class ShareEnumerator(BaseEnumerator):
    """Share enumerator class."""
    def __init__(self, **kwargs):
        """Initialise ShareEnumerator class. Extends BaseEnumerator.__init__()"""
        super(ShareEnumerator, self).__init__(**kwargs)
        self._shares = []
        try:
            if isinstance(kwargs['auth'], BaseAuthenticator):
                self.auth = kwargs['auth']
                try:
                    self._host = self.auth.get_host()
                except:
                    raise Exception("Target host required from auth object.")
                else:
                    if isinstance(self._host, basestring):
                        pass
                    else:
                        raise Exception("Target host must be a string.")
            else:
                raise Exception("auth must be an instance of BaseAuthenticator.")
        except Exception as e:
            raise Exception("An authenticator is required.")

    def _get_host(self):
        """Return domain controller."""
        return self._host

    def get_shares(self):
        """Return dictionary of shares."""
        return self._shares

    def _set_share(self, name, remark, type_):
        """Create dictionary of share info and append it to the list if not aleardy known."""
        share = {'name': name, 'remark': remark, 'type_': type_}
        if share not in self.get_shares():
            # If the shrae is not in the list of shares, add it.
            self._shares.append(share)


class NetShareEnum(ShareEnumerator):
    """Share enumerator using pywin32.NetShareEnum()"""
    def __init__(self, **kwargs):
        """Initialise NetShareEnum class. Extends ShareEnumerator.__init__()"""
        super(NetShareEnum, self).__init__(**kwargs)

    def enumerate(self):
        """Enumerate share information."""
        if self._auth.authenticate("smb"):  #Authenticate using SMB.
            self.log.debug("enumerating shares on '{0}' using NetServerEnum().".format(self._get_host()))
            # Share type constants:
            share_types = {long(2147483648): 'STYPE_SPECIAL-Disk_drive',    # For some reason in win32security this is a negative, but that isn't what is returned by the api...
                           long(-2147483648): 'STYPE_SPECIAL-Disk_drive',   # Just in case win32security is right...
                           long(2147483649): 'STYPE_SPECIAL-Print_queue',   # This is missing from win32security.
                           long(2147483650): 'STYPE_SPECIAL-Devices',       # This is missing from win32security.
                           long(2147483651): 'STYPE_SPECIAL-IPC',           # This is missing from win32security.
                           long(0): 'STYPE_DISKTREE',
                           long(1): 'STYPE_PRINTQ',
                           long(2): 'STYPE_DEVICE',
                           long(3): 'SYTPE_IPC'}
                        # Note: use share_info.get('type', "UNKNOWN_SHARE_TYPE") is used so that there is a default value
                        # This handles cases where documentation is wrong (which is a lot of the time...) so share type isn't in share_types
            resume = 0  # Flag used by the Windows API to indicate there is more data.
            while True:
                try:
                    # Use pywin32 to get a list of shares on the host.
                    shares_info, total, resume = win32net.NetShareEnum(self._get_host(), 1, resume)  # can only get levels 0 and 1 without special privs.
                except Exception as e:
                    self.log.warning("Error enumerating share information on '{0}'. {1}".format(self._get_host(), e))
                    break  # If there was an error, break out of the infinite loop.
                else:
                    # For each share, lookup the string for the share type and assign to self.
                    for share_info in shares_info:
                        # Note share_info.get('type', "UNKOWN_SHARE_TYPE") is used so that there is a default value
                        # This handles cases where documentation is wrong (which is a lot of the time...) so share type isn't in share_types
                        self.log.debug("Found share: '{name}' of type '{type}' with remark '{remark}' on '{host}'.".format(name=share_info['netname'],
                                                                                                                           type=share_types.get(share_info['type'], "UNKNOWN_SHARE_TYPE"),
                                                                                                                           remark=share_info['remark'],
                                                                                                                           host=self._get_host()))
                        self._set_share(share_info['netname'], share_info['remark'], share_types.get(share_info['type'], "UNKNOWN_SHARE_TYPE"))
                    if not resume:
                        break  # Windows API has indicated there is no more data, so break out of the infinite loop.
            self._auth.deauthenticate()  # Deauthenticate from the host.
        else:
            self.log.debug("smb authentication failed. skipping NetServerEnum() enumerating shares on '{0}'".format(self._get_host()))


class ShareEnumerationController(ShareEnumerator):
    """Share enumeration controller class."""
    def __init__(self, **kwargs):
        """Initialise ShareEnumerationController. Extends ShareEnumerator.__init__()"""
        super(ShareEnumerationController, self).__init__(**kwargs)
        self._kwargs = kwargs

    def enumerate(self):
        """Enumerate shares using all known methods."""
        # Add an instance of every method for enumerating sharess to the list passing it the keyword arguments.
        methods = []
        methods.append(NetShareEnum(**self._kwargs))

        # Enumerate with each method and set the shares to self.
        # Duplicates are removed by _set_share.
        for method in methods:
            method.enumerate()
            for share in method.get_shares():
                self._set_share(**share)


class SessionEnumerator(BaseEnumerator):
    """Session enumerator class."""
    def __init__(self, **kwargs):
        """Initialise SessionEnumerator object.
        Extends BaseEnumerator.__init__()"""
        super(SessionEnumerator, self).__init__(**kwargs)
        self._sessions = []
        try:
            self._user = kwargs['user']
        except:
            self._user = None
        try:
            if isinstance(kwargs['auth'], BaseAuthenticator):
                self.auth = kwargs['auth']
                try:
                    self._host = self.auth.get_host()
                except:
                    raise Exception("Target host required from auth object.")
                else:
                    if isinstance(self._host, basestring):
                        pass
                    else:
                        raise Exception("Target host must be a string.")
            else:
                raise Exception("auth must be an instance of BaseAuthenticator.")
        except Exception as e:
            raise Exception("An authenticator is required.")

    def _get_host(self):
        """Return host."""
        return self._host

    def get_user(self):
        """Return user."""
        return self._user

    def set_user(self, user):
        """Assign host to use for enumeration."""
        self._user = user

    def _set_session(self, client_name, user_name=None, active_time=None, idle_time=None):
        """Add session information to list."""
        session = {'user_name': user_name, 'active_time': active_time, 'client_name': client_name, 'idle_time': idle_time}
        if session not in self._session:
            # If the session is not already in the list, add it.
            self._sessions.append(session)

    def get_sessions(self):
        """Return list of sessions."""
        return self._sessions


class NetSessionEnum(SessionEnumerator):
    """Session enumerator class which uses pywin32 NetSessionEnum()."""
    def __init__(self, **kwargs):
        """Initialise NetSessionEnum object.
        Extends SessionEnumerator."""
        super(NetSessionEnum, self).__init__(**kwargs)

    def enumerate(self):
        """Enumerate active sessions using NetSessionEnum.
        Overrides SessionEnumerator.enumerate()."""
        if self._auth.authenticate("smb"):  # Authenticate using SMB.
            self.log.debug("enumerating sessions user '{0}' on '{1}' using NetSessionEnum().".format(self.get_user(), self._get_host()))
            try:
                # Use pywin32 to get sessions on the host.
                sessions = win32net.NetSessionEnum(10, "\\\\{0}".format(self._get_host()), None, self.get_user())
                '''level 10 gives: user_name, active_time, client_name, idle_time and requires no special permissions.
                level 0 gives client_name requiring no special permissions
                levels 1,2,502 require administrator permissions.'''
            except Exception as e:
                self.log.warning("Error getting sessions for '{0}' at level 10: {1}".format(self.get_user(), e))
            else:
                # For each session, set it to self.
                for session in sessions:
                    try:
                        self._set_session(**session)
                    except Exception as e:
                        self.log.warning("failed to add session '{0}': {1}".format(session, e))
            self._auth.deauthenticate()  # Deauthenticate from the host.
        else:
                self.log.debug("smb authentication failed. skipping NetSessionEnum() session enumeration for '{0}' on '{1}'".format(self.get_user(), self._get_host()))


class SessionEnumerationController(SessionEnumerator):
    """Run all session enumerators."""
    def __init__(self, **kwargs):
        """Initialise SessionEnumerationController object.
        Extends SessionEnumerator.__init__()."""
        super(SessionEnumerationController, self).__init__(**kwargs)

    def enumerate(self):
        """Enumerate active sessions using all methods."""
        # Add an instance of every method for enumerating sessions to the list passing it the keyword arguments.
        methods = []
        methods.append(NetSessionEnum(**self._kwargs))

        # Enumerate with each method and set the sessions to self.
        # Duplicates are removed by _set_session.
        for method in methods:
            method.enumerate()
            for session in method.get_sessions():
                self._set_session(**session)


if __name__ == '__main__':
    logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.DEBUG)
