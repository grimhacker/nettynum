'''
.       .1111...          | Title: reporters.py
    .10000000000011.   .. | Author: Oliver Morton
 .00              000...  | Email: grimhacker@grimhacker.com
1                  01..   | Description:
                    ..    | All reporter objects.
                   ..     |
GrimHacker        ..      |
                 ..       |
grimhacker.com  ..        |
@grimhacker    ..         | Created on: 9 Jan 2014
----------------------------------------------------------------------------
Nettynum - A Windows Domain Enumeration Tool
    Copyright (C) 2014  Oliver Morton

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
import logging
import xml.etree.ElementTree as ET
from xml.dom import minidom
from xml.sax.saxutils import escape

from data import Domain as Domain
from data import Host as Host
from data import Group
from data import User

#TODO: XSLT transform checks and built in?

#Note: Throughout exceptions are dealt with wherever possible by printing an error message and moving on.
#  This is to try and make sure that all possible data is output to a file instead of being lost completely.


class BaseReporter(object):
    """Base reporter class."""
    def __init__(self, **kwargs):
        """Initialise base reporter."""
        self.log = logging.getLogger(__name__)
        try:
            self._data = kwargs['data']
        except:
            self._data = None

        try:
            self._filename = kwargs['filename']
        except:
            self._filename = None

    def generate(self):
        """Generate report."""
        pass


class BaseXMLReporter(BaseReporter):
    """Base XML reporter class."""
    def __init__(self, **kwargs):
        """Initialise BaseXMLReporter. Extends BaseReporter.__init__()."""
        super(BaseXMLReporter, self).__init__(**kwargs)

    def _escape_text(self, text):
        """Convert text to a string and escape it to prevent XML injection.
        WARNING: This function is not sufficient for data in attribute values."""
        return escape(str(text))

    def _get_data(self):
        """Return data."""
        return self._data

    def _to_rough_xml(self):
        """Create XML from data."""
        pass

    def _subelement_with_text(self, parent, tag, text, attrib={}, **kwargs):
        """Create an xml subelement."""
        elem = ET.SubElement(parent, tag, attrib, **kwargs)
        elem.text = self._escape_text(text)  # Escape text to prevent XML injection.
        return elem

    def _policies_subelement(self, parent, policies):
        """Create policies subelement."""
        #printing error messages instead of raising exceptions here because nothing is dependent on these tags existing.
        try:
            threshold = self._subelement_with_text(parent, 'threshold', policies['lockout_threshold'], attrib={'desc': 'lockout threshold'})
        except Exception as e:
            self.log.warning("Error creating lockout_threshold tag. {0}".format(e))
        try:
            duration = self._subelement_with_text(parent, 'duration', policies['lockout_duration'], attrib={'desc': 'lockout duration'})
        except Exception as e:
            self.log.warning("Error creating lockout_duration tag. {0}".format(e))
        try:
            observation = self._subelement_with_text(parent, 'observation', policies['lockout_observation_window'], attrib={'desc': 'lockout observation window'})
        except Exception as e:
            self.log.warning("Error creating lockout_observation_window tag. {0}".format(e))
        try:
            minpasswd = self._subelement_with_text(parent, 'minpasswd', policies['min_passwd_len'], attrib={'desc': 'minimum password length'})
        except Exception as e:
            self.log.warning("Error creating min_passwd_len tag. {0}".format(e))
        try:
            maxage = self._subelement_with_text(parent, 'maxage', policies['max_passwd_age'], attrib={'desc': 'maximum password age'})
        except Exception as e:
            self.log.warning("Error creating max_passwd_age tag. {0}".format(e))
        try:
            maxage = self._subelement_with_text(parent, 'minage', policies['min_passwd_age'], attrib={'desc': 'minimum password age'})
        except Exception as e:
            self.log.warning("Error creating min_passwd_age tag. {0}".format(e))
        try:
            histpasswd = self._subelement_with_text(parent, 'histpasswd', policies['password_hist_len'], attrib={'desc': 'password history length'})
        except Exception as e:
            self.log.warning("Error creating password_hist_len tag. {0}".format(e))
        try:
            forcelogoff = self._subelement_with_text(parent, 'forcelogoff', policies['force_logoff'], attrib={'desc': 'force log off'})
        except Exception as e:
            self.log.warning("Error creating force_logoff tag. {0}".format(e))
        try:
            complexity = self._subelement_with_text(parent, 'complexity', policies['complexity'], attrib={'desc': 'password complexity policy'})
        except Exception as e:
            self.log.warning("Error creating complexity tag.")

    def _shares_subelement(self, parent, share_info):
        """Create share subelement."""
        try:
            share = self._subelement_with_text(parent, 'share', None, attrib={'desc': 'share information'})
        except Exception as e:
            self.log.warning("Error creating share tag. {0}".format(e))
        else:
            # Sub tags are dependent on parent tag.
            try:
                name = self._subelement_with_text(share, 'name', share_info['name'], attrib={'desc': 'share name'})
            except Exception as e:
                    self.log.warning("Error creating share name tag. {0}".format(e))
            try:
                remark = self._subelement_with_text(share, 'remark', share_info['remark'], attrib={'desc': 'share remark'})
            except Exception as e:
                    self.log.warning("Error creating share remark tag. {0}".format(e))
            try:
                type_ = self._subelement_with_text(share, 'type', share_info['type_'], attrib={'desc': 'share type'})
            except Exception as e:
                    self.log.warning("Error creating share type tag. {0}".format(e))

    def _group_subelement(self, parent, group):
        """Create group subelement."""
        if isinstance(group, Group):
            try:
                grp = self._subelement_with_text(parent, 'group', None, attrib={'desc': 'domain group information'})
            except Exception as e:
                self.log.warning("Error creating group tag. {0}".format(e))
            else:
                # Sub tags are dependent on parent tags.
                try:
                    name = self._subelement_with_text(grp, 'name', group.get_group_name(), attrib={'desc': 'group name'})
                except Exception as e:
                    self.log.warning("Error creating group name tag. {0}".format(e))
                try:
                    comment = self._subelement_with_text(grp, 'comment', group.get_group_comment(), attrib={'desc': 'group comment'})
                except Exception as e:
                    self.log.warning("Error creating group comment tag. {0}".format(e))
                try:
                    members = self._subelement_with_text(grp, 'members', None, attrib={'desc': 'group members'})
                except Exception as e:
                    self.log.warning("Error creating group members tag. {0}".format(e))
                else:
                    # Creating group member tags is dependent on getting the members.
                    try:
                        for group_member in group.get_members():
                            try:
                                usr_name = self._subelement_with_text(members, 'member', group_member, attrib={'desc': 'member name'})
                            except Exception as e:
                                self.log.warning("Error creating group members tag. {0}".format(e))
                    except Exception as e:
                        self.log.warning("Error creating group members tag. {0}".format(e))
        else:
            self.log.warning("'group' is not an instance of data.Group.")

    def _user_subelement(self, parent, user):
        """Create user subelement."""
        if isinstance(user, User):
            # Note long integers are being converted to strings.
            try:
                usr = self._subelement_with_text(parent, 'usr', None, attrib={'desc': 'user account information'})
            except Exception as e:
                self.log.warning("Error creating usr tag. {0}".format(e))
            else:
                # Sub tags are dependent on creating the parent tag.
                try:
                    self._subelement_with_text(usr, 'name', user.get_name())
                except Exception as e:
                    self.log.warning("Error creating name tag. {0}".format(e))
                try:
                    self._subelement_with_text(usr, 'comment', user.get_comment())
                except Exception as e:
                    self.log.warning("Error creating comment tag. {0}".format(e))
                try:
                    self._subelement_with_text(usr, 'workstations', user.get_workstations())
                except Exception as e:
                    self.log.warning("Error creating workstations tag. {0}".format(e))
                try:
                    self._subelement_with_text(usr, 'country_code', user.get_country_code())
                except Exception as e:
                    self.log.warning("Error creating country_code tag. {0}".format(e))
                try:
                    self._subelement_with_text(usr, 'last_logon', user.get_last_logon())
                except Exception as e:
                    self.log.warning("Error creating last_logon tag. {0}".format(e))
                try:
                    self._subelement_with_text(usr, 'password_expired', user.get_password_expired())
                except Exception as e:
                    self.log.warning("Error creating password_expired tag. {0}".format(e))
                try:
                    self._subelement_with_text(usr, 'full_name', user.get_full_name())
                except Exception as e:
                    self.log.warning("Error creating full_name tag. {0}".format(e))
                try:
                    # #self._subelement_with_text(usr, 'parms', (user.get_parms()))
                    pass
                except Exception as e:
                    self.log.warning("Error creating parms tag. {0}".format(e))
                try:
                    self._subelement_with_text(usr, 'code_page', user.get_code_page())
                except Exception as e:
                    self.log.warning("Error creating code_page tag. {0}".format(e))
                try:
                    self._subelement_with_text(usr, 'priv', user.get_priv())
                except Exception as e:
                    self.log.warning("Error creating priv tag. {0}".format(e))
                try:
                    self._subelement_with_text(usr, 'auth_flags', user.get_auth_flags())
                except Exception as e:
                    self.log.warning("Error creating auth_flags tag. {0}".format(e))
                try:
                    self._subelement_with_text(usr, 'logon_server', user.get_logon_server())
                except Exception as e:
                    self.log.warning("Error creating logon_server tag. {0}".format(e))
                try:
                    self._subelement_with_text(usr, 'home_dir', user.get_home_dir())
                except Exception as e:
                    self.log.warning("Error creating home_dir tag. {0}".format(e))
                try:
                    self._subelement_with_text(usr, 'home_dir_drive', user.get_home_dir_drive())
                except Exception as e:
                    self.log.warning("Error creating home_dir_drive tag. {0}".format(e))
                try:
                    self._subelement_with_text(usr, 'usr_comment', user.get_usr_comment())
                except Exception as e:
                    self.log.warning("Error creating usr_comment tag. {0}".format(e))
                try:
                    self._subelement_with_text(usr, 'profile', user.get_profile())
                except Exception as e:
                    self.log.warning("Error creating profile tag. {0}".format(e))
                try:
                    self._subelement_with_text(usr, 'acct_expires', user.get_acct_expires())
                except Exception as e:
                    self.log.warning("Error creating acct_expires tag. {0}".format(e))
                try:
                    self._subelement_with_text(usr, 'primary_group_id', user.get_primary_group_id())
                except Exception as e:
                    self.log.warning("Error creating primary_group_id tag. {0}".format(e))
                try:
                    self._subelement_with_text(usr, 'bad_pw_count', user.get_bad_pw_count())
                except Exception as e:
                    self.log.warning("Error creating bad_pw_count tag. {0}".format(e))
                try:
                    self._subelement_with_text(usr, 'user_id', user.get_user_id())
                except Exception as e:
                    self.log.warning("Error creating user_id tag. {0}".format(e))
                try:
                    # #self._subelement_with_text(usr, 'logon_hours', user.get_logon_hours())
                    pass
                except Exception as e:
                    self.log.warning("Error creating logon_hours tag. {0}".format(e))
                try:
                    self._subelement_with_text(usr, 'password', user.get_password())
                except Exception as e:
                    self.log.warning("Error creating password tag. {0}".format(e))
                try:
                    self._subelement_with_text(usr, 'units_per_week', user.get_units_per_week())
                except Exception as e:
                    self.log.warning("Error creating units_per_week tag. {0}".format(e))
                try:
                    self._subelement_with_text(usr, 'last_logoff', user.get_last_logoff())
                except Exception as e:
                    self.log.warning("Error creating last_logoff tag. {0}".format(e))
                try:
                    self._subelement_with_text(usr, 'max_storage', user.get_max_storage())
                except Exception as e:
                    self.log.warning("Error creating max_storage tag. {0}".format(e))
                try:
                    self._subelement_with_text(usr, 'num_logons', user.get_num_logons())
                except Exception as e:
                    self.log.warning("Error creating num_logons tag. {0}".format(e))
                try:
                    self._subelement_with_text(usr, 'password_age', user.get_password_age())
                except Exception as e:
                    self.log.warning("Error creating password_age tag. {0}".format(e))
                try:
                    self._subelement_with_text(usr, 'flags', user.get_flags())
                except Exception as e:
                    self.log.warning("Error creating flags tag. {0}".format(e))
                try:
                    self._subelement_with_text(usr, 'script_path', user.get_script_path())
                except Exception as e:
                    self.log.warning("Error creating script_path tag. {0}".format(e))
        else:
            self.log.warning("'user' is not an instance of data.User.")

        try:
            groups = self._subelement_with_text(usr, 'membership', None, attrib={'desc': 'Groups user is a member of'})
        except Exception as e:
            self.log.warning("Error creating groups membership tag. {0}".format(e))
        else:
            try:
                for group in user.get_groups():
                    try:
                        self._subelement_with_text(groups, 'member_of', group)
                    except Exception as e:
                        self.log.warning("Error creating membership tag. {0}".format(e))
            except Exception as e:
                self.log.warning("Error creating membership groups tag. {0}".format(e))

    def _add_pi(self, xml):
        """Add processing instruction node."""
        return xml

    def _to_pretty_xml(self, xml):
        """Make xml readable by a human.
        Returns either a string of pretty xml or an emtpy string."""
        pretty_xml = ""
        try:
            rough_xml = ET.tostring(xml, encoding='utf-8')  # Output the xml to a string (no indentation).
        except Exception as e:
            self.log.critical("failed to output xml to rough string: {0}".format(e))
        else:
            try:
                reparsed = minidom.parseString(rough_xml)  # Reparse rough xml using minidom.
            except Exception as e:
                self.log.critical("failed to parse string to xml: {0}".format(e))
            else:
                reparsed = self._add_pi(reparsed)
                try:
                    pretty_xml = reparsed.toprettyxml(encoding='utf-8')  # Output the xml to a pretty string (with indentation).
                except Exception as e:
                    self.log.critical("failed to output xml to pretty string: {0}".format(e))
        return pretty_xml

    def _write_file(self, xml):
        """Write xml to file."""
        try:
            with open(self._filename, 'w') as out:
                out.writelines(xml)
        except Exception as e:
            self.log.critical("Failed to output xml to file: {0}".format(e))
        else:
            self.log.info("Written file: '{0}'".format(self._filename))

    def generate(self):
        """Generate XML report. Overrides BaseReporter.__init__()"""
        xml = self._to_rough_xml()
        pretty_xml = self._to_pretty_xml(xml)
        self._write_file(pretty_xml)


class DomainXMLReporter(BaseXMLReporter):
    """XML reporter class that handles Domain data structure."""
    def __init__(self, **kwargs):
        """Initialise DomainXMLReporter. Extends BaseXMLReporter.__init__()."""
        super(DomainXMLReporter, self).__init__(**kwargs)
        try:
            self._data = kwargs.pop('data')
        except Exception as e:
            raise Exception("Reporter requires data: {0}".format(e))
        else:
            if isinstance(self._data, list):
                if all(isinstance(x, Domain) for x in self._data):
                    pass
                else:
                    raise Exception("All elements in data must be instance of Domain.")  # TODO: Consider changing this so that any valid structures can be output.
            else:
                raise Exception("data must be a list of Domain instances.")
        finally:
            pass
            #del kwargs['data']  # Delete the data key from the kwargs argument dictionary so self._data is not overwritten by BaseReporter.__init__().

    def _add_pi(self, xml):
        """Add xml-stylesheet processing instruction node. Overrides BaseReporter._add_pi()."""
        original_xml = xml
        try:
            pi = xml.createProcessingInstruction('xml-stylesheet',
                                 'type="text/xsl" href="domain.xsl"')
            root = xml.firstChild
            xml.insertBefore(pi, root)
        except:
            self.log.warning("failed to add xml-stylesheet.")
            return original_xml
        else:
            return xml

    def _to_rough_xml(self):
        """Crate xml from Domain data."""
        try:
            xml = ET.Element('nettynum')
        except Exception as e:
            self.log.critical("Failed to create 'nettynum' xml tag. {0}".format(e))
            xml = ""
        else:
            try:
                for domain in self._get_data():
                    try:
                        dom = self._subelement_with_text(xml, 'domain', None, attrib={'desc': 'domain'})
                    except Exception as e:
                        self.log.warning("Error creating domain tag. {0}".format(e))
                    else:
                        try:
                            name = self._subelement_with_text(dom, 'name', domain.get_domain_name(), attrib={'desc': 'domain name'})
                        except Exception as e:
                            self.log.warning("Error creating domain name tag. {0}".format(e))
                        try:
                            dcs = self._subelement_with_text(dom, 'domain_controllers', None, attrib={'desc': 'domain controllers'})
                        except Exception as e:
                            self.log.warning("Error creating domain controllers tag. {0}".format(e))
                        else:
                            # sub tags dependent on parent tag
                            try:
                                for domain_controller in domain.get_domain_controllers():
                                    try:
                                        dc = self._subelement_with_text(dcs, 'dc', None, attrib={'desc': 'domain controller'})
                                    except Exception as e:
                                        self.log.warning("Error creating domain controller tag. {0}".format(e))
                                    else:
                                        # sub tag dependent on parent tag
                                        try:
                                            dc_ip = self._subelement_with_text(dc, 'dc_ip', domain_controller['dc_ip'], attrib={'desc': 'domain controller IP address'})
                                        except Exception as e:
                                            self.log.warning("Error creating dc ip tag. {0}".format(e))
                                        try:
                                            dc_fqdn = self._subelement_with_text(dc, 'dc_fqdn', domain_controller['dc_fqdn'], attrib={'desc': 'domain controller FQDN'})
                                        except Exception as e:
                                            self.log.warning("Error creating dc fqdn tag. {0}".format(e))
                                        try:
                                            dc_nb = self._subelement_with_text(dc, 'dc_nb', domain_controller['dc_nb'], attrib={'desc': 'domain controller NetBIOS name'})
                                        except Exception as e:
                                            self.log.warning("Error creating dc nb tag. {0}".format(e))
                            except Exception as e:
                                self.log.warning("Error getting domain controllers. {0}".format(e))

                        try:
                            policies = domain.get_policy()
                        except Exception as e:
                            self.log.warning("Error getting domain policy. {0}".format(e))
                        else:
                            try:
                                policy = self._subelement_with_text(dom, 'policy', None, attrib={'desc': 'accounts policy'})
                            except Exception as e:
                                self.log.warning("Error creating policy tag. {0}".format(e))
                            else:
                                try:
                                    self._policies_subelement(policy, policies)
                                except Exception as e:
                                    self.log.warning("Error creating policy sub tags. {0}".format(e))

                        try:
                            groups = self._subelement_with_text(dom, 'groups', None, attrib={'desc': 'domain groups'})
                        except Exception as e:
                            self.log.warning("Error creating groups tag. {0}".format(e))
                        else:
                            try:
                                for group in domain.get_groups():
                                    try:
                                        self._group_subelement(groups, group)
                                    except Exception as e:
                                        self.log.warning("Error creating groups sub tags. {0}".format(e))
                            except Exception as e:
                                self.log.warning("Error getting groups. {0}".format(e))

                        try:
                            users = self._subelement_with_text(dom, 'users', None, attrib={'desc': 'user accounts'})
                        except Exception as e:
                            self.log.warning("Error creating users tag. {0}".format(e))
                        else:
                            try:
                                # Doing it this way instead of bundling with group membership means that account information is only included in the file once..
                                for user in domain.get_users():
                                    try:
                                        self._user_subelement(users, user)
                                    except Exception as e:
                                        self.log.warning("Error creating user sub tags. {0}".format(e))
                            except Exception as e:
                                self.log.warning("Error getting users. {0}".format(e))
            except Exception as e:
                self.log.warning("Error getting data. {0}".format(e))

        return xml


class LocalXMLReporter(BaseXMLReporter):
    """XML reporter class that handles Host data structures."""
    def __init__(self, **kwargs):
        """Initialise DomainXMLReporter. Extends BaseXMLReporter.__init__()."""
        super(LocalXMLReporter, self).__init__(**kwargs)
        try:
            self._data = kwargs.pop('data')
        except Exception as e:
            raise Exception("Reporter requires data: {0}".format(e))
        else:
            if isinstance(self._data, list):
                if all(isinstance(x, Host) for x in self._data):
                    pass
                else:
                    raise Exception("All elements in data must be instance of Host.")  # TODO: Consider changing this so that any valid structures can be output.
            else:
                raise Exception("data must be a list of Host instances.")

    def _add_pi(self, xml):
        """Add xml-stylesheet processing instruction node. Overrides BaseReporter._add_pi()."""
        original_xml = xml
        try:
            pi = xml.createProcessingInstruction('xml-stylesheet',
                                 'type="text/xsl" href="hosts.xsl"')
            root = xml.firstChild
            xml.insertBefore(pi, root)
        except:
            self.log.warning("failed to add xml-stylesheet.")
            return original_xml
        else:
            return xml

    def _to_rough_xml(self):
        """Crate xml from Host data."""
        try:
            xml = ET.Element('nettynum')
        except Exception as e:
            self.log.critical("Failed to create 'nettynum' xml tag. {0}".format(e))
            xml = ""
        else:
            try:
                for host_data in self._get_data():
                    try:
                        host = self._subelement_with_text(xml, 'host', None, attrib={'desc': 'host'})
                    except Exception as e:
                        self.log.warning("Error creating host tag. {0}".format(e))
                    else:
                        try:
                            name = self._subelement_with_text(host, 'name', host_data.get_name(), attrib={'desc': 'name'})
                        except Exception as e:
                            self.log.warning("Error creating host name tag. {0}".format(e))

                    try:
                        policies = host_data.get_policy()
                    except Exception as e:
                        self.log.warning("Error getting policies. {0}".format(e))
                    else:
                        try:
                            policy = self._subelement_with_text(host, 'policy', None, attrib={'desc': 'accounts policy'})
                        except Exception as e:
                            self.log.warning("Error creating policy tag. {0}".format(e))
                        else:
                            try:
                                self._policies_subelement(policy, policies)
                            except Exception as e:
                                self.log.warning("Error creating policies sub tags. {0}".format(e))

                    try:
                        shares_info = host_data.get_shares()
                    except Exception as e:
                        self.log.warning("Error get shares. {0}".format(e))
                    else:
                        try:
                            shares = self._subelement_with_text(host, 'shares', None, attrib={'desc': 'shares'})
                        except Exception as e:
                            self.log.warning("Error creating shares tag. {0}".format(e))
                        else:
                            try:
                                for share_info in shares_info:
                                    try:
                                        self._shares_subelement(shares, share_info)
                                    except Exception as e:
                                        self.log.warning("Error creating shares sub tags. {0}".format(e))
                            except Exception as e:
                                self.log.warning("Error looping over shares_info. {0}".format(e))

                    try:
                        srvs = self._subelement_with_text(host, 'services', None, attrib={'desc': 'services'})
                    except Exception as e:
                        self.log.warning("Error creating services tag. {0}".format(e))
                    else:
                        try:
                            for service in host_data.get_services():
                                try:
                                    srv = self._subelement_with_text(srvs, 'srv', None, attrib={'desc': 'service'})
                                except Exception as e:
                                    self.log.warning("Error creating service tag. {0}".format(e))
                                else:
                                    try:
                                        srv_name = self._subelement_with_text(srv, 'srv_name', service['srv_name'], attrib={'desc': 'service name'})
                                    except Exception as e:
                                        self.log.warning("Error creating service name tag. {0}".format(e))
                                    try:
                                        port = self._subelement_with_text(srv, 'port', service['srv_port'], attrib={'desc': 'port service is running on'})
                                    except Exception as e:
                                        self.log.warning("Error creating port tag. {0}".format(e))
                        except Exception as e:
                            self.log.warning("Error getting services. {0}".format(e))

                    try:
                        groups = self._subelement_with_text(host, 'groups', None, attrib={'desc': 'domain groups'})
                    except Exception as e:
                        self.log.warning("Error creating groups tag. {0}".format(e))
                    else:
                        try:
                            for group in host_data.get_groups():
                                try:
                                    self._group_subelement(groups, group)
                                except Exception as e:
                                    self.log.warning("Error creating group sub tags. {0}".format(e))
                        except Exception as e:
                            self.log.warning("Error getting groups. {0}".format(e))

                    try:
                        users = self._subelement_with_text(host, 'users', None, attrib={'desc': 'user accounts'})
                    except Exception as e:
                        self.log.warning("Error creating users tag. {0}".format(e))
                    else:
                        try:
                            # Doing it this way instead of bundling with group membership means that account information is only included in the file once..
                            for user in host_data.get_users():
                                try:
                                    self._user_subelement(users, user)
                                except Exception as e:
                                    self.log.warning("Error creating user sub tags. {0}".format(e))
                        except Exception as e:
                            self.log.warning("Error getting users. {0}".format(e))
            except Exception as e:
                self.log.warning("Error getting data. {0}".format(e))

        return xml
