'''
.       .1111...          | Title: testing_authentication.py
    .10000000000011.   .. | Author: Oliver Morton
 .00              000...  | Email: grimhacker@grimhacker.com
1                  01..   | Description:
                    ..    | pyUnit testing of nettynum authentication
                   ..     |
GrimHacker        ..      |
                 ..       |
grimhacker.com  ..        |
@grimhacker    ..         | Created on: 14 Mar 2014
----------------------------------------------------------------------------
Nettynum - A Windows Domain Enumeration Tool
    Copyright (C) 2014  Oliver Morton

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import unittest
import os
import sys
import errno
sys.path.insert(0, os.path.abspath(".."))  # Add the directory above to the path so that we can find authenticators.

from reporters import DomainXMLReporter
from reporters import LocalXMLReporter

from data import Host
from data import Domain


class BaseReporterTestCase(unittest.TestCase):
    def setUp(self):
        self._filename = "test_file.xml"

    def tearDown(self):
        try:
            os.remove(self._filename)  # delete the test file.
        except OSError as e:
            if e.errno != errno.ENOENT:  # errno.ENOENT is no such file or directory
                raise  # re-raise exception


class DomainReporterTestCase(BaseReporterTestCase):
    def test_valid_data(self):
        try:
            data = Domain("test_domain")
            data.set_domain_controller(dc_ip="127.0.0.1", dc_fqdn="testdc.local", dc_nb="testdc")
            data.set_group("test_group", "test_group_comment", ["member1", "member2"])
            data.set_policy(force_logoff="false",
                            min_passwd_len="1",
                            max_passwd_age="2",
                            password_hist_len="3",
                            min_passwd_age="4",
                            lockout_observation_window="5",
                            lockout_duration="6",
                            lockout_threshold="7",
                            complexity="8")
            data.set_user(comment='comment1',
                        workstations='workstations1',
                        country_code='country_code1',
                        last_logon='last_logon1',
                        password_expired='password_expired1',
                        full_name='full_name1',
                        parms='parms1',
                        code_page='code_page1',
                        priv='priv1',
                        auth_flags='auth_flags1',
                        logon_server='logon_server1',
                        home_dir='home_dir1',
                        home_dir_drive='home_dir_drive1',
                        usr_comment='usr_comment1',
                        profile='profile1',
                        acct_expires='acct_expires1',
                        primary_group_id='primary_group_id1',
                        bad_pw_count='bad_pw_count1',
                        user_id='user_id1',
                        logon_hours='logon_hours1',
                        password='password1',
                        units_per_week='units_per_week1',
                        last_logoff='last_logoff1',
                        name='name1',
                        max_storage='max_storage1',
                        num_logons='num_logons1',
                        password_age='password_age1',
                        flags='flags1',
                        script_path='script_path1',
                        groups=["test_group"])
        except Exception as e:
            self.fail("Error creating valid data. {0}".format(e))

        try:
            reporter = DomainXMLReporter(data=[data], filename=self._filename)
        except Exception as e:
            self.fail("Failed to create instance of DomainXMLReporter. {0}".format(e))
        try:
            reporter.generate()
        except Exception as e:
            self.fail("DomainXMLReporter raised an exception: {0}".format(e))
        else:
            try:
                if os.stat(self._filename).st_size > 0:
                    pass  # file exists and its not empty
                else:
                    self.fail("DomainXMLReporter created an empty file.")
            except OSError:
                self.fail("DomainXMLReporter failed to create a file.")

    def test_invalid_data1(self):
        #List of Domain structures expected, sending list with Host structure.
        try:
            data = Host("test_host")
        except Exception as e:
            self.fail("Error creating invalid data. {0}".format(e))
        self.assertRaises(Exception, DomainXMLReporter, data=[data], filename=self._filename)

    def test_invalid_data2(self):
        #List of Domain structures expected, only sending one instance of Domain.
        try:
            data = Domain("test_domain")
        except Exception as e:
            self.fail("Error creating invalid data. {0}".format(e))
        self.assertRaises(Exception, DomainXMLReporter, data=data, filename=self._filename)

    def test_malformed_data1(self):
        # The reporter is designed to walk over all errors (except being given the wrong top level data structure) and complete successfully.
        # So, as unintuitive as this may look, if it throws an exception it fails.
        try:
            data = Domain("")
            # Overriding all the 'private' variables with None. (Lists/dictionaries are expected.)
            data._domain_name = None
            data._domain_controllers = None
            data._groups = None
            data._users = None
            data._policies = None
        except Exception as e:
            self.fail("Error creating invalid data. {0}".format(e))

        reporter = DomainXMLReporter(data=[data], filename=self._filename)
        reporter.generate()

        try:
            if os.stat(self._filename).st_size > 0:
                pass  # file exists and its not empty
            else:
                self.fail("DomainXMLReporter created an empty file.")
        except OSError:
            self.fail("DomainXMLReporter failed to create a file.")

    def test_malformed_data2(self):
        # The reporter is designed to walk over all errors (except being given the wrong top level data structure) and complete successfully.
        # So, as unintuitive as this may look, if it throws an exception it fails.
        try:
            data = Domain("test_domain")
            # Setting all nested data structures to None.
            data.set_domain_controller(dc_ip=None, dc_fqdn=None, dc_nb=None)
            data.set_group(None, None, None)
            data.set_policy(force_logoff=None,
                            min_passwd_len=None,
                            max_passwd_age=None,
                            password_hist_len=None,
                            min_passwd_age=None,
                            lockout_observation_window=None,
                            lockout_duration=None,
                            lockout_threshold=None,
                            complexity=None)
            data.set_user(comment=None,
                        workstations=None,
                        country_code=None,
                        last_logon=None,
                        password_expired=None,
                        full_name=None,
                        parms=None,
                        code_page=None,
                        priv=None,
                        auth_flags=None,
                        logon_server=None,
                        home_dir=None,
                        home_dir_drive=None,
                        usr_comment=None,
                        profile=None,
                        acct_expires=None,
                        primary_group_id=None,
                        bad_pw_count=None,
                        user_id=None,
                        logon_hours=None,
                        password=None,
                        units_per_week=None,
                        last_logoff=None,
                        name=None,
                        max_storage=None,
                        num_logons=None,
                        password_age=None,
                        flags=None,
                        script_path=None,
                        groups=None)
        except Exception as e:
            self.fail("Error creating invalid data. {0}".format(e))

        reporter = DomainXMLReporter(data=[data], filename=self._filename)
        reporter.generate()

        try:
            if os.stat(self._filename).st_size > 0:
                pass  # file exists and its not empty
            else:
                self.fail("DomainXMLReporter created an empty file.")
        except OSError:
            self.fail("DomainXMLReporter failed to create a file.")



class LocalReporterTestCase(BaseReporterTestCase):
    def test_valid_data(self):
        try:
            data = Host("test_host")
            data.set_group("test_group", "test_group_comment", ["member1", "member2"])
            data.set_policy(force_logoff="false",
                            min_passwd_len="1",
                            max_passwd_age="2",
                            password_hist_len="3",
                            min_passwd_age="4",
                            lockout_observation_window="5",
                            lockout_duration="6",
                            lockout_threshold="7",
                            complexity="8")
            data.set_service("service1", "1")
            data.set_user(comment='comment1',
                        workstations='workstations1',
                        country_code='country_code1',
                        last_logon='last_logon1',
                        password_expired='password_expired1',
                        full_name='full_name1',
                        parms='parms1',
                        code_page='code_page1',
                        priv='priv1',
                        auth_flags='auth_flags1',
                        logon_server='logon_server1',
                        home_dir='home_dir1',
                        home_dir_drive='home_dir_drive1',
                        usr_comment='usr_comment1',
                        profile='profile1',
                        acct_expires='acct_expires1',
                        primary_group_id='primary_group_id1',
                        bad_pw_count='bad_pw_count1',
                        user_id='user_id1',
                        logon_hours='logon_hours1',
                        password='password1',
                        units_per_week='units_per_week1',
                        last_logoff='last_logoff1',
                        name='name1',
                        max_storage='max_storage1',
                        num_logons='num_logons1',
                        password_age='password_age1',
                        flags='flags1',
                        script_path='script_path1',
                        groups=["test_group"])
        except Exception as e:
            self.fail("Error creating valid data. {0}".format(e))

        try:
            reporter = LocalXMLReporter(data=[data], filename=self._filename)
        except Exception as e:
            self.fail("Failed to create instance of LocalXMLReporter. {0}".format(e))
        try:
            reporter.generate()
        except Exception as e:
            self.fail("LocalXMLReporter raised an exception: {0}".format(e))
        else:
            try:
                if os.stat(self._filename).st_size > 0:
                    pass  # file exists and its not empty
                else:
                    self.fail("LocalXMLReporter created an empty file.")
            except OSError:
                self.fail("LocalXMLReporter failed to create a file.")

    def test_invalid_data1(self):
        #List of Host structures expected, sending list with Domain structure.
        try:
            data = Domain("test_domain")
        except Exception as e:
            self.fail("Error creating invalid data. {0}".format(e))
        self.assertRaises(Exception, LocalXMLReporter, data=[data], filename=self._filename)

    def test_invalid_data2(self):
        #List of Host structures expected, only sending one instance of Host.
        try:
            data = Host("test_host")
        except Exception as e:
            self.fail("Error creating invalid data. {0}".format(e))
        self.assertRaises(Exception, LocalXMLReporter, data=data, filename=self._filename)

    def test_malformed_data1(self):
        # The reporter is designed to walk over all errors (except being given the wrong top level data structure) and complete successfully.
        # So, as unintuitive as this may look, if it throws an exception it fails.
        try:
            data = Host("")
            # Overriding all the 'private' variables with None. (Lists/dictionaries are expected.)
            data._name = None
            data._services = None
            data._groups = None
            data._users = None
            data._policies = None
        except Exception as e:
            self.fail("Error creating invalid data. {0}".format(e))

        reporter = LocalXMLReporter(data=[data], filename=self._filename)
        reporter.generate()

        try:
            if os.stat(self._filename).st_size > 0:
                pass  # file exists and its not empty
            else:
                self.fail("LocalXMLReporter created an empty file.")
        except OSError:
            self.fail("LocalXMLReporter failed to create a file.")

    def test_malformed_data2(self):
        # The reporter is designed to walk over all errors (except being given the wrong top level data structure) and complete successfully.
        # So, as unintuitive as this may look, if it throws an exception it fails.
        try:
            data = Host("test_host")
            # Setting all nested data structures to None.
            data.set_group(None, None, None)
            data.set_policy(force_logoff=None,
                            min_passwd_len=None,
                            max_passwd_age=None,
                            password_hist_len=None,
                            min_passwd_age=None,
                            lockout_observation_window=None,
                            lockout_duration=None,
                            lockout_threshold=None,
                            complexity=None)
            data.set_service(None, None)
            data.set_user(comment=None,
                        workstations=None,
                        country_code=None,
                        last_logon=None,
                        password_expired=None,
                        full_name=None,
                        parms=None,
                        code_page=None,
                        priv=None,
                        auth_flags=None,
                        logon_server=None,
                        home_dir=None,
                        home_dir_drive=None,
                        usr_comment=None,
                        profile=None,
                        acct_expires=None,
                        primary_group_id=None,
                        bad_pw_count=None,
                        user_id=None,
                        logon_hours=None,
                        password=None,
                        units_per_week=None,
                        last_logoff=None,
                        name=None,
                        max_storage=None,
                        num_logons=None,
                        password_age=None,
                        flags=None,
                        script_path=None,
                        groups=None)
        except Exception as e:
            self.fail("Error creating invalid data. {0}".format(e))

        reporter = LocalXMLReporter(data=[data], filename=self._filename)
        reporter.generate()

        try:
            if os.stat(self._filename).st_size > 0:
                pass  # file exists and its not empty
            else:
                self.fail("LocalXMLReporter created an empty file.")
        except OSError:
            self.fail("LocalXMLReporter failed to create a file.")


if __name__ == "__main__":
    #Note these tests are dependent on external hosts. If you get unexpected results it could be caused by that.

    suite = unittest.TestSuite()
    loader = unittest.TestLoader()

    cases = [LocalReporterTestCase,
             DomainReporterTestCase]

    for case in cases:
        suite.addTests(loader.loadTestsFromTestCase(case))

    unittest.TextTestRunner(verbosity=2).run(suite)