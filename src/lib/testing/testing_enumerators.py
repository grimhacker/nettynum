'''
.       .1111...          | Title: testing_authentication.py
    .10000000000011.   .. | Author: Oliver Morton
 .00              000...  | Email: grimhacker@grimhacker.com
1                  01..   | Description:
                    ..    | pyUnit testing of nettynum authentication
                   ..     |
GrimHacker        ..      |
                 ..       |
grimhacker.com  ..        |
@grimhacker    ..         |Created on: 14 Mar 2014
----------------------------------------------------------------------------
Nettynum - A Windows Domain Enumeration Tool
    Copyright (C) 2014  Oliver Morton

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import unittest
import os, sys
sys.path.insert(0, os.path.abspath(".."))  # Add the directory above to the path so that we can find authenticators.

from authenticators import SMBAuthenticator
from authenticators import SNMPAuthenticator

from enumerators import NetLocalGroupEnum
from enumerators import NetLocalGroupGetMembers
from enumerators import NetUserEnum
from enumerators import NetUserGetInfo
from enumerators import NetUserGetLocalGroups
from enumerators import PasswordPolicy
from enumerators import LockoutPolicy
from enumerators import NetGroupEnum
from enumerators import NetGroupGetUsers
from enumerators import NetUserGetGroups
from enumerators import NetShareEnum

from enumerators import SNMPUserEnum

from enumerators import FromFQDN
from enumerators import FromObject
from enumerators import FromDNSServFQDN
from enumerators import FromMaster

from enumerators import DNSLookup
from enumerators import NetGetDC
from enumerators import DsGetDCName

class BaseSMBEnumerationTestCase(unittest.TestCase):
    def setUp(self):
        self._share = "IPC$"
        self._user = "user1"
        self._passwd = "Password1"

    def tearDown(self):
        self._auth.deauthenticate()


class BaseLocalSMBEnumerationTestCase(BaseSMBEnumerationTestCase):
    def setUp(self):
        super(BaseLocalSMBEnumerationTestCase, self).setUp()
        self._host = "10.10.10.1"
        self._domain = self._host
        self._auth = SMBAuthenticator(host=self._host, user=self._user, passwd=self._passwd, domain=self._domain, share=self._share)
        if self._auth.authenticate():
            pass
        else:
            raise Exception("Failed to authenticate.")


class BaseDomainSMBEnumerationTestCase(BaseSMBEnumerationTestCase):
    def setUp(self):
        super(BaseDomainSMBEnumerationTestCase, self).setUp()
        self._host = "10.10.10.1"
        self._domain = self._host
        self._auth = SMBAuthenticator(host=self._host, user=self._user, passwd=self._passwd, domain=self._domain, share=self._share)
        if self._auth.authenticate():
            pass
        else:
            raise Exception("Failed to authenticate.")


class BaseDomainControllerEnumerationTestCase(unittest.TestCase):
    pass


class BaseSNMPEnumerationTestCase(unittest.TestCase):
    def setUp(self):
        self._host = "10.10.10.1"
        self._community_string = "public"
        self._auth = SNMPAuthenticator(host=self._host, community_string=self._community_string)
        if self._auth.authenticate():
            pass
        else:
            raise Exception("Failed to authenticate.")


class BaseDomainNameEnumerationTestCase(unittest.TestCase):
    pass


class ShareSMBEnumerationTestCase(BaseLocalSMBEnumerationTestCase):
    def test_valid(self):
        enum = NetShareEnum(auth=self._auth, host=self._host)
        enum.enumerate()
        #print enum.get_shares()
        self.assertNotEqual(enum.get_shares(), [], "Failed to retrieve shares.")

    def test_invalid_auth(self):
        self.assertRaises(Exception, NetShareEnum, auth=None, host=self._host)

    def test_invalid_host(self):
        self.assertRaises(Exception, NetShareEnum, auth=self._auth, host=None)


class LocalGroupSMBEnumerationTestCase(BaseLocalSMBEnumerationTestCase):
    def test_valid(self):
        enum = NetLocalGroupEnum(auth=self._auth, host=self._host)
        enum.enumerate()
        #print enum.get_groups()
        self.assertNotEqual(enum.get_groups(), [], "Failed to retrieve groups.")

    def test_invalid_auth(self):
        self.assertRaises(Exception, NetLocalGroupEnum, auth=None, host=self._host)

    def test_invalid_host(self):
        self.assertRaises(Exception, NetLocalGroupEnum, auth=self._auth, host=None)


class LocalGroupMembersSMBEnumerationTestCase(BaseLocalSMBEnumerationTestCase):
    def test_valid(self):
        enum = NetLocalGroupGetMembers(auth=self._auth, host=self._host, group="Administrators")
        enum.enumerate()
        #print enum.get_members()
        self.assertNotEqual(enum.get_members(), [], "Failed to retrieve group members.")

    def test_invalid_auth(self):
        self.assertRaises(Exception, NetLocalGroupGetMembers, auth=None, host=self._host, group="Administrators")

    def test_invalid_host(self):
        self.assertRaises(Exception, NetLocalGroupGetMembers, auth=self._auth, host=None, group="Administrators")

    def test_invalid_group(self):
        self.assertRaises(Exception, NetLocalGroupGetMembers, auth=self._auth, host=self._host, group=None)

    def test_nonexistent_group(self):
        enum = NetLocalGroupGetMembers(auth=self._auth, host=self._host, group="NonexistentGroupName")
        enum.enumerate()
        #print enum.get_members()
        self.assertEqual(enum.get_members(), [], "Retrieved group members for nonexistent group.")


class LocalUsersSMBEnumerationTestCase(BaseLocalSMBEnumerationTestCase):
    def test_valid(self):
        enum = NetUserEnum(auth=self._auth, host=self._host)
        enum.enumerate()
        #print enum.get_info()
        self.assertNotEqual(enum.get_info(), [], "Failed to retrieve users.")

    def test_invalid_auth(self):
        self.assertRaises(Exception, NetUserEnum, auth=None, host=self._host)

    def test_invalid_host(self):
        self.assertRaises(Exception, NetUserEnum, auth=self._auth, host=None)


class LocalUserInfoSMBEnumerationTestCase(BaseLocalSMBEnumerationTestCase):
    def test_valid(self):
        enum = NetUserGetInfo(auth=self._auth, host=self._host, user="administrator")
        enum.enumerate()
        #print enum.get_info()
        self.assertNotEqual(enum.get_info(), {}, "Failed to retrieve user info.")

    def test_invalid_auth(self):
        self.assertRaises(Exception, NetUserGetInfo, auth=None, host=self._host, user="Administrator")

    def test_invalid_host(self):
        self.assertRaises(Exception, NetUserGetInfo, auth=self._auth, host=None, user="Administrator")

    def test_invalid_user(self):
        self.assertRaises(Exception, NetUserGetInfo, auth=self._auth, host=self._host, user=None)

    def test_nonexistent_user(self):
        enum = NetUserGetInfo(auth=self._auth, host=self._host, user="NonexistentUser")
        enum.enumerate()
        #print enum.get_info()
        self.assertEqual(enum.get_info(), {}, "Retrieved information about nonexistent user.")


class LocalGroupMembershipSMBEnumerationTestCase(BaseLocalSMBEnumerationTestCase):
    def test_valid(self):
        enum = NetUserGetLocalGroups(auth=self._auth, host=self._host, user="administrator")
        enum.enumerate()
        #print enum.get_groups()
        self.assertNotEqual(enum.get_groups(), [], "Failed to retrieve group membership.")

    def test_invalid_auth(self):
        self.assertRaises(Exception, NetUserGetLocalGroups, auth=None, host=self._host, user="Administrator")

    def test_invalid_host(self):
        self.assertRaises(Exception, NetUserGetLocalGroups, auth=self._auth, host=None, user="Administrator")

    def test_invalid_user(self):
        self.assertRaises(Exception, NetUserGetLocalGroups, auth=self._auth, host=self._host, user=None)

    def test_nonexistent_user(self):
        enum = NetUserGetLocalGroups(auth=self._auth, host=self._host, user="NonexistentUser")
        enum.enumerate()
        #print enum.get_info()
        self.assertEqual(enum.get_groups(), [], "Retrieved group membership for nonexistent user.")


class LocalPasswordPolicySMBEnumerationTestCase(BaseLocalSMBEnumerationTestCase):
    def test_valid(self):
        enum = PasswordPolicy(auth=self._auth, host=self._host)
        enum.enumerate()
        policy = {'force_logoff': "",
                 'min_passwd_len': "",
                 'max_passwd_age': "",
                 'password_hist_len': "",
                 'min_passwd_age': "",
                 'lockout_observation_window': "",
                 'lockout_duration': "",
                 'lockout_threshold': "",
                 'complexity': ""}
        #print enum.get_policy()
        self.assertNotEqual(enum.get_policy(), policy, "Failed to retrieve password policy.")

    def test_invalid_auth(self):
        self.assertRaises(Exception, PasswordPolicy, auth=None, host=self._host)

    def test_invalid_host(self):
        self.assertRaises(Exception, PasswordPolicy, auth=self._auth, host=None)


class LocalLockoutPolicySMBEnumerationTestCase(BaseLocalSMBEnumerationTestCase):
    def test_valid(self):
        enum = LockoutPolicy(auth=self._auth, host=self._host)
        enum.enumerate()
        policy = {'force_logoff': "",
                 'min_passwd_len': "",
                 'max_passwd_age': "",
                 'password_hist_len': "",
                 'min_passwd_age': "",
                 'lockout_observation_window': "",
                 'lockout_duration': "",
                 'lockout_threshold': "",
                 'complexity': ""}
        #print enum.get_policy()
        self.assertNotEqual(enum.get_policy(), policy, "Failed to retrieve lockout policy.")

    def test_invalid_auth(self):
        self.assertRaises(Exception, PasswordPolicy, auth=None, host=self._host)

    def test_invalid_host(self):
        self.assertRaises(Exception, PasswordPolicy, auth=self._auth, host=None)


class DomainGroupSMBEnumerationTestCase(BaseDomainSMBEnumerationTestCase):
    def test_valid(self):
        enum = NetGroupEnum(auth=self._auth, host=self._host)
        enum.enumerate()
        #print enum.get_groups()
        self.assertNotEqual(enum.get_groups(), [], "Failed to retrieve groups.")

    def test_invalid_auth(self):
        self.assertRaises(Exception, NetGroupEnum, auth=None, host=self._host)

    def test_invalid_host(self):
        self.assertRaises(Exception, NetGroupEnum, auth=self._auth, host=None)


class DomainGroupMembersSMBEnumerationTestCase(BaseDomainSMBEnumerationTestCase):
    def test_valid(self):
        enum = NetGroupGetUsers(auth=self._auth, host=self._host, group="Domain Admins")
        enum.enumerate()
        #print enum.get_members()
        self.assertNotEqual(enum.get_members(), [], "Failed to retrieve group members.")

    def test_invalid_auth(self):
        self.assertRaises(Exception, NetGroupGetUsers, auth=None, host=self._host, group="Domain Admins")

    def test_invalid_host(self):
        self.assertRaises(Exception, NetGroupGetUsers, auth=self._auth, host=None, group="Domain Admins")

    def test_invalid_group(self):
        self.assertRaises(Exception, NetGroupGetUsers, auth=self._auth, host=self._host, group=None)

    def test_nonexistent_group(self):
        enum = NetGroupGetUsers(auth=self._auth, host=self._host, group="NonexistentGroupName")
        enum.enumerate()
        #print enum.get_members()
        self.assertEqual(enum.get_members(), [], "Retrieved group members for nonexistent group.")


class DomainUsersSMBEnumerationTestCase(BaseDomainSMBEnumerationTestCase):
    def test_valid(self):
        enum = NetUserEnum(auth=self._auth, host=self._host)
        enum.enumerate()
        #print enum.get_info()
        self.assertNotEqual(enum.get_info(), [], "Failed to retrieve users.")

    def test_invalid_auth(self):
        self.assertRaises(Exception, NetUserEnum, auth=None, host=self._host)

    def test_invalid_host(self):
        self.assertRaises(Exception, NetUserEnum, auth=self._auth, host=None)


class DomainUserInfoSMBEnumerationTestCase(BaseDomainSMBEnumerationTestCase):
    def test_valid(self):
        enum = NetUserGetInfo(auth=self._auth, host=self._host, user="user1")
        enum.enumerate()
        #print enum.get_info()
        self.assertNotEqual(enum.get_info(), {}, "Failed to retrieve user info.")

    def test_invalid_auth(self):
        self.assertRaises(Exception, NetUserGetInfo, auth=None, host=self._host, user="user1")

    def test_invalid_host(self):
        self.assertRaises(Exception, NetUserGetInfo, auth=self._auth, host=None, user="user1")

    def test_invalid_user(self):
        self.assertRaises(Exception, NetUserGetInfo, auth=self._auth, host=self._host, user=None)

    def test_nonexistent_user(self):
        enum = NetUserGetInfo(auth=self._auth, host=self._host, user="NonexistentUser")
        enum.enumerate()
        #print enum.get_info()
        self.assertEqual(enum.get_info(), {}, "Retrieved information about nonexistent user.")


class DomainGroupMembershipSMBEnumerationTestCase(BaseDomainSMBEnumerationTestCase):
    def test_valid(self):
        enum = NetUserGetGroups(auth=self._auth, host=self._host, user="user1")
        enum.enumerate()
        #print enum.get_groups()
        self.assertNotEqual(enum.get_groups(), [], "Failed to retrieve group membership.")

    def test_invalid_auth(self):
        self.assertRaises(Exception, NetUserGetGroups, auth=None, host=self._host, user="user1")

    def test_invalid_host(self):
        self.assertRaises(Exception, NetUserGetGroups, auth=self._auth, host=None, user="user1")

    def test_invalid_user(self):
        self.assertRaises(Exception, NetUserGetGroups, auth=self._auth, host=self._host, user=None)

    def test_nonexistent_user(self):
        enum = NetUserGetGroups(auth=self._auth, host=self._host, user="NonexistentUser")
        enum.enumerate()
        #print enum.get_info()
        self.assertEqual(enum.get_groups(), [], "Retrieved group membership for nonexistent user.")


class DomainPasswordPolicySMBEnumerationTestCase(BaseDomainSMBEnumerationTestCase):
    def test_valid(self):
        enum = PasswordPolicy(auth=self._auth, host=self._host)
        enum.enumerate()
        policy = {'force_logoff': "",
                 'min_passwd_len': "",
                 'max_passwd_age': "",
                 'password_hist_len': "",
                 'min_passwd_age': "",
                 'lockout_observation_window': "",
                 'lockout_duration': "",
                 'lockout_threshold': "",
                 'complexity': ""}
        #print enum.get_policy()
        self.assertNotEqual(enum.get_policy(), policy, "Failed to retrieve password policy.")

    def test_invalid_auth(self):
        self.assertRaises(Exception, PasswordPolicy, auth=None, host=self._host)

    def test_invalid_host(self):
        self.assertRaises(Exception, PasswordPolicy, auth=self._auth, host=None)


class DomainLockoutPolicySMBEnumerationTestCase(BaseDomainSMBEnumerationTestCase):
    def test_valid(self):
        enum = LockoutPolicy(auth=self._auth, host=self._host)
        enum.enumerate()
        policy = {'force_logoff': "",
                 'min_passwd_len': "",
                 'max_passwd_age': "",
                 'password_hist_len': "",
                 'min_passwd_age': "",
                 'lockout_observation_window': "",
                 'lockout_duration': "",
                 'lockout_threshold': "",
                 'complexity': ""}
        #print enum.get_policy()
        self.assertNotEqual(enum.get_policy(), policy, "Failed to retrieve lockout policy.")

    def test_invalid_auth(self):
        self.assertRaises(Exception, PasswordPolicy, auth=None, host=self._host)

    def test_invalid_host(self):
        self.assertRaises(Exception, PasswordPolicy, auth=self._auth, host=None)


class UserSNMPEnumerationTestCase(BaseSNMPEnumerationTestCase):
    def test_valid(self):
        enum = SNMPUserEnum(auth=self._auth, host=self._host)
        enum.enumerate()
        #print enum.get_info()
        self.assertNotEqual(enum.get_info(), [], "Failed to retrieve users.")

    def test_invalid_auth(self):
        self.assertRaises(Exception, SNMPUserEnum, auth=None, host=self._host)

    def test_invalid_host(self):
        self.assertRaises(Exception, SNMPUserEnum, auth=self._auth, host=None)


class FromFQDNDomainNameEnumerationTestCase(BaseDomainNameEnumerationTestCase):
    def test_valid(self):
        enum = FromFQDN()
        enum.enumerate()
        self.assertNotEqual(enum.get_domain_names(), [], "No domain names found using fqdn.")


class FromObjectDomainNameEnumerationTestCase(BaseDomainNameEnumerationTestCase):
    def test_valid(self):
        enum = FromObject()
        enum.enumerate()
        self.assertNotEqual(enum.get_domain_names(), [], "No domain names found using object.")


class FromDNSServFQDNDomainNameEnumerationTestCase(BaseDomainNameEnumerationTestCase):
    def test_valid(self):
        enum = FromDNSServFQDN()
        enum.enumerate()
        self.assertNotEqual(enum.get_domain_names(), [], "No domain names found using dns server fqdn.")


class FromMasterDomainNameEnumerationTestCase(BaseDomainNameEnumerationTestCase):
    def test_valid(self):
        enum = FromMaster()
        enum.enumerate()
        self.assertNotEqual(enum.get_domain_names(), [], "No domain names found using master.")


class DNSLookupDomainControllerEnumerationTestCase(BaseDomainControllerEnumerationTestCase):
    def test_valid(self):
        domain_name = "testdomain.local"
        enum = DNSLookup(domain_name=domain_name)
        enum.enumerate()
        self.assertNotEqual(enum.get_domain_controllers(), [], "No domain controllers found using dns lookup.")

    def test_invalid_domain_name(self):
        domain_name = None
        self.assertRaises(Exception, DNSLookup, domain_name=None)

    def test_nonexistent_domain_name(self):
        domain_name = "nonexistentdomainname"
        enum = DNSLookup(domain_name=domain_name)
        enum.enumerate()
        self.assertEqual(enum.get_domain_controllers(), [], "Domain controllers found for non-existent domain name.")


class NetGetDCDomainControllerEnumerationTestCase(BaseDomainControllerEnumerationTestCase):
    def test_valid(self):
        domain_name = "TESTDOMAIN"
        enum = NetGetDC(domain_name=domain_name)
        enum.enumerate()
        self.assertNotEqual(enum.get_domain_controllers(), [], "No domain controllers found using NetGetDC.")

    def test_invalid_domain_name(self):
        domain_name = None
        self.assertRaises(Exception, NetGetDC, domain_name=None)

    def test_nonexistent_domain_name(self):
        domain_name = "nonexistentdomainname"
        enum = NetGetDC(domain_name=domain_name)
        enum.enumerate()
        self.assertEqual(enum.get_domain_controllers(), [], "Domain controllers found for non-existent domain name.")

class DsGetDCNameDomainControllerEnumerationTestCase(BaseDomainControllerEnumerationTestCase):
    def test_valid(self):
        domain_name = "TESTDOMAIN"
        enum = DsGetDCName(domain_name=domain_name)
        enum.enumerate()
        self.assertNotEqual(enum.get_domain_controllers(), [], "No domain controllers found using DsGetDCName.")

    def test_invalid_domain_name(self):
        domain_name = None
        self.assertRaises(Exception, DsGetDCName, domain_name=None)

    def test_nonexistent_domain_name(self):
        domain_name = "nonexistentdomainname"
        enum = DsGetDCName(domain_name=domain_name)
        enum.enumerate()
        self.assertEqual(enum.get_domain_controllers(), [], "Domain controllers found for non-existent domain name.")

if __name__ == "__main__":
    #Note these tests are dependent on external hosts. If you get unexpected results it could be caused by that.

    suite = unittest.TestSuite()
    loader = unittest.TestLoader()

    cases = [ShareSMBEnumerationTestCase,
             LocalGroupSMBEnumerationTestCase,
             LocalGroupMembersSMBEnumerationTestCase,
             LocalUsersSMBEnumerationTestCase,
             LocalUserInfoSMBEnumerationTestCase,
             LocalGroupMembershipSMBEnumerationTestCase,
             LocalPasswordPolicySMBEnumerationTestCase,
             LocalLockoutPolicySMBEnumerationTestCase,
             DomainGroupSMBEnumerationTestCase,
             DomainGroupMembersSMBEnumerationTestCase,
             DomainUsersSMBEnumerationTestCase,
             DomainUserInfoSMBEnumerationTestCase,
             DomainGroupMembershipSMBEnumerationTestCase,
             DomainPasswordPolicySMBEnumerationTestCase,
             DomainLockoutPolicySMBEnumerationTestCase,
             UserSNMPEnumerationTestCase,
             DNSLookupDomainControllerEnumerationTestCase,
             NetGetDCDomainControllerEnumerationTestCase,
             DsGetDCNameDomainControllerEnumerationTestCase,
             FromFQDNDomainNameEnumerationTestCase,
             FromObjectDomainNameEnumerationTestCase,
             FromObjectDomainNameEnumerationTestCase,
             FromDNSServFQDNDomainNameEnumerationTestCase,
             FromMasterDomainNameEnumerationTestCase]

    for case in cases:
        suite.addTests(loader.loadTestsFromTestCase(case))

    unittest.TextTestRunner(verbosity=2).run(suite)
    exit()
'''
    local_smb_suite = unittest.TestSuite()
    local_smb_cases = [LocalGroupSMBEnumerationTestCase,
                     LocalGroupMembersSMBEnumerationTestCase,
                     LocalUsersSMBEnumerationTestCase,
                     LocalUserInfoSMBEnumerationTestCase,
                     LocalGroupMembershipSMBEnumerationTestCase,
                     LocalPasswordPolicySMBEnumerationTestCase,
                     LocalLockoutPolicySMBEnumerationTestCase]
    for case in local_smb_cases:
        local_smb_suite.addTests(loader.loadTestsFromTestCase(case))

    domain_smb_suite = unittest.TestSuite()
    domain_smb_cases = [DomainGroupSMBEnumerationTestCase,
                     DomainGroupMembersSMBEnumerationTestCase,
                     DomainUsersSMBEnumerationTestCase,
                     DomainUserInfoSMBEnumerationTestCase,
                     DomainGroupMembershipSMBEnumerationTestCase,
                     DomainPasswordPolicySMBEnumerationTestCase,
                     DomainLockoutPolicySMBEnumerationTestCase]
    for case in domain_smb_cases:
        domain_smb_suite.addTests(loader.loadTestsFromTestCase(case))

    domain_name_suite = unittest.TestSuite()
    domain_name_cases = [FromFQDNDomainNameEnumerationTestCase,
                         FromObjectDomainNameEnumerationTestCase,
                         FromObjectDomainNameEnumerationTestCase,
                         FromDNSServFQDNDomainNameEnumerationTestCase,
                         FromMasterDomainNameEnumerationTestCase]
    for case in domain_name_cases:
        domain_name_suite.addTests(loader.loadTestsFromTestCase(case))

    domain_controller_suite = unittest.TestSuite()
    domain_controller_cases = [DNSLookupDomainControllerEnumerationTestCase,
                             NetGetDCDomainControllerEnumerationTestCase,
                             DsGetDCNameDomainControllerEnumerationTestCase]
    for case in domain_controller_cases:
        domain_controller_suite.addTests(loader.loadTestsFromTestCase(case))

    snmp_suite = unittest.TestSuite()
    snmp_cases = [UserSNMPEnumerationTestCase]
    for case in snmp_cases:
        snmp_suite.addTests(loader.loadTestsFromTestCase(case))
'''
