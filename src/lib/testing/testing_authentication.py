'''
.       .1111...          | Title: testing_authentication.py
    .10000000000011.   .. | Author: Oliver Morton
 .00              000...  | Email: grimhacker@grimhacker.com
1                  01..   | Description:
                    ..    | pyUnit testing of nettynum authentication
                   ..     |
GrimHacker        ..      |
                 ..       |
grimhacker.com  ..        |
@grimhacker    ..         |Created on: 14 Mar 2014
----------------------------------------------------------------------------
Nettynum - A Windows Domain Enumeration Tool
    Copyright (C) 2014  Oliver Morton

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import unittest
import os, sys
sys.path.insert(0, os.path.abspath(".."))  # Add the directory above to the path so that we can find authenticators.

from authenticators import SMBAuthenticator
from authenticators import SNMPAuthenticator


class BaseSMBAuthenticationTestCase(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        try:
            self._auth.deauthenticate()
        except AttributeError as e:
            pass
        except Exception as e:
            raise


class CredsSMBAuthenticaitonTestCase(BaseSMBAuthenticationTestCase):
    def test_valid(self):
        self._host = "10.10.10.1"
        self._share = "IPC$"
        self._user = "user1"
        self._passwd = "Password1"
        self._domain = self._host
        self._auth = SMBAuthenticator(host=self._host, user=self._user, passwd=self._passwd, domain=self._domain, share=self._share)
        self.assertTrue(self._auth.authenticate("smb"), "Failed to authenticate with valid credentials.")

    def test_invalid_password(self):
        self._host = "10.10.10.1"
        self._share = "IPC$"
        self._user = "user1"
        self._passwd = "invalidpassword"
        self._domain = self._host
        self._auth = SMBAuthenticator(host=self._host, user=self._user, passwd=self._passwd, domain=self._domain, share=self._share)
        self.assertFalse(self._auth.authenticate("smb"), "Successfully authenticated with invalid password.")

    def test_invalid_user(self):
        self._host = "10.10.10.1"
        self._share = "IPC$"
        self._user = "invaliduser"
        self._passwd = "Password1"
        self._domain = self._host
        self._auth = SMBAuthenticator(host=self._host, user=self._user, passwd=self._passwd, domain=self._domain, share=self._share)
        self.assertFalse(self._auth.authenticate("smb"), "Successfully authenticated with invalid user.")


class InvalidSMBAuthenticationTestCase(BaseSMBAuthenticationTestCase):
    def test_invalid_host(self):
        self._host = None
        self._share = "IPC$"
        self._user = "user1"
        self._passwd = "Password1"
        self._domain = self._host
        self.assertRaises(Exception,
                          SMBAuthenticator,
                          host=self._host,
                          user=self._user,
                          passwd=self._passwd,
                          domain=self._domain,
                          share=self._share)

    def test_invalid_share(self):
        self._host = "10.10.10.1"
        self._share = None
        self._user = "user1"
        self._passwd = "Password1"
        self._domain = self._host
        self.assertRaises(Exception,
                          SMBAuthenticator,
                          host=self._host,
                          user=self._user,
                          passwd=self._passwd,
                          domain=self._domain,
                          share=self._share)

    def test_invalid_user(self):
        self._host = "10.10.10.1"
        self._share = "IPC$"
        self._user = None
        self._passwd = "Password1"
        self._domain = self._host
        self.assertRaises(Exception,
                          SMBAuthenticator,
                          host=self._host,
                          user=self._user,
                          passwd=self._passwd,
                          domain=self._domain,
                          share=self._share)

    def test_invalid_passwd(self):
        self._host = "10.10.10.1"
        self._share = "IPC$"
        self._user = "user1"
        self._passwd = None
        self._domain = self._host
        self.assertRaises(Exception,
                          SMBAuthenticator,
                          host=self._host,
                          user=self._user,
                          passwd=self._passwd,
                          domain=self._domain,
                          share=self._share)

    def test_invalid_domain(self):
        self._host = "10.10.10.1"
        self._share = "IPC$"
        self._user = "user1"
        self._passwd = "Password1"
        self._domain = None
        self.assertRaises(Exception,
                          SMBAuthenticator,
                          host=self._host,
                          user=self._user,
                          passwd=self._passwd,
                          domain=self._domain,
                          share=self._share)


class SMBDeauthenticationTestCase(unittest.TestCase):
    def setUp(self):
        self._host = "10.10.10.1"
        self._domain = self._host
        self._share = "IPC$"
        self._user = "user1"
        self._passwd = "Password1"
        self._auth = SMBAuthenticator(host=self._host, user=self._user, passwd=self._passwd, domain=self._domain, share=self._share)
        if self._auth.authenticate() == False:
            raise Exception("Cannot test deauthentication without being able to authenticate.")

    def test_valid(self):
        try:
            self._auth.deauthenticate()
        except Exception as e:
            self.fail("Failed to deauthenticate. {0}".format(e))
        else:
            pass


class BaseSNMPAuthenticationTestCase(unittest.TestCase):
    def setUp(self):
        pass


class CredsSNMPAuthenticationTestCase(BaseSNMPAuthenticationTestCase):
    def test_valid(self):
        self._host = "10.10.10.1"
        self._community_string = "public"
        self._auth = SNMPAuthenticator(host=self._host, community_string=self._community_string)
        self.assertTrue(self._auth.authenticate(), "Failed to authenticate with valid community string.")

    def test_invalid_community_string(self):
        self._host = "10.10.10.1"
        self._community_string = "invalidcommunitystring"
        self._auth = SNMPAuthenticator(host=self._host, community_string=self._community_string)
        self.assertFalse(self._auth.authenticate(), "Successfully authenticated with invalid community string.")


class InvalidSNMPAuthenticationTestCase(BaseSNMPAuthenticationTestCase):
    def test_invalid_host(self):
        self._host = None
        self._community_string = "public"
        self.assertRaises(Exception,
                          SNMPAuthenticator,
                          host=self._host,
                          community_string=self._community_string)

    def test_invalid_community_string(self):
        self._host = "10.10.10.1"
        self._community_string = None
        self.assertRaises(Exception,
                          SNMPAuthenticator,
                          host=self._host,
                          community_string=self._community_string)

if __name__ == "__main__":
    #Note these tests are dependent on external hosts. If you get unexpected results it could be caused by that.

    suite = unittest.TestSuite()
    loader = unittest.TestLoader()

    cases = [CredsSMBAuthenticaitonTestCase,
             InvalidSMBAuthenticationTestCase,
             SMBDeauthenticationTestCase,
             CredsSNMPAuthenticationTestCase,
             InvalidSNMPAuthenticationTestCase]

    for case in cases:
        suite.addTests(loader.loadTestsFromTestCase(case))

    unittest.TextTestRunner(verbosity=2).run(suite)