'''
.       .1111...          | Title: automation.py
    .10000000000011.   .. | Author: Oliver Morton
 .00              000...  | Email: grimhacker@grimhacker.com
1                  01..   | Description:
                    ..    | All automation objects.
                   ..     |
GrimHacker        ..      |
                 ..       |
grimhacker.com  ..        |
@grimhacker    ..         | Created on: 7 Jan 2014
----------------------------------------------------------------------------
Nettynum - A Windows Domain Enumeration Tool
    Copyright (C) 2014  Oliver Morton

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
import logging
import os
import re

from data import Domain as Domain
from data import Host as Host
from authenticators import AuthenticationController as AuthenticationController
from enumerators import DomainNameEnumerationController as DomainNameEnumerationController
from enumerators import DomainControllerEnumerationController as DomainControllerEnumerationController
from enumerators import PolicyEnumerationController as PolicyEnumerationController
from enumerators import GroupEnumerationController as GroupEnumerationController
from enumerators import GroupMemberEnumerationController as GroupMemberEnumerationController
from enumerators import UserInfoEnumerationController as UserInfoEnumerationController
from enumerators import InterestingHostEnumerationController as InterestingHostEnumerationController
from enumerators import LocalGroupEnumerationController as LocalGroupEnumerationController
from enumerators import LocalGroupMemberEnumerationController as LocalGroupMemberEnumerationController
from enumerators import LocalGroupMembershipEnumerationController as LocalGroupMembershipEnumerationController
from enumerators import GroupMembershipEnumerationController as GroupMembershipEnumerationController
from enumerators import ShareEnumerationController as ShareEnumerationController
from reporters import DomainXMLReporter as DomainXMLReporter
from reporters import LocalXMLReporter as LocalXMLReporter


class BaseAutomation(object):
    """Base automation class."""
    def __init__(self, **kwargs):
        """Initialise base automation class."""
        self.log = logging.getLogger(__name__)
        self._data = []

        try:
            self._auth_domain = kwargs['auth_domain']
        except:
            self._auth_domain = ""

        try:
            self._auth_user = kwargs['auth_user']
        except:
            self._auth_user = ""

        try:
            self._auth_passwd = kwargs['auth_passwd']
        except:
            self._auth_passwd = ""

        try:
            self._auth_community = kwargs['auth_community']
        except:
            self._auth_community = ""

        self._regex = []
        try:
            #TODO: Pass this file in so the user can specify it.
            with open(os.path.join("conf","group_regex"), 'r') as f:  # File containing regular expressions, one per line.
                for line in f.readlines():
                    if line.strip() == "":
                        pass  # Ignore blank lines.
                    else:
                        try:
                            self._regex.append(re.compile(line.strip(), re.IGNORECASE))  # Compile regular expression (because it will be used repeatedly) ignoring case.
                        except Exception as e:
                            self.log.warning("Failed to add group name regex to list: {0}".format(e))
        except Exception as e:
            self.log.warning("Failed to get group name regular expressions. {0}".format(e))
            try:
                # If the regular expression couldn't be opened, fall back to a wild card.
                self._regex.append(re.compile(".*", re.IGNORECASE))  # Compile regular expression (because it will be used repeatedly) ignoring case.
            except Exception as e:
                self.log.warning("Failed to add fall back, wild card group name regex to list. {0}".format(e))

        self._forced_groups = []
        try:
            forced_groups_file = kwargs['forced_groups']  # File containing group names that should always have their members enumerated
                                                            # regardless of whether the group was found in group enumeration.
                                                            # This is to work around a 100 group limitation that may exist in the Windows API.
        except Exception as e:
            self.log.warning("Failed to get forced groups file. {0}".format(e))
        else:
            try:
                # Read file and put all the groups into a list.
                with open(forced_groups_file, 'r') as f:
                    for line in f:
                        try:
                            self._forced_groups.append(line.strip())
                        except Exception as e:
                            self.log.warning("Failed to append group to list of forced groups. {0}".format(e))
            except Exception as e:
                self.log.warning("Failed to open forced groups file '{0}', this groups may not be enumerated. {1}".format(forced_groups_file, e))

    def _get_forced_groups(self):
        """Return list of groups that must be enumerated."""
        return self._forced_groups

    def _get_group_name_regex(self):
        """Return list of compile group name regular expressions."""
        return self._regex

    def _create_reporter(self, filename, data):
        """Return an instance of a reporter object."""
        pass

    def get_data(self):
        """Return enumerated information."""
        return self._data

    def generate_report(self, **kwargs):
        """Generate report of enumerated data."""
        try:
            filename = kwargs['filename']
        except:
            filename = "nettynum.xml"

        try:
            # Create a reporter instance specific to the child class.
            reporter = self._create_reporter(filename, self._data)
        except Exception as e:
            self.log.warning("Failed to create reporter instance. {0}".format(e))
        else:
            try:
                # Generate the report.
                reporter.generate()
            except Exception as e:
                self.log.warning("Failed to generate report. {0}".format(e))


class DomainAutomation(BaseAutomation):
    """Domain automation class."""
    def __init__(self, **kwargs):
        """Initialise domain automation class. Extends BaseAutomation.__init__()."""
        super(DomainAutomation, self).__init__(**kwargs)

        try:
            domain_name = kwargs['domain_name']
        except:
            pass
        else:
            self._set_domain_name(domain_name)

        try:
            domain_controller = kwargs['domain_controller']
        except:
            pass
        else:
            # If domain controller is specified then domain name must be too. Domain name setup creates an
            # instance in data so can access it as self._data[0].
            if "." in domain_controller:
                # DNS style or IP address
                if re.match("[a-zA-Z]", domain_controller):
                    # DNS Style
                    self._data[0].set_domain_controller(dc_fqdn=domain_controller)
                else:
                    # IP address
                    self._data[0].set_domain_controller(dc_ip=domain_controller)
            else:
                # NetBIOS style
                self._data[0].set_domain_controller(dc_nb=domain_controller)

        try:
            self._group = kwargs['group']
        except:
            self._group = None


    def _create_reporter(self, filename, data):
        """Return a reporter object. Overrides BaseAutomation._create_reporter()."""
        return DomainXMLReporter(filename=filename, data=data)

    def _set_domain_name(self, domain_name):
        """Create domain object for domain_name."""
        domains = []
        if domain_name.endswith("."):
            domain_name = domain_name[:-1]  # strip the trailing "." from fully qualified domain names.
        # Get all the domain names that are in self._data in a list.
        for domain in self.get_data():
            domains.append(domain.get_domain_name())
        # If the new domain name is not already in the list of domains, create a new Domain instance to add it.
        if domain_name not in domains:
            self._data.append(Domain(domain_name))

    def enumerate(self):
        """Enumerate all information from the domain. Overrides BaseAutomation.enumerate()."""
        ignore_domains = ["workgroup","in-addr.arpa."]  # Domain names to ignore.
        if len(self.get_data()) == 0:
            # Domain names were not manually specified, so enumerate them with an instance of DomainNameEnumerationController.
            self.log.info("Enumerating domain names...")
            domain_name_enumerator = DomainNameEnumerationController()
            domain_name_enumerator.enumerate()
            for domain_name in domain_name_enumerator.get_domain_names():
                # For every domain name that has been enumerated:
                # List comprehension used to make each element of the ignore_domains list lower case before comparing it.
                if domain_name.lower() not in [x.lower() for x in ignore_domains]:
                    self._set_domain_name(domain_name)  # If the domain name isn't in the ignore list, set it to self.
                else:
                    self.log.debug("Ignoring domain '{0}'".format(domain_name))
        else:
            self.log.info("Using specified domain name.")

        if len(self.get_data()) == 0:
            # At this point either the domain name should have been specified or we should have found some.
            # If not there is nothing more we can do so exit.
            self.log.critical("No domain names to enumerate.")
            exit()
        else:
            for domain in self.get_data():
                # For each domain name in the list of Domain elements.
                self.log.info("Working on domain '{0}'...".format(domain.get_domain_name()))
                if len(domain.get_domain_controllers()) == 0:
                    # Domain controller not manually specified, so enumerate them using an instance of DomainControllerEnumerationController.
                    self.log.info("Enumerating domain controllers...")
                    domain_controller_enumerator = DomainControllerEnumerationController(domain_name=domain.get_domain_name())
                    domain_controller_enumerator.enumerate()
                    for domain_controller in domain_controller_enumerator.get_domain_controllers():
                        # For each domain controller that has been enumerated, set it to self.
                        domain.set_domain_controller(**domain_controller)
                else:
                    self.log.info("Using specified domain controller.")
                if len(domain.get_domain_controllers()) == 0:
                    # At this point the domain controller should either have been specified by the user or discovered.
                    # If not there is nothing more we can do for this domain.
                    self.log.warning("no domain controllers for domain: '{0}'".format(domain.get_domain_name()))
                    break  # Proceed to the next domain.
                else:
                    # Found at least one domain controller so continue.
                    for domain_controller in domain.get_domain_controllers():
                        # dc_nb first because NetValidatePasswordPolicy requires netbios name and will error with fqdn or ip.
                        host = domain_controller['dc_nb'] or domain_controller['dc_ip'] or domain_controller['dc_fqdn']
                        # Authenticate to the domain controller.
                        self.log.info("Authenticating to domain controller...")
                        # Create authenticationController with specified credentials.
                        auth = AuthenticationController(user=self._auth_user, host=host, passwd=self._auth_passwd, domain=self._auth_domain, community_string=self._auth_community)
                        if auth.create_sessions():  # If authentication controller has created any sessions.
                            # Get Policies.
                            self.log.info("Enumerating domain accounts policies...")
                            policy_enumerator = PolicyEnumerationController(auth=auth)
                            policy_enumerator.enumerate()
                            domain.set_policy(**policy_enumerator.get_policy())

                            # Get Groups
                            if self._group is not None:
                                # Group name has been specified so use it.
                                self.log.info("Using specified group name.")
                                domain.set_group(name=self._group)
                            else:
                                # Group name has not be specified, so enumerate them.
                                self.log.info("Enumerating groups...")
                                group_enumerator = GroupEnumerationController(auth=auth)
                                group_enumerator.enumerate()
                                for group in group_enumerator.get_groups():
                                    domain.set_group(**group)
                                # Add forced groups to list of groups so that their members will be enumerated.
                                self.log.debug("Adding forced groups to list of groups.")
                                for group in self._get_forced_groups():
                                    domain.set_group(name=group)

                            self.log.info("Enumerating members of groups that match the specified regex or are forced...")
                            for group in domain.get_groups():
                                enumerate_group = False  # Flag to indicate whether the group should be enumerated or not.
                                # Considered making this a function rather than using a flag, but it would get very messy
                                # due to the number of variables that needed to be passed. And it wouldn't be consistent with
                                # the rest of the enumeration that is done in this function.
                                if enumerate_group == False and group.get_group_name().lower() in (forced_group.lower() for forced_group in self._get_forced_groups()):
                                    # Group hasn't already been marked to enumerate and is in the list of forced groups to enumerate so must be enumerated.
                                    self.log.debug("Group: '{0}' is in the list of forced groups.".format(group.get_group_name()))
                                    enumerate_group = True

                                if enumerate_group == False:  # Group hasn't already been marked to enumerate so check if it matches the regex.
                                    for regex in self._get_group_name_regex():
                                        if regex.match(group.get_group_name()):
                                            self.log.debug("Group: '{0}' matches regex: '{1}'.".format(group.get_group_name(), regex.pattern))
                                            # Group name matches the regex of groups to enumerate so must be enumerated.
                                            enumerate_group = True
                                            break  # If any one regex is matched then the group must be enumerated
                                                    # so don't need to check the rest when a match has been found.
                                if enumerate_group:
                                    # Flag has been set to True indicating that the group must be enumerated.
                                    # Get list of members for groups.
                                    group_member_enumerator = GroupMemberEnumerationController(auth=auth, group=group.get_group_name())
                                    group_member_enumerator.enumerate()
                                    for member in group_member_enumerator.get_members():
                                        group.set_member(member)
                                        # Get group member account information.
                                        user_enumerator = UserInfoEnumerationController(user=member, auth=auth)
                                        user_enumerator.enumerate()
                                        if user_enumerator.get_info() != {}:  # Without this check set_user will error if the required keys aren't present.
                                            # Get groups user is a member of.
                                            membership_enumerator = GroupMembershipEnumerationController(auth=auth, user=member)
                                            membership_enumerator.enumerate()
                                            domain.set_user(groups=membership_enumerator.get_groups(), **user_enumerator.get_info())  # Set member user information to self.

                            # Deauthenticate from the domain controller.
                            self.log.info("Deauthenticating from domain controller...")
                            auth.destroy_sessions()
                            break  # Enumerated one domain controller, they all hold the same information so don't need to do the rest. Break out of the domain controller loop.
                        else:
                            if self._auth_user == "":
                                # Trying to use NULL session, so continue to the next domain controller as that can't be locked out
                                # And NULL session could be disabled on one domain controller but enabled on another.
                                self.log.debug("Authentication Failed. Trying to use NULL user so moving attempting to authenticate to next domain controller.")
                                continue
                            else:
                                # Not using a NULL session, don't want to lock out a user so stop.
                                self.log.debug("Authentication Failed. Not using NULL user giving up on domain controller authentication.")
                                break


class LocalAutomation(BaseAutomation):
    """Domain automation class."""
    def __init__(self, **kwargs):
        """Initialise local automation class. Extends BaseAutomation.__init__()."""
        super(LocalAutomation, self).__init__(**kwargs)

        self._domain_names = []
        try:
            self._domain_name.append(kwargs['domain_name'])
        except:
            pass

        try:
            self._group = kwargs['group']
        except:
            self._group = None

        try:
            self._set_host(name=kwargs['host'])
        except:
            pass

    def _create_reporter(self, filename, data):
        """Return a reporter object. Overrides BaseAutomation._create_reporter()."""
        return LocalXMLReporter(filename=filename, data=data)

    def _set_host(self, name):
        """Add host to list of hosts"""
        host_in_list = False  # Flag to indicate whether host is in the list or not.
        for host in self._data:
            if name == host.get_name(): # Check if the new host has the same name as an existing host in self._data
                host_in_list = True  # If the host is in the list set the flag to True.
        if host_in_list:
            self.log.debug("ignoring duplicate host name: '{0}'".format(name))
        else:
            # Host is not already in the list so create a Host instance for it.
            self._data.append(Host(name=name))

    def _set_domain_name(self, name):
        """Add domain name to list of domain names if it is not already known."""
        if name not in self._domain_names:
            # If the domain name is not already in the list, add it.
            self._domain_names.append(name)

    def get_domain_names(self):
        """Return list of domain names."""
        return self._domain_names

    def enumerate(self):
        """Enumerate all information from local machines. Overrides BaseAutomation.enumerate()."""
        # get domain names
        if len(self.get_data()) == 0:
            # Host was not specified.
            if len(self.get_domain_names()) == 0:
                # Domain names were not manually specified, so enumerate them.
                self.log.info("Enumerating domain names...")
                domain_name_enumerator = DomainNameEnumerationController()
                domain_name_enumerator.enumerate()
                for domain_name in domain_name_enumerator.get_domain_names():
                    self._set_domain_name(domain_name)  # Set each domain name that has been enumerated to self.
            else:
                self.log.info("Using specified domain name.")

            if len(self.get_domain_names()) == 0:
                # At this point either the user should have specified a domain name or we should have enumerated some.
                # If not there is nothing we can do so exit.
                self.log.critical("No domain names to enumerate.")
                exit()
            else:
                for domain in self.get_domain_names():
                    self.log.info("Working on domain '{0}'".format(domain))
                    # for each domain name: get hosts based on domain name
                    self.log.info("Enumerating 'interesting' hosts...")
                    interesting_hosts_enumerator = InterestingHostEnumerationController(domain=domain)
                    interesting_hosts_enumerator.enumerate()
                    for host_service in interesting_hosts_enumerator.get_hosts():
                        #print host_service
                        self._set_host(host_service['name'])  # Add host names.

                    for host in self.get_data():
                        for host_service in interesting_hosts_enumerator.get_hosts():
                            if host_service['name'] == host.get_name():
                                host.set_service(host_service['service'], host_service['port'])  # Add services.
        else:
            self.log.info("Using specified host.")

        # At this point hosts to enumerate have either been manually specified or discovered.
        if len(self.get_data()) == 0:
            # At this point either the user has specified a host or we have discovered one (or more).
            # If not there is nothing more we can do so exit.
            self.log.critical("No hosts to enumerate.")
            exit()
        else:
            for host_data in self.get_data():
                # For each host that we know about.
                host = host_data.get_name()
                self.log.info("Working on '{0}'...".format(host))
                # Create Sessions
                self.log.info("Authenticating...")
                auth = AuthenticationController(user=self._auth_user, passwd=self._auth_passwd, host=host, domain=self._auth_domain, community_string=self._auth_community)
                auth.create_sessions()

                # Get Policies.
                self.log.info("Enumerating accounts policies...")
                policy_enumerator = PolicyEnumerationController(auth=auth)
                policy_enumerator.enumerate()
                host_data.set_policy(**policy_enumerator.get_policy())

                # Get Shares.
                self.log.info("Enumerating shares...")
                share_enumerator = ShareEnumerationController(auth=auth)
                share_enumerator.enumerate()
                for share in share_enumerator.get_shares():
                    host_data.set_share(**share)

                if self._group is not None:
                    # Group name has been specified so use it.
                    self.log.info("Using specified group name.")
                    host_data.set_group(name=self._group)
                else:
                    # Group name has not be specified, so enumerate them.
                    self.log.info("Enumerating groups...")
                    group_enumerator = LocalGroupEnumerationController(auth=auth)
                    group_enumerator.enumerate()
                    for group in group_enumerator.get_groups():
                        host_data.set_group(**group)
                    self.log.debug("Adding forced groups to list of groups.")
                    # Add forced groups to list of groups so that their members will be enumerated.
                    for group in self._get_forced_groups():
                        host_data.set_group(name=group)

                    self.log.info("Enumerating group members...")
                    for group in host_data.get_groups():
                        enumerate_group = False  # Flag to indicate whether the group should be enumerated or not.
                        # Considered making this a function rather than using a flag, but it would get very messy
                        # due to the number of variables that needed to be passed. And it wouldn't be consistent with
                        # the rest of the enumeration that is done in this function.
                        if enumerate_group == False and group.get_group_name().lower() in (forced_group.lower() for forced_group in self._get_forced_groups()):
                            # Group hasn't already been marked to enumerate and is in the list of forced groups to enumerate so must be enumerated.
                            self.log.debug("Group: '{0}' is in the list of forced groups.".format(group.get_group_name()))
                            enumerate_group = True

                        if enumerate_group == False:  # Group hasn't already been marked to enumerate so check if it matches the regex.
                            for regex in self._get_group_name_regex():
                                if regex.match(group.get_group_name()):
                                    self.log.debug("Group: '{0}' matches regex: '{1}'.".format(group.get_group_name(), regex.pattern))
                                    # Group name matches the regex of groups to enumerate so must be enumerated.
                                    enumerate_group = True
                                    break  # If any one regex is matched then the group must be enumerated
                                            # so don't need to check the rest when a match has been found.

                        if enumerate_group:
                            # Flag has been set to True indicating that the group must be enumerated.
                            # Get list of members for groups.
                            group_member_enumerator = LocalGroupMemberEnumerationController(auth=auth, group=group.get_group_name())
                            group_member_enumerator.enumerate()
                            for member in group_member_enumerator.get_members():
                                group.set_member(member)
                                # Get group member account information.
                                user_enumerator = UserInfoEnumerationController(user=member, auth=auth)
                                user_enumerator.enumerate()
                                if user_enumerator.get_info() != {}:  # Without this check set_user will error if the required keys aren't present.
                                    # Get user's group membership.
                                    membership_enumerator = LocalGroupMembershipEnumerationController(auth=auth, user=member)
                                    membership_enumerator.enumerate()
                                    host_data.set_user(groups=membership_enumerator.get_groups(), **user_enumerator.get_info())
                            group_member_enumerator = None  # TODO: Don't think this is required. Test removing it.
                        else:
                            pass  # group does not need to be enumerated.

                # Deauthenticate from the domain controller.
                self.log.info("Deauthenticating...")
                auth.destroy_sessions()


if __name__ == '__main__':
    logging.basicConfig(format='%(levelname)s - %(module)s: %(message)s', level=logging.DEBUG)
