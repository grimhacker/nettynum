<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
Nettynum - A Windows Domain Enumeration Tool
    Copyright (C) 2014  Oliver Morton

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<html>
			<head>
				<style>
					p {
						margin-top: .25cm;
						margin-right: 0cm;
						margin-bottom: 0cm;
						margin-left: 0cm;
						margin-bottom: .0001pt;
						line-height: normal;
						font-size: 11.0pt;
						font-family: "Calibri","sans-serif";
						}
					div {
						margin-top: 0cm;
						margin-right: 0cm;
						margin-bottom: 0cm;
						margin-left: 0cm;
						margin-bottom: .0001pt;
						line-height: normal;
						font-size: 11.0pt;
						font-family: "Calibri","sans-serif";
						}
					h1 {
						margin-top: 24.0pt;
						margin-right: 0cm;
						margin-bottom: 0cm;
						margin-left: 0cm;
						margin-bottom: .0001pt;
						text-indent: 18.0pt;
						line-height: 150%;
						page-break-after: avoid;
						background: #33296B;
						font-size: 14.0pt;
						font-family: "Cambria","serif";
						color: white;
						text-transform: uppercase;
						letter-spacing: 1.0pt;
					}
					h2 {
						margin-top: 10.0pt;
						margin-right: 0cm;
						margin-bottom: 0cm;
						margin-left: 0cm;
						margin-bottom: .0001pt;
						line-height: 115%;
						page-break-after: avoid;
						background: #0078BE;
						font-size: 13.0pt;
						font-family: "Cambria","serif";
						color: white;
						text-transform: uppercase;
						font-weight: normal;
					}
					h3 {
						margin-top: 10.0pt;
						margin-right: 0cm;
						margin-bottom: 0cm;
						margin-left: 0cm;
						margin-bottom: .0001pt;
						line-height: 200%;
						page-break-after: avoid;
						font-size: 11.0pt;
						font-family: "Cambria","serif";
						color: gray;
						text-transform: uppercase;
					}
					h4{
						margin-top: 6.0pt;
						margin-right: 0cm;
						margin-bottom: 6.0pt;
						margin-left: 0cm;
						line-height: 115%;
						font-size: 11.0pt;
						font-family: "Calibri","sans-serif";
					}
					a {
						color: blue;
					}
					a:visited, a:link {
						text-decoration: none;
					}
					a:hover, a:active {
						text-decoration: underline;
					}
					div.margin1 {
						margin-left:15px;
					}
					div.margin2 {
						margin-left:20px;
					}
				</style>
				<title>Nettynum Report</title>
				<script>
					function toggle(caller, id){
						var element = document.getElementById(id);
						if(element.style.display  == 'none'){

							caller.innerHTML = caller.innerHTML.replace("[+]","[-]");
							element.style.display  = 'block';
						}
						else
						{
							caller.innerHTML = caller.innerHTML.replace("[-]","[+]");
							element.style.display  = 'none';
						}
						toggle.preventDefault();
					}
				</script>
			</head>
			<body>
				<div id="byline">
					Report from nettynum.py by GrimHacker
				</div>
				<xsl:for-each select="nettynum">
					<div id="container">
						<xsl:for-each select="host">
							<p>
								<div>
									<h1><a href="javascript:void(0)" style="color:white" id="{generate-id()}a" onclick="toggle(this, '{generate-id(current())}')">[-]</a> Host: <xsl:value-of select="name"/></h1>
								</div>
								<div id="{generate-id(current())}">
									<div>
										<h2><a href="javascript:void(0)" style="color:white" id="{generate-id()}" onclick="toggle(this, '{generate-id(policy)}')">[-]</a> Policies</h2>
										<div class="margin1" id="{generate-id(policy)}">
											<p>
												<xsl:if test="policy/threshold != ''">
													<strong>Lockout Threshold: </strong><xsl:value-of select="policy/threshold"/><br />
												</xsl:if>
												<xsl:if test="policy/duration != ''">
													<strong>Lockout Duration: </strong><xsl:value-of select="policy/duration"/> Minutes<br />
												</xsl:if>
												<xsl:if test="policy/observation != ''">
													<strong>Lockout Observation Window: </strong><xsl:value-of select="policy/observation"/> Minutes<br />
												</xsl:if>
												<xsl:if test="policy/minpasswd != ''">
													<strong>Minimum Password Length: </strong><xsl:value-of select="policy/minpasswd"/><br />
												</xsl:if>
												<xsl:if test="policy/maxage != ''">
													<strong>Maximum Password Age: </strong><xsl:value-of select="policy/maxage"/> Days<br />
												</xsl:if>
												<xsl:if test="policy/minage != ''">
													<strong>Minimum Password Age: </strong><xsl:value-of select="policy/minage"/> Days<br />
												</xsl:if>
												<xsl:if test="policy/histpasswd != ''">
													<strong>Password History Length: </strong><xsl:value-of select="policy/histpasswd"/><br />
												</xsl:if>
												<xsl:if test="policy/forcelogoff != ''">
													<strong>Force Logoff: </strong><xsl:value-of select="policy/forcelogoff"/> Seconds<br />
												</xsl:if>
												<xsl:if test="policy/complexity != ''">
													<strong>Password Complexity Enabled: </strong><xsl:value-of select="policy/complexity"/><br />
												</xsl:if>
											</p>
										</div>
									</div>
									<div>
										<h2><a href="javascript:void(0)" style="color:white" id="{generate-id()}" onclick="toggle(this, '{generate-id(groups)}')">[-]</a> Groups</h2>
										<div class="margin1" id="{generate-id(groups)}">
											<xsl:for-each select="groups/group">
												<div id="{generate-id(current())}">
													<p>
														<strong>Name: </strong><xsl:value-of select="name"/><br />
														<strong>Comment: </strong><xsl:value-of select="comment"/><br />
														<xsl:if test="count(members/member) &gt; 0">
															<strong>Members: </strong><a href="javascript:void(0)" id="{generate-id()}" onclick="toggle(this, '{generate-id(members)}')">[+]</a><br />
															<div class="margin2" id="{generate-id(members)}" style="display:none;">
																<xsl:for-each select="members/member">
																	<xsl:value-of select="."/><br />
																</xsl:for-each>
															</div>
														</xsl:if>
													</p>
												</div>
											</xsl:for-each>
										</div>
									</div>
									<div>
										<h2><a href="javascript:void(0)" style="color:white" id="{generate-id()}" onclick="toggle(this, '{generate-id(users)}')">[-]</a> Users</h2>
										<div id="{generate-id(users)}">
											<xsl:for-each select="users/usr">
												<div class="margin1">
													<p>
														<strong>Name: </strong><xsl:value-of select="name"/><br />
														<strong>Comment: </strong><xsl:value-of select="comment"/><br />
														<a href="javascript:void(0)" id="{generate-id()}a" onclick="toggle(this, '{generate-id(current())}')">[+]</a>
														<div id="{generate-id(current())}" style="display:none;">
															<strong>Workstations: </strong><xsl:value-of select="workstations"/><br />
															<strong>Country Code: </strong><xsl:value-of select="country_code"/><br />
															<strong>Last Logon: </strong><xsl:value-of select="last_logon"/><br />
															<strong>Password Expired: </strong><xsl:value-of select="password_expired"/><br />
															<strong>Full Name: </strong><xsl:value-of select="full_name"/><br />
															<strong>Code Page: </strong><xsl:value-of select="code_page"/><br />
															<strong>Priv: </strong><xsl:value-of select="priv"/><br />
															<strong>Auth Flags: </strong><xsl:value-of select="auth_flags"/><br />
															<strong>Logon Server: </strong><xsl:value-of select="logon_server"/><br />
															<strong>Home Dir: </strong><xsl:value-of select="home_dir"/><br />
															<strong>Home Dir Drive: </strong><xsl:value-of select="home_dir_drive"/><br />
															<strong>Usr Comment: </strong><xsl:value-of select="usr_comment"/><br />
															<strong>Profile: </strong><xsl:value-of select="profile"/><br />
															<strong>Account Expires: </strong><xsl:value-of select="acct_expires"/><br />
															<strong>Primary Group ID: </strong><xsl:value-of select="primary_group_id"/><br />
															<strong>Bad Password Count: </strong><xsl:value-of select="bad_pw_count"/><br />
															<strong>User ID: </strong><xsl:value-of select="user_id"/><br />
															<strong>Password: </strong><xsl:value-of select="password"/><br />
															<strong>Units Per Week: </strong><xsl:value-of select="units_per_week"/><br />
															<strong>Last Logoff: </strong><xsl:value-of select="last_logoff"/><br />
															<strong>Max Storage: </strong><xsl:value-of select="max_storage"/><br />
															<strong>Number of Logons: </strong><xsl:value-of select="num_logons"/><br />
															<strong>Password Age: </strong><xsl:value-of select="password_age"/><br />
															<strong>Flags: </strong><xsl:value-of select="flags"/><br />
															<strong>Script Path: </strong><xsl:value-of select="script_path"/><br />
															<strong>Groups: </strong><a href="javascript:void(0)" id="{generate-id()}" onclick="toggle(this, '{generate-id(membership)}')">[-]</a>
															<div class="margin2" id="{generate-id(membership)}">
																<xsl:for-each select="membership/member_of">
																	<xsl:value-of select="."/><br />
																</xsl:for-each>
															</div>
														</div>
													</p>
												</div>
											</xsl:for-each>
										</div>
									</div>
									<h2><a href="javascript:void(0)" style="color:white" id="{generate-id()}" onclick="toggle(this, '{generate-id(shares)}')">[-]</a> Shares</h2>
									<div class="margin1" id="{generate-id(shares)}">
										<xsl:for-each select="shares/share">
											<p>
												<strong>Name: </strong><xsl:value-of select="name"/><br />
												<strong>Remark: </strong><xsl:value-of select="remark"/><br />
												<strong>Type: </strong><xsl:value-of select="type"/><br />
											</p>
										</xsl:for-each>
									</div>
									<div>
										<h2><a href="javascript:void(0)" style="color:white" id="{generate-id()}" onclick="toggle(this, '{generate-id(services)}')">[-]</a> Services</h2>
										<div class="margin1" id="{generate-id(services)}">
											<xsl:for-each select="services/srv">
												<p>
													<strong>Name: </strong><xsl:value-of select="srv_name"/><br />
													<strong>Port: </strong><xsl:value-of select="port"/><br />
												</p>
											</xsl:for-each>
										</div>
									</div>
								</div>
							</p>
						</xsl:for-each>
					</div>
				</xsl:for-each>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>